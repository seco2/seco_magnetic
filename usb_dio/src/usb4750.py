#!/usr/bin/env python3.8
# -*- coding: utf-8 -*-
import rospy
import traceback
import datetime
import os

from typing import List, Dict, Any
from usb4750_frame import *

from usb_dio.msg import io_in, io_out, mag_detect, mag_mark, lift_ctrl
from std_msgs.msg import Bool, UInt8
from geometry_msgs.msg import Twist
from move_control.msg import teleop


class USB4750(USB4750Frame):
    ########################################
    #             변수 선언
    ########################################
    di_pub: rospy.Publisher             # 입력 데이터 Publisher
    do_pub: rospy.Publisher             # 출력 데이터 Publisher
    mag_detect_pub: rospy.Publisher     # 마그네틱 인식 여부 Publisher
    mag_mark_pub: rospy.Publisher       # 마커 인식 여부 Publisher
    restart_pub: rospy.Publisher        # 재시작 버튼 여부 Publisher

    led_count: int = 0   # LED 카운트
    motor_power_count: int = 0  # 모터 전원 카운트
    charge_off_flag: bool = False   # 충전 OFF 여부
    pre_hour: int = datetime.datetime.now().hour       # 이전 시간

    lift_run: bool = False     # 리프트 작동
    auto_run: bool = False     # 자동 동작
    joy_run: bool = False      # 수동 동작
    emergency_state: bool = False   # 비상 정지 상태
    emergency_mode: bool = False    # 비상 모드
    emergency_mode_joy: bool = False    # 비상 모드 (수동)
    slow_flag: bool = False         # 슬로우 여부
    charge_flag: bool = True       # 충전 여부

    # 입력 포트 번호
    input_port_number: Dict[int, str] = {
        0: 'emergency_sw',
        1: 'power_sw',
        2: 'lift_up_limit',
        3: 'lift_down_limit',
        4: 'plt_detect_1',
        5: 'plt_detect_2',
        6: 'plt_detect_3',
        7: 'plt_detect_4',
        8: 'restart_btn',
        9: 'mark_front',
        10: 'bumper',
        11: 'mark_left',
        12: 'mag_front',
        13: 'mag_rear',
        14: 'mag_left',
        15: 'mag_right',
    }
    
    # 출력 포트 번호
    output_port_number: Dict[int, str] = {
        0: 'motor_power',
        1: 'lift_up',
        2: 'led_red',
        3: 'led_yellow',
        4: 'led_green',
        5: 'buzzer',
        6: 'pc_on_lamp',
        7: 'lift_down',
        8: 'melody_1',
        9: 'melody_2',
        10: 'melody_3',
        11: 'melody_4',
        12: 'melody_5',
        14: 'charge',
    }

    # 입력 데이터
    input_data: Dict[str, bool] = {
        'emergency_sw': False,
        'power_sw': False,
        'lift_up_limit': False,
        'lift_down_limit': False,
        'plt_detect_1': False,
        'plt_detect_2': False,
        'plt_detect_3': False,
        'plt_detect_4': False,
        'restart_btn': False,
        'mark_front': False,
        'bumper': False,
        'mark_left': False,
        'mag_front': False,
        'mag_rear': False,
        'mag_left': False,
        'mag_right': False,
    }

    # 출력 데이터
    output_data: Dict[str, bool] = {
        'motor_power': False,
        'lift_up': False,
        'led_red': False,
        'led_yellow': False,
        'led_green': False,
        'buzzer': False,
        'pc_on_lamp': True,
        'lift_down': False,
        'melody_1': False,
        'melody_2': False,
        'melody_3': False,
        'melody_4': False,
        'melody_5': False,
        'charge': True,
    }

    # 입력 데이터 반전
    input_reverse_data: dict = {
        'emergency_sw': True, 
        'lift_up_limit': True,
        'lift_down_limit': True,
        'power_sw': True,
        'restart_btn': True,
        'plt_detect_1': True,
        'plt_detect_2': True,
        'plt_detect_3': True,
        'plt_detect_4': True,
        'mark_front': True,
        'mag_front': True,
        'mag_rear': True,
        'mag_left': True,
        'mag_right': True,
    }

    # 입력 신호 유지
    input_data_maintain: Dict[str, dict] = {
        'mark_front': {
            'count': 0,     # 횟수
            'check': 5,    # 체크할 횟수 (0.5초)
        },
        'mark_left': {
            'count': 0,     # 횟수
            'check': 5,    # 체크할 횟수 (0.5초)
        },
        'mag_front': {
            'count': 0,     # 횟수
            'check': 3,    # 체크할 횟수 (0.3초)
        },
        'mag_rear': {
            'count': 0,     # 횟수
            'check': 3,    # 체크할 횟수 (0.3초)
        },
        'mag_left': {
            'count': 0,     # 횟수
            'check': 3,    # 체크할 횟수 (0.3초)
        },
        'mag_right': {
            'count': 0,     # 횟수
            'check': 3,    # 체크할 횟수 (0.3초)
        },
        'lift_up_limit': {
            'count': 0,     # 횟수
            'check': 5,    # 체크할 횟수 (0.5초)
        },
        'lift_down_limit': {
            'count': 0,     # 횟수
            'check': 5,    # 체크할 횟수 (0.5초)
        },
    }

    # 입력 신호 필터
    input_data_filter: Dict[str, dict] = {
        'emergency_sw': {
            'count': 0,     # 횟수
            'check': 5,    # 체크할 횟수 (0.1초)
        },
        'power_sw': {
            'count': 0,     # 횟수
            'check': 10,    # 체크할 횟수 (1초)
        },
        'bumper': {
            'count': 0,     # 횟수
            'check': 5,    # 체크할 횟수 (0.1초)
        },
        'restart_btn': {
            'count': 0,     # 횟수
            'check': 5,    # 체크할 횟수 (0.5초)
        },
    }

    ########################################
    #             함수 선언
    ########################################
    # 사용자 ROS 설정
    def initUserROS(self):
        # Set Publish
        self.di_pub = rospy.Publisher("/io_in", io_in, queue_size=10)
        self.do_pub = rospy.Publisher("/io_out", io_out, queue_size=10)
        self.mag_detect_pub = rospy.Publisher("/MAG/DETECT", mag_detect, queue_size=10)
        self.mag_mark_pub = rospy.Publisher("/MAG/MARK", mag_mark, queue_size=10)
        self.restart_pub = rospy.Publisher("/USB4750/restart", Bool, queue_size=10)

        # Set Subscribe
        rospy.Subscriber("/lift/ctrl", lift_ctrl, self.liftControlCallback)
        rospy.Subscriber("/cmd_vel_joy", teleop, self.joyControlCallback)
        rospy.Subscriber("/cmd_vel", Twist, self.navControlCallback)
        rospy.Subscriber("/EMERGENCY/STATE", Bool, self.emcStateCallback)
        rospy.Subscriber("/EMERGENCY/MODE", Bool, self.emcModeCallback)
        rospy.Subscriber("/EMERGENCY/JOY_MODE", Bool, self.emcJoyModeCallback)
        rospy.Subscriber("/EMERGENCY/SLOW", Bool, self.emcSlowCallback)
        rospy.Subscriber("/CHG", UInt8, self.chargeCallback)

        self.input_reverse_data.update({'mark_left': False if self.agv_no == 1 else True})

    # 입력 데이터 처리
    def inputDataProcess(self):
        for i in self.input_port_number.keys():
            port_name: str = self.input_port_number[i]
            self.input_data[port_name] = self.DIs[i]
            
            # 입력 신호 반전
            if self.input_reverse_data.get(port_name) is True:
                self.input_data[port_name] = not self.input_data[port_name]

            # 일정 시간동안 신호 유지
            if self.input_data_maintain.get(port_name) is not None:
                if self.input_data[port_name] is True:
                    self.input_data_maintain[port_name]['count'] = 1
                if self.input_data_maintain[port_name]['count'] >= 1:
                    self.input_data[port_name] = True
                    if self.input_data_maintain[port_name]['count'] >= self.input_data_maintain[port_name]['check']:
                        self.input_data_maintain[port_name]['count'] = 0
                    else:
                        self.input_data_maintain[port_name]['count'] += 1

            # 일정 시간동안 유지되어야함
            if self.input_data_filter.get(port_name) is not None:
                if self.input_data[port_name] is True:
                    if self.input_data_filter[port_name]['count'] >= self.input_data_filter[port_name]['check']:
                        self.input_data[port_name] = True
                    else:
                        self.input_data_filter[port_name]['count'] += 1
                        self.input_data[port_name] = False
                else:
                    self.input_data_filter[port_name]['count'] = 0

    # 출력 데이터 처리
    def outputDataProcess(self):
        #   melody_1 = 주행 (팔레트 없음)
        #   melody_2 = 리프트 UP
        #   melody_3 = 리프트 Down
        #   melody_4 = 주행 (팔레트 있음)
        #   melody_5 = 주행 (비상모드)
        if self.emergency_mode is True or self.emergency_mode_joy is True:
            # 비상 모드일 경우
            self.output_data['motor_power'] = True
            self.output_data['led_red'] = True
            self.output_data['led_yellow'] = True
            self.output_data['led_green'] = True
            self.output_data['buzzer'] = True
            self.led_count = 0

            self.output_data['melody_1'] = False
            self.output_data['melody_4'] = False
            self.output_data['melody_5'] = True if self.auto_run is True or self.joy_run is True else False   # 주행 중일 경우 On
        elif self.input_data['bumper'] or self.input_data['emergency_sw']:
            # 물리적 비상 정지
            self.output_data['motor_power'] = False
            self.output_data['led_red'] = True
            self.output_data['led_yellow'] = False
            self.output_data['led_green'] = False
            self.output_data['buzzer'] = True
            self.led_count = 0

            self.output_data['lift_up'] = False
            self.output_data['lift_down'] = False

            self.output_data['melody_1'] = False
            self.output_data['melody_2'] = False
            self.output_data['melody_3'] = False
            self.output_data['melody_4'] = False
            self.output_data['melody_5'] = False
        else:
            if self.auto_run is True or self.joy_run is True or self.lift_run is True:
                # 바퀴 & 리프트가 작동 중일 경우
                if self.emergency_state is True:
                    # 비상 정지
                    self.output_data['motor_power'] = False
                    self.output_data['led_red'] = False
                    self.output_data['led_yellow'] = False
                    self.output_data['led_green'] = True
                    self.output_data['buzzer'] = True

                    self.output_data['lift_up'] = False
                    self.output_data['lift_down'] = False

                    self.output_data['melody_1'] = False
                    self.output_data['melody_2'] = False
                    self.output_data['melody_3'] = False
                    self.output_data['melody_4'] = False
                    self.output_data['melody_5'] = False

                    if self.led_count <= 10:
                        self.output_data['led_red'] = True
                    elif self.led_count >= 20:
                        self.led_count = 0
                else:
                    # 정상 작동 상태
                    self.output_data['motor_power'] = True
                    self.output_data['led_red'] = False
                    self.output_data['led_yellow'] = False
                    self.output_data['led_green'] = True
                    self.output_data['buzzer'] = False

                    self.output_data['melody_1'] = False
                    self.output_data['melody_4'] = False
                    self.output_data['melody_5'] = False

                    if self.lift_run is False:
                        if self.input_data['plt_detect_1'] is True or \
                            self.input_data['plt_detect_2'] is True or \
                            self.input_data['plt_detect_3'] is True or \
                            self.input_data['plt_detect_4'] is True:
                            # 팔레트가 감지되었다면
                            self.output_data['melody_4'] = True
                        else:
                            self.output_data['melody_1'] = True

                    if self.slow_flag is True:
                        # 슬로우 상태일 경우
                        if self.led_count <= 10:
                            self.output_data['led_yellow'] = True
                        elif self.led_count >= 20:
                            self.led_count = 0
                    else:
                        self.led_count = 0
            else:
                # 대기 상태
                self.output_data['motor_power'] = False
                self.output_data['led_red'] = False
                self.output_data['led_yellow'] = True
                self.output_data['led_green'] = False
                self.output_data['buzzer'] = False
                self.led_count = 0

                self.output_data['lift_up'] = False
                self.output_data['lift_down'] = False

                self.output_data['melody_1'] = False
                self.output_data['melody_2'] = False
                self.output_data['melody_3'] = False
                self.output_data['melody_4'] = False
                self.output_data['melody_5'] = False

        # 충전 버튼 켜지면 PC OFF
        if self.input_data['power_sw'] is True and self.output_data['pc_on_lamp'] is True:
            self.output_data['pc_on_lamp'] = False
            os.system('shutdown -h now')

        # 모터 드라이브 파워 딜레이
        if self.output_data['motor_power'] is False:
            if self.motor_power_count >= 20:
                # 2초 후 OFF
                self.output_data['motor_power'] = False
            else:
                # 2초 지나기 전까진 ON
                self.motor_power_count += 1
                self.output_data['motor_power'] = True
        else:
            self.motor_power_count = 0

        # 정각이 되면 5초 동안 충전 껐다 키기
        if self.charge_flag is True:
            now_time: datetime.datetime = datetime.datetime.now()
            if self.pre_hour != now_time.hour:
                # 시간이 다를 경우
                if self.charge_off_flag is True:
                    if now_time.second == 5:
                        # 충전이 OFF 되어있고 현재 초가 5초일경우 On
                        self.pre_hour = now_time.hour
                        self.charge_off_flag = False
                        self.output_data['charge'] = True
                    else:
                        self.output_data['charge'] = False
                else:
                    self.charge_off_flag = True
                    self.output_data['charge'] = False
            else:
                # 시간이 같을 경우
                self.output_data['charge'] = True
        else:
            self.output_data['charge'] = False

        for i in self.output_port_number.keys():
            self.DOs[i] = self.output_data[self.output_port_number[i]]

    # 입력 데이터 보내기
    def sendInputData(self):
        # 마그네틱 인식 여부 보내기
        magDetect: mag_detect = mag_detect()
        magDetect.front = self.input_data['mag_front']
        magDetect.rear = self.input_data['mag_rear']
        magDetect.left = self.input_data['mag_left']
        magDetect.right = self.input_data['mag_right']
        self.mag_detect_pub.publish(magDetect)

        # 마커 인식 여부 보내기
        magMark: mag_mark = mag_mark()
        magMark.front = False
        magMark.decel = self.input_data['mark_front']
        magMark.left = self.input_data['mark_left']
        self.mag_mark_pub.publish(magMark)

        # 재시작 버튼 보내기
        restart_data: Bool = Bool()
        restart_data.data = self.input_data['restart_btn']
        self.restart_pub.publish(restart_data)

        # 입력 데이터 보내기
        ioSig: io_in = io_in()
        ioSig.emc = 1 if self.input_data['emergency_sw'] is True else 0
        ioSig.bump = 1 if self.input_data['bumper'] is True else 0
        ioSig.powSw = 1 if self.input_data['power_sw'] is True else 0
        ioSig.lim_up = 1 if self.input_data['lift_up_limit'] is True else 0
        ioSig.lim_down = 1 if self.input_data['lift_down_limit'] is True else 0
        ioSig.limSen1 = 1 if self.input_data['plt_detect_1'] is True else 0
        ioSig.limSen2 = 1 if self.input_data['plt_detect_2'] is True else 0
        ioSig.limSen3 = 1 if self.input_data['plt_detect_3'] is True else 0
        ioSig.limSen4 = 1 if self.input_data['plt_detect_4'] is True else 0
        self.di_pub.publish(ioSig)

    # 출력 데이터 보내기
    def sendOuputData(self):
        outSig: io_out = io_out()
        outSig.liftUp = 1 if self.output_data['lift_up'] is True else 0
        outSig.liftDown = 1 if self.output_data['lift_down'] is True else 0
        outSig.moterPower = 1 if self.output_data['motor_power'] is True else 0
        outSig.pcOn = 1 if self.output_data['pc_on_lamp'] is True else 0
        outSig.redL = 1 if self.output_data['led_red'] is True else 0
        outSig.yelL = 1 if self.output_data['led_yellow'] is True else 0
        outSig.greL = 1 if self.output_data['led_green'] is True else 0
        outSig.buzz = 1 if self.output_data['buzzer'] is True else 0
        outSig.mel1L = 1 if self.output_data['melody_1'] is True else 0
        outSig.mel2L = 1 if self.output_data['melody_2'] is True else 0
        outSig.mel3L = 1 if self.output_data['melody_3'] is True else 0
        outSig.mel4L = 1 if self.output_data['melody_4'] is True else 0
        outSig.mel5L = 1 if self.output_data['melody_5'] is True else 0
        outSig.chg = 1 if self.output_data['charge'] is True else 0
        self.do_pub.publish(outSig)

    ################################
    #       ROS 관련 함수 정의
    #################################
    # 리프트 작동 콜백
    def liftControlCallback(self, liftCtrl: lift_ctrl):
        if liftCtrl.run is True:
            # 리프트 작동일 경우
            self.lift_run = True
            if liftCtrl.up is True:
                # 리프트 상승 일 경우
                self.output_data['lift_up'] = True
                self.output_data['lift_down'] = False
                self.output_data['melody_2'] = True
                self.output_data['melody_3'] = False
            else:
                # 리프트 하강일 경우
                self.output_data['lift_up'] = False
                self.output_data['lift_down'] = True
                self.output_data['melody_2'] = False
                self.output_data['melody_3'] = True
        else:
            # 리프트가 동작하지 않을 경우
            self.lift_run = False
            self.output_data['lift_up'] = False
            self.output_data['lift_down'] = False
            self.output_data['melody_2'] = False
            self.output_data['melody_3'] = False

    # 수동 동작 콜백
    def joyControlCallback(self, joy_data: teleop):
        self.joy_run = True if joy_data.control_mode == 1 else False

    # 자동 동작 콜백
    def navControlCallback(self, nav_data: Twist):
        self.auto_run = False if nav_data.linear.x == 0 and nav_data.linear.y == 0 and nav_data.angular.z == 0 else True

    # 비상정지 상태 콜백
    def emcStateCallback(self, emergency_data: Bool):
        self.emergency_state = emergency_data.data

    # 비상모드 콜백
    def emcModeCallback(self, emergency_data: Bool):
        self.emergency_mode = emergency_data.data
    
    # 비상모드 (수동) 콜백
    def emcJoyModeCallback(self, emergency_data: Bool):
        self.emergency_mode_joy = emergency_data.data

    # 슬로우 감지 콜백
    def emcSlowCallback(self, slow_data: Bool):
        self.slow_flag = slow_data.data

    # 충전 콜백
    def chargeCallback(self, chg_data: UInt8):
        self.charge_flag = True if chg_data.data == 1 else False


if __name__=='__main__':
    USB4750()