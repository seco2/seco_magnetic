#include "ros/ros.h"
#include "ros/time.h"

#include <errno.h>
#include <fcntl.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <time.h>

#include <iostream>
#include <string>

#include "bdaqctrl.h"
#include "compatibility.h"
#include "geometry_msgs/Twist.h"
#include "usb_dio/io_in.h"
#include "usb_dio/io_out.h"
#include "usb_dio/lift_ctrl.h"
#include "usb_dio/mag_detect.h"
#include "usb_dio/mag_mark.h"
#include <math.h>
#include <move_control/teleop.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/Bool.h>

// using namespace std;
using namespace Automation::BDaq;
//-----------------------------------------------------------------------------------
// Configure the following parameters before running the demo
//-----------------------------------------------------------------------------------
typedef unsigned char byte;
#define deviceDescription L"USB-4750,BID#0"
// const wchar_t* profilePath = L"../../profile/DemoDevice.xml";

class usb4750
{
public:
  usb4750();
  ~usb4750();
  int spin();

protected:
private:
  ros::NodeHandle nh_;
  ros::Publisher pubDIO, pubDO, pubUsbConnect, magDetectPub, magMarkPub, restartPub;
  ros::Subscriber subDIO, subNavVel, subJoyVel, sublift, subCharge, sub_emc_state, sub_emc_mode, sub_emc_joy_mode, sub_emc_slow;

  void dIOPublish();
  void dOPub();
  void liftControlCallback(const usb_dio::lift_ctrl::ConstPtr& liftCtrl);
  void NavControlCallback(const geometry_msgs::Twist::ConstPtr& motorCmd_);
  void JoyControlCallback(const move_control::teleop::ConstPtr& motorCmd_);
  void chargeCallback(const std_msgs::UInt8::ConstPtr& msg);

  void connectPub(bool flag);
  void emcStateCallback(const std_msgs::Bool::ConstPtr& msg);
  void emcModeCallback(const std_msgs::Bool::ConstPtr& msg);
  void emcJoyModeCallback(const std_msgs::Bool::ConstPtr& msg);
  void emcSlowCallback(const std_msgs::Bool::ConstPtr& msg);

  int port(int val);
  int portNum(int val);
  // void ioRead();
  // void ioControl();
  int rate_ = 10;
  int32_t startPort = 0;
  int32_t portCount = 2;

  int8_t DIs_[16], DOs_[16];
  const wchar_t* profilePath = L"../../profile/DemoDevice.xml";

  int redO_ = 2, greO_ = 4, yelO_ = 3, buzO_ = 5, motPowO_ = 0, liftUpO_ = 1, liftDownO_ = 7, pcOnO_ = 6;
  uint32 redV_ = 0, greV_ = 0, yelV_ = 0, buzV_ = 0, motPowV_ = 0, liftUpV_ = 0, liftDownV_ = 0, pcOnV_ = 0;
  uint32 pre_redV_ = 0, pre_greV_ = 0, pre_yelV_ = 0, pre_buzV_ = 0, pre_motPowV_ = 0, pre_liftUpV_ = 0, pre_liftDownV_ = 0, pre_pcOnV_ = 0;

  int emcI_ = 0, powSwI_ = 1, limUpI_ = 2, limDownI_ = 3, bumpI_ = 10, limSen1I_ = 4, limSen2I_ = 5, limSen3I_ = 6, limSen4I_ = 7;
  int8_t emc_ = 0, powSw_ = 0, lim_up_ = 0, lim_down_ = 0, bump_ = 0, limSen1_ = 0, limSen2_ = 0, limSen3_ = 0, limSen4_ = 0;

  int mag_frontI = 12, mag_rearI = 13, mag_leftI = 14, mag_rightI = 15;
  int mark_frontI = 8, mark_decelI = 9, mark_leftI = 11;

  int restart_btnI = 8;

  // 사운드
  int mel1O_ = 8, mel2O_ = 9, mel3O_ = 10, mel4O_ = 11, mel5O_ = 12;
  uint32 mel1V_ = 0, mel2V_ = 0, mel3V_ = 0, mel4V_ = 0, mel5V_ = 0;
  uint32 pre_mel1V_ = 0, pre_mel2V_ = 0, pre_mel3V_ = 0, pre_mel4V_ = 0, pre_mel5V_ = 0;

  int chargeO_ = 14;
  uint32 chgV_ = 1;
  uint32 pre_chgV_ = 0;

  ErrorCode ret, ret2;
  InstantDiCtrl* instantDiCtrl;
  InstantDoCtrl* instantDoCtrl;

  bool agvAutoRun_ = false, agvManuRun_ = false;

  int input_count = 0;
  int count_ = 0;
  int pc_off_count = 0;

  int emc_count = 0;
  int bump_count = 0;

  bool emc_state_data = false;    // 비상 모드 상태
  bool emc_mode_data = false;       // 비상 모드 (ACS)
  bool emc_joy_mode_data = false;   // 비상 모드 (조이스틱)

  int pre_hour = 0;
  int pre_second = 0;
  bool chg_turn = false;   // 충전 껐다 키기 여부

  bool slow_flag = false;   // 슬로우 여부

  bool lift_run = false;    // 리프트 작동 여부

  int lim_up_count = 0;
  int lim_down_count = 0;

  int mark_front_count = 0;
  int mark_decel_count = 0;
  int mark_left_count = 0;

  int mag_front_count = 0;
  int mag_rear_count = 0;
  int mag_left_count = 0;
  int mag_right_count = 0;

  int motPowV_count = 0;
  int restart_btn_count = 0;
  int error_count = 0;
  int input_count_force = 0;
};

usb4750::usb4750()
{

  pubDIO = nh_.advertise<usb_dio::io_in>("/io_in", 10);
  pubDO = nh_.advertise<usb_dio::io_out>("/io_out", 10);

  sublift = nh_.subscribe("/lift/ctrl", 1, &usb4750::liftControlCallback, this);

  // subcribe navigation command
  subNavVel = nh_.subscribe("cmd_vel", 1, &usb4750::NavControlCallback, this);
  // subcribe commanded velocity
  subJoyVel =
      nh_.subscribe("cmd_vel_joy", 1, &usb4750::JoyControlCallback, this);
  subCharge = nh_.subscribe("/CHG", 1, &usb4750::chargeCallback, this);

  pubUsbConnect = nh_.advertise<std_msgs::Bool>("/USB4750/CONNECT", 1);

  magDetectPub = nh_.advertise<usb_dio::mag_detect>("/MAG/DETECT", 10);
  magMarkPub = nh_.advertise<usb_dio::mag_mark>("/MAG/MARK", 10);
  restartPub = nh_.advertise<std_msgs::Bool>("/USB4750/restart", 10);

  sub_emc_state = nh_.subscribe("/EMERGENCY/STATE", 1, &usb4750::emcStateCallback, this);
  sub_emc_mode = nh_.subscribe("/EMERGENCY/MODE", 1, &usb4750::emcModeCallback, this);
  sub_emc_joy_mode = nh_.subscribe("/EMERGENCY/JOY_MODE", 1, &usb4750::emcJoyModeCallback, this);
  sub_emc_slow = nh_.subscribe("/EMERGENCY/SLOW", 1, &usb4750::emcSlowCallback, this);

  // 연결 여부 보내기
  usleep(100000);   // 0.1초 쉼
  connectPub(false);   
  usleep(100000);   // 0.1초 쉼

  
  for (int i = 0; i < 16; i++)
  {
    DOs_[i] = 1;
  }

  for (int i = 0; i < 16; i++)
  {
    DIs_[i] = 1;
  }

  ret = Success, ret2 = Success;
  // Create a 'InstantDiCtrl' for DI function.
  instantDiCtrl = InstantDiCtrl::Create();

  // Create a 'InstantDoCtrl' for DO function.
  instantDoCtrl = InstantDoCtrl::Create();

  DeviceInformation devInfo(deviceDescription);
  ret = instantDiCtrl->setSelectedDevice(devInfo);
  ret = instantDiCtrl->LoadProfile(profilePath); // Loads a profile to initialize the device.
  ret2 = instantDoCtrl->setSelectedDevice(devInfo);
  ret2 = instantDoCtrl->LoadProfile(profilePath); // Loads a profile to initialize the device.
}

void usb4750::chargeCallback(const std_msgs::UInt8::ConstPtr& msg)
{
  chgV_ = (uint32)msg->data;
}

int usb4750::port(int val)
{
  if (val < 8)
    return val;
  else
    return val - 8;
}
int usb4750::portNum(int val)
{
  if (val < 8)
    return 0;
  else
    return 1;
}
void usb4750::liftControlCallback(const usb_dio::lift_ctrl::ConstPtr& liftCtrl)
{
  if (liftCtrl->run)
  { 
    lift_run = true;
    if (liftCtrl->up)
    {
      liftUpV_ = 1;
      liftDownV_ = 0;
      mel2V_ = 1;
      mel3V_ = 0;
    }
    else
    {
      liftUpV_ = 0;
      liftDownV_ = 1;
      mel2V_ = 0;
      mel3V_ = 1;
    }
  }
  else
  {
    lift_run = false;
    liftUpV_ = 0;
    liftDownV_ = 0;
    mel3V_ = 0;
    mel2V_ = 0;
  }
}

void usb4750::connectPub(bool flag)
{
  std_msgs::Bool connect_msgs;
  connect_msgs.data = flag;
  pubUsbConnect.publish(connect_msgs);
}

void usb4750::emcStateCallback(const std_msgs::Bool::ConstPtr& msg)
{
  emc_state_data = msg->data;
}

void usb4750::emcModeCallback(const std_msgs::Bool::ConstPtr& msg)
{
  emc_mode_data = msg->data;
}

void usb4750::emcJoyModeCallback(const std_msgs::Bool::ConstPtr& msg)
{
  emc_joy_mode_data = msg->data;
}

void usb4750::emcSlowCallback(const std_msgs::Bool::ConstPtr& msg)
{
  slow_flag = msg->data;
}

void usb4750::NavControlCallback(const geometry_msgs::Twist::ConstPtr& motorCmd_)
{
  if ((motorCmd_->linear.x == 0) && (motorCmd_->linear.y == 0) &&
      (motorCmd_->angular.z == 0))
    agvAutoRun_ = false;
  else
    agvAutoRun_ = true;
}

void usb4750::JoyControlCallback(const move_control::teleop::ConstPtr& motorCmd_)
{ 
  if (motorCmd_->control_mode == 1)
    agvManuRun_ = true;
  else
    agvManuRun_ = false;
}

usb4750::~usb4750()
{
  instantDiCtrl->Dispose();
  instantDoCtrl->Dispose();
}

void usb4750::dIOPublish()
{
  usb_dio::io_in ioSig;

  ioSig.emc = emc_;
  ioSig.bump = bump_;
  ioSig.powSw = powSw_;

  ioSig.lim_up = lim_up_;
  ioSig.lim_down = lim_down_;
  ioSig.limSen1 = limSen1_;
  ioSig.limSen2 = limSen2_;
  ioSig.limSen3 = limSen3_;
  ioSig.limSen4 = limSen4_;

  pubDIO.publish(ioSig);
}

void usb4750::dOPub()
{
  usb_dio::io_out outSig;

  outSig.liftUp = liftUpV_;
  outSig.liftDown = liftDownV_;
  outSig.moterPower = motPowV_;
  outSig.pcOn = pcOnV_;
  outSig.redL = redV_;
  outSig.yelL = yelV_;
  outSig.greL = greV_;
  outSig.buzz = buzV_;
  outSig.mel1L = mel1V_;
  outSig.mel2L = mel2V_;
  outSig.mel3L = mel3V_;
  outSig.mel4L = mel4V_;
  outSig.mel5L = mel5V_;
  outSig.chg = chgV_;

  pubDO.publish(outSig);
}

int usb4750::spin()
{

  ros::Rate loop_rate(rate_);

  // CHK_RESULT(ret);
  if (BioFailed(ret)) {
    // 에러 발생 시
    // 연결 해제 보내기
    connectPub(false);
    ros::shutdown();
  }

  while (ros::ok()) // continouslly receive signals from a serial port
  {
    try {
      time_t curTime = time(NULL);
      struct tm *pLocal = localtime(&curTime);
      int now_hour = pLocal->tm_hour;

      if (pre_hour != now_hour) {
        // 시간이 같지 않을 경우
        int now_second = pLocal->tm_sec;
        if (chg_turn == true && now_second >= 5) {
          // 충전을 껏다 키기 5초가 지난 경우
          chg_turn = false;
          pre_hour = now_hour;
        } else if (chg_turn == false) {
          // 충전을 껏다 키기 시작한 경우
          chg_turn = true;
        }
      }

      byte bufferForReading[2] = {0}; // the first element of this array is used for start port
      ret = instantDiCtrl->Read(startPort, portCount, bufferForReading);

      if (BioFailed(ret)) {
        // 에러 발생 시
        // 연결 해제 보내기
        // wchar_t enumString[256];
        // AdxEnumToString(L"ErrorCode", (int32)ret, 256, enumString);
        // printf("Some error occurred. And the last error code is 0x%X. [%ls]\n", ret,
        //   enumString);
        throw "receive error";
      }
      // CHK_RESULT(ret);

      for (int i = 0; i < 8; i++)
      {
        DIs_[i] = ((bufferForReading[startPort] >> i) & 0x01);
      }

      for (int i = 0; i < 8; i++)
      {
        DIs_[i + 8] = ((bufferForReading[startPort + 1] >> i) & 0x01);
      }

      int8_t cur_bump_ = DIs_[bumpI_];
      bump_ = 0;
      if (cur_bump_ == 1) {
        if (bump_count >= 5) {
          bump_ = 1;
        } else {
          bump_count++;
        }
      } else {
        bump_count = 0;
      }
      bump_ = 0;
      
      int8_t cur_emc_ = 1 - DIs_[emcI_];
      emc_ = 0;
      if (cur_emc_ == 1) {
        if (emc_count >= 5) {
          emc_ = 1;
        } else {
          emc_count++;
        }
      } else {
        emc_count = 0;
      }

      lim_up_ = 1 - DIs_[limUpI_];
      lim_down_ = 1 - DIs_[limDownI_];
      powSw_ = 1 - DIs_[powSwI_];

      if (lim_up_ == 1){
        lim_up_count = 1;
      }
      if (lim_up_count >= 1) {
        lim_up_ = 1;
        if (lim_up_count >= 5) {
          lim_up_count = 0;
        } else {
          lim_up_count++;
        }
      }

      if (lim_down_ == 1){
        lim_down_count = 1;
      }
      if (lim_down_count >= 1) {
        lim_down_ = 1;
        if (lim_down_count >= 5) {
          lim_down_count = 0;
        } else {
          lim_down_count++;
        }
      }

      limSen1_ = 1 - DIs_[limSen1I_];
      limSen2_ = 1 - DIs_[limSen2I_];
      limSen3_ = 1 - DIs_[limSen3I_];
      limSen4_ = 1 - DIs_[limSen4I_];

      // magnetic limit
      usb_dio::mag_detect magDetect;
      magDetect.front = DIs_[mag_frontI] == 0 ? true : false;
      magDetect.rear = DIs_[mag_rearI] == 0 ? true : false;
      magDetect.left = DIs_[mag_leftI] == 0 ? true : false;
      magDetect.right = DIs_[mag_rightI] == 0 ? true: false;

      if (magDetect.front == true){
        mag_front_count = 1;
      }
      if (mag_front_count >= 1) {
        magDetect.front = true;
        if (mag_front_count >= 5) {
          mag_front_count = 0;
        } else {
          mag_front_count++;
        }
      }

      if (magDetect.rear == true){
        mag_rear_count = 1;
      }
      if (mag_rear_count >= 1) {
        magDetect.rear = true;
        if (mag_rear_count >= 5) {
          mag_rear_count = 0;
        } else {
          mag_rear_count++;
        }
      }

      if (magDetect.left == true){
        mag_left_count = 1;
      }
      if (mag_left_count >= 1) {
        magDetect.left = true;
        if (mag_left_count >= 5) {
          mag_left_count = 0;
        } else {
          mag_left_count++;
        }
      }

      if (magDetect.right == true){
        mag_right_count = 1;
      }
      if (mag_right_count >= 1) {
        magDetect.right = true;
        if (mag_right_count >= 5) {
          mag_right_count = 0;
        } else {
          mag_right_count++;
        }
      }
      magDetectPub.publish(magDetect);

      // mark
      usb_dio::mag_mark magMark;
      magMark.front = DIs_[mark_frontI] == 0 ? true : false;
      magMark.left = DIs_[mark_leftI] == 0 ? true : false;
      magMark.decel = DIs_[mark_decelI] == 0 ? true : false;

      if (magMark.front == true){
        mark_front_count = 1;
      }
      if (mark_front_count >= 1) {
        magMark.front = true;
        if (mark_front_count >= 15) {
          mark_front_count = 0;
        } else {
          mark_front_count++;
        }
      }

      // MARK LEFT
      if (magMark.left == true){
        mark_left_count = 1;
      }
      if (mark_left_count >= 1) {
        magMark.left = true;
        if (mark_left_count >= 15) {
          mark_left_count = 0;
        } else {
          mark_left_count++;
        }
      }

      // MARK DECEL
      if (magMark.decel == true){
        mark_decel_count = 1;
      }
      if (mark_decel_count >= 1) {
        magMark.decel = true;
        if (mark_decel_count >= 15) {
          mark_decel_count = 0;
        } else {
          mark_decel_count++;
        }
      }
      magMarkPub.publish(magMark);

      dIOPublish();

      std_msgs::Bool retart_msgs;
      retart_msgs.data = DIs_[restart_btnI] == 0 ? true : false;
      if (retart_msgs.data == true){
        if (restart_btn_count >= 5) {
          retart_msgs.data = true;
        } else {
          retart_msgs.data = false;
          restart_btn_count++;
        }
      } else {
        restart_btn_count = 0;
      }
      restartPub.publish(retart_msgs);

      //pc off
      // if(powSw_ == 1)
      // {
      //    if(pc_off_count >= 60)
      //    {
      //      system("shutdown -P now");
      //    }
      //    pc_off_count++; 
      // }
      // else
      // {
      //     pc_off_count = 0;
      // }

      /*
      mel1V_ = 주행 (팔레트 없음);
      mel2V_ = 리프트 UP;
      mel3V_ = 리프트 Down;
      mel4V_ = 주행 (팔레트 있음);
      mel5V_ = 주행 (비상모드);
      */
      if (emc_mode_data == true || emc_joy_mode_data == true) {
        // 비상 모드일 경우
        motPowV_ = 1;
        redV_ = 1;
        greV_ = 1;
        yelV_ = 1;
        buzV_ = 1;
        count_ = 0;

        mel1V_ = 0;
        mel4V_ = 0;
        mel5V_ = (agvAutoRun_ || agvManuRun_) ? 1 : 0;    // 주행 중일 경우 1, 아닐 경우 0
      } else if ((emc_ == 1) || (bump_ == 1)) {
        // 물리적 비상 정지
        motPowV_ = 0;
        redV_ = 1;
        greV_ = 0;
        yelV_ = 0;
        buzV_ = 1;
        count_ = 0;

        mel1V_ = 0;
        mel2V_ = 0;
        mel3V_ = 0;
        mel5V_ = 0;
        mel4V_ = 0;
        // 리프트 업 다운 끄기
        liftUpV_ = 0;
        liftDownV_ = 0;
      } else {
        if (agvAutoRun_ || agvManuRun_ || lift_run)
        { 
          if (emc_state_data == true) {
            // 비상 정지
            redV_ = 0;
            greV_ = 1;
            yelV_ = 0;
            buzV_ = 1;

            mel1V_ = 0;
            mel2V_ = 0;
            mel3V_ = 0;
            mel4V_ = 0;
            mel5V_ = 0;

            // 리프트 업 다운 끄기
            liftUpV_ = 0;
            liftDownV_ = 0;

            if (count_ <= 10) {
              redV_ = 1;
              motPowV_ = 0;
            }

            count_++;
            if (count_ >= 20)
              count_ = 0;
          } else {
            // 작동 작동 상태
            motPowV_ = 1;
            redV_ = 0;
            greV_ = 1;
            yelV_ = 0;
            buzV_ = 0;

            mel1V_ = 0;
            mel4V_ = 0;

            if (lift_run == false) {
              // 리프트가 작동 중이지 않을 때
              if (limSen1_ == 1 || limSen2_ == 1 || limSen3_ == 1 || limSen4_ == 1) {
                // 팔레트가 인식이 되었을 떄
                mel4V_ = 1;
              } else {
                // 팔레트가 인식이 되어있지 않을 경우
                mel1V_ = 1;
              }
            }

            // 슬로우 상태일 경우
            if (slow_flag == true) {
              if (count_ <= 10) {
                yelV_ = 1;
              }

              count_++;
              if (count_ >= 20)
                count_ = 0;
            }
          }
        } else {
          // 대기 상태
          motPowV_ = 0;
          redV_ = 0;
          greV_ = 0;
          yelV_ = 1;
          buzV_ = 0;

          mel1V_ = 0;
          mel2V_ = 0;
          mel3V_ = 0;
          mel4V_ = 0;
          mel5V_ = 0;
        }
      }
      
      if (motPowV_ == 0) {
        if (motPowV_count >= 30) {
          motPowV_ = 0;
        } else {
          motPowV_count++;
          motPowV_ = 1;
        } 
      } else {
        motPowV_count = 0;
      }
      
      if (input_count >= 5) {
        if (pre_redV_ != redV_) {
          ret2 = instantDoCtrl->WriteBit(startPort, redO_, redV_);
        }
        
        if (pre_buzV_ != buzV_) {
          ret2 = instantDoCtrl->WriteBit(startPort, buzO_, buzV_);
        }

        if (pre_greV_ != greV_) {
          ret2 = instantDoCtrl->WriteBit(startPort, greO_, greV_);
        }

        if (pre_yelV_ != yelV_) {
          ret2 = instantDoCtrl->WriteBit(startPort, yelO_, yelV_);
        }
        
        if (pre_motPowV_ != motPowV_) {
          ret2 = instantDoCtrl->WriteBit(startPort, motPowO_, motPowV_);
        }

        if (pre_liftUpV_ != liftUpV_) {
          ret2 = instantDoCtrl->WriteBit(startPort, liftUpO_, liftUpV_);
        }

        if (pre_liftDownV_ != liftDownV_) {
          ret2 = instantDoCtrl->WriteBit(startPort, liftDownO_, liftDownV_);
        }

        if (pre_pcOnV_ != pcOnV_) {
          ret2 = instantDoCtrl->WriteBit(startPort, pcOnO_, pcOnV_);
        }

        if (pre_mel1V_ != mel1V_) {
          ret2 = instantDoCtrl->WriteBit(startPort + portNum(mel1O_), port(mel1O_), mel1V_);
        }

        if (pre_mel2V_ != mel2V_) {
          ret2 = instantDoCtrl->WriteBit(startPort + portNum(mel2O_), port(mel2O_), mel2V_);
        }
        
        if (pre_mel3V_ != mel3V_) {
          ret2 = instantDoCtrl->WriteBit(startPort + portNum(mel3O_), port(mel3O_), mel3V_);
        }

        if (pre_mel4V_ != mel4V_) {
          ret2 = instantDoCtrl->WriteBit(startPort + portNum(mel4O_), port(mel4O_), mel4V_);
        }

        if (pre_mel5V_ != mel5V_) {
          ret2 = instantDoCtrl->WriteBit(startPort + portNum(mel5O_), port(mel5O_), mel5V_);
        }

        uint32 chgV_data = chgV_;
        if (chg_turn == true) {
          // 충전을 껏다 키고 있을 경우
          chgV_data = 0;
        }

        if (pre_chgV_ != chgV_data) {
          ret2 = instantDoCtrl->WriteBit(startPort + portNum(chargeO_), port(chargeO_), chgV_data);
        }

        if (input_count_force >= 3) {
          // moter run -> no change
          if (motPowV_ == 0) {
            ret2 = instantDoCtrl->WriteBit(startPort, redO_, redV_);
            ret2 = instantDoCtrl->WriteBit(startPort, buzO_, buzV_);
            ret2 = instantDoCtrl->WriteBit(startPort, greO_, greV_);
            ret2 = instantDoCtrl->WriteBit(startPort, yelO_, yelV_);
            ret2 = instantDoCtrl->WriteBit(startPort + portNum(mel1O_), port(mel1O_), mel1V_);
            ret2 = instantDoCtrl->WriteBit(startPort + portNum(mel2O_), port(mel2O_), mel2V_);
            ret2 = instantDoCtrl->WriteBit(startPort + portNum(mel3O_), port(mel3O_), mel3V_);
            ret2 = instantDoCtrl->WriteBit(startPort + portNum(mel4O_), port(mel4O_), mel4V_);
            ret2 = instantDoCtrl->WriteBit(startPort + portNum(mel5O_), port(mel5O_), mel5V_);
            ret2 = instantDoCtrl->WriteBit(startPort + portNum(chargeO_), port(chargeO_), chgV_data);
          }
          ret2 = instantDoCtrl->WriteBit(startPort, motPowO_, motPowV_);
          ret2 = instantDoCtrl->WriteBit(startPort, liftUpO_, liftUpV_);
          ret2 = instantDoCtrl->WriteBit(startPort, liftDownO_, liftDownV_);
          
          input_count_force = 0;
        } else {
          input_count_force++;
        }
        pre_redV_ = redV_;
        pre_buzV_ = buzV_;
        pre_greV_ = greV_;
        pre_yelV_ = yelV_;
        pre_motPowV_ = motPowV_;
        pre_liftUpV_ = liftUpV_;
        pre_liftDownV_ = liftDownV_;
        pre_pcOnV_ = pcOnV_;
        pre_mel1V_ = mel1V_;
        pre_mel2V_ = mel2V_;
        pre_mel3V_ = mel3V_;
        pre_mel4V_ = mel4V_;
        pre_mel5V_ = mel5V_;
        pre_chgV_ = chgV_data;

        dOPub();
      } else {
        input_count++;
      }
      error_count = 0;
    } catch (...) {
      error_count++;
    }
    
    if (error_count >=10) {
      error_count = 0;
      // 에러 발생 시
      // 연결 해제 보내기
      connectPub(false);
      ros::shutdown();
    } else {
      // 연결 중 보내기
      connectPub(true);
    }
    ros::spinOnce();
    loop_rate.sleep();
  }

  // 연결 해제 보내기
  usleep(100000);
  connectPub(false);
  usleep(100000);

  ret2 = instantDoCtrl->WriteBit(startPort, redO_, 0);
  // CHK_RESULT(ret2);
  ret2 = instantDoCtrl->WriteBit(startPort, buzO_, 0);
  // CHK_RESULT(ret2);
  ret2 = instantDoCtrl->WriteBit(startPort, greO_, 0);
  // CHK_RESULT(ret2);
  ret2 = instantDoCtrl->WriteBit(startPort, yelO_, 0);
  // CHK_RESULT(ret2);
  ret2 = instantDoCtrl->WriteBit(startPort, motPowO_, 0);
  ret2 = instantDoCtrl->WriteBit(startPort, liftUpO_, 0);
  ret2 = instantDoCtrl->WriteBit(startPort, liftDownO_, 0);
  ret2 = instantDoCtrl->WriteBit(startPort, pcOnO_, 0);

  ret2 = instantDoCtrl->WriteBit(startPort + portNum(mel1O_), port(mel1O_), 0);
  ret2 = instantDoCtrl->WriteBit(startPort + portNum(mel2O_), port(mel2O_), 0);
  ret2 = instantDoCtrl->WriteBit(startPort + portNum(mel3O_), port(mel3O_), 0);
  ret2 = instantDoCtrl->WriteBit(startPort + portNum(mel4O_), port(mel4O_), 0);
  ret2 = instantDoCtrl->WriteBit(startPort + portNum(mel5O_), port(mel5O_), 0);
  ret2 = instantDoCtrl->WriteBit(startPort + portNum(chargeO_), port(chargeO_), 0);

  instantDiCtrl->Dispose();
  instantDoCtrl->Dispose();

  // If something wrong in this execution, print the error code on screen for
  // tracking.
  if (BioFailed(ret))
  {
    wchar_t enumString[256];
    AdxEnumToString(L"ErrorCode", (int32)ret, 256, enumString);
    printf("Some error occurred. And the last error code is 0x%X. [%ls]\n", ret,
           enumString);
  }

  return 0;
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "USB4750");
  usb4750 usbIO;
  return usbIO.spin();
}
