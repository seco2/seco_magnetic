# -*- coding: utf-8 -*-
import rospy
import traceback
import os

from abc import *   # 추상 클래스 사용
from typing import List, Dict, Any
from std_msgs.msg import Bool
from threading import Timer

from Automation.BDaq import *
from Automation.BDaq.InstantDoCtrl import InstantDoCtrl
from Automation.BDaq.InstantDiCtrl import InstantDiCtrl
from Automation.BDaq.BDaqApi import AdxEnumToString, BioFailed


# bit 데이터 타입 가져오기
def getBitData(data_item: int) -> List[bool]:
    return [False if bin(data_item & (1 << i)) == '0b0' else True for i in range(0, 8)]


# bit 데이터 타입 설정
def setBitData(data_item: List[bool]) -> int:
    return int(''.join(['1' if i is True else '0' for i in data_item[::-1]]).rjust(8, '0')[:8], 2)


# USB4750 프레임
class USB4750Frame(metaclass=ABCMeta):
    # ROS 관련 변수
    connect_pub: rospy.Publisher     # 연결정보 보내기
    
    # 상태 변수
    agv_no: int = 0     # AGV 번호
    connect_do: bool = False    # Output 연결 여부
    connect_di: bool = False    # Input 연결 여부
    connect_flag: bool = False   # 연결 여부
    disconnect_count: int = 0   # 연결 해제 카운트

    # USB4750 관련 변수 
    deviceDescription: str = "USB-4750,BID#0"        # USB4750 이름
    profilePath: str = os.path.dirname("../profile/DemoDevice.xml")      # USB4750 매개변수 파일
    instantDoCtrl: InstantDoCtrl    # Ouput 인스턴스
    instantDiCtrl: InstantDiCtrl    # Input 인스턴스
    startPort: int = 0      # 시작 포트 번호 (0)
    portCount: int = 2      # 포트 개수 (2)
    DIs: List[bool] = [False] * 16      # Input 데이터
    DOs: List[bool] = [False] * 16      # Output 데이터

    # 생성자
    def __init__(self):
        rospy.init_node('usb4750_node')

        # ROS 설정
        self.initBasicROS()   # 기본 ROS 설정
        self.initUserROS()    # 사용자 ROS 설정

        # Set Timer
        rospy.Timer(rospy.Duration(0.1), self.writeTimerCallback)   # 쓰기 콜백
        rospy.Timer(rospy.Duration(0.1), self.readTimerCallback)    # 읽기 콜백
        
        rospy.spin()

    # 소멸자
    def __del__(self):
        if self.connect_do is True:
            # 연결이 되어있다면 Output 데이터 초기화 후 종료
            self.DOs = [False] * 16
            self.writeAllData()
            self.closeDo()

        if self.connect_di is True:
            # 연결이 되어있다면
            self.closeDi()

    # 기본 ROS 설정
    def initBasicROS(self):
        # Get Parameter
        self.agv_no = float(rospy.get_param("/agv_no", self.agv_no))       # AGV 번호

        # Set Publish
        self.connect_pub = rospy.Publisher("/USB4750/CONNECT", Bool, queue_size=10)

    # 사용자 ROS 설정
    @abstractmethod
    def initUserROS(self):
        pass

    ########################################
    #         타이머 함수 선언
    ########################################
    # 쓰기 타이머
    def writeTimerCallback(self, event):
        if self.connect_do is True:
            # 연결이 되어있다면
            self.outputDataProcess()
            self.writeAllData()
            self.sendOuputData()
        else:
            # 연결이 되어있지않다면
            self.initDo()
        self.sendConnect()      # 연결 정보 보내기

    # 읽기 타이머
    def readTimerCallback(self, event):
        if self.connect_di is True:
            # 연결이 되어있다면
            self.readAllData()
        else:
            # 연결이 되어있지않다면
            self.initDi()

    ########################################
    #        USB4750 관련 함수 선언
    ########################################
    # =========== Write(Ouput) =========== #
    # Output 인스턴스 초기화
    def initDo(self):
        # Output 인스턴스 초기화
        rospy.loginfo("[USB4750(Output)] Connecting ...")
        self.instantDoCtrl = InstantDoCtrl(self.deviceDescription) 
        self.instantDoCtrl.loadProfile = self.profilePath

        ret, _ = self.readDoData(port=0)
        if BioFailed(ret):
            # 에러 발생할 경우 닫기
            rospy.loginfo("[USB4750(Output)] Connection failed")
            self.closeDo(ret)
        else:
            rospy.loginfo("[USB4750(Output)] Connection complete")
            self.connect_do = True

    # Input 인스턴스 닫기
    def closeDo(self, ret=None):
        self.connect_do = False
        if ret is not None:
            enumStr = AdxEnumToString("ErrorCode", ret.value, 256)
            rospy.logwarn("[USB4750(Output)] Some error occurred. And the last error code is %#x. [%s]" % (ret.value, enumStr))
        self.instantDoCtrl.dispose()   # Input 인스턴스 종료

    # 비트 전체 쓰기
    def writeAllData(self):
        write_data: list = [setBitData(self.DOs[:8]), setBitData(self.DOs[8:])]
        ret = self.instantDoCtrl.writeAny(self.startPort, self.portCount, write_data)
        if BioFailed(ret):
            # 에러 발생 시 USB4750 연결 해제
            self.closeDo(ret)

    # 비트 쓰기
    def writeData(self, port: int, flag: bool):
        self.instantDoCtrl.writeBit(port//8, port%8, 1 if flag is True else 0)

    # 비트 출력 값 가져오기
    def readDoData(self, port: int):
        return self.instantDoCtrl.readBit(port//8, port%8)

    # =========== Read(Input) =========== #
    # Input 인스턴스 초기화
    def initDi(self):
        # Input 인스턴스 초기화
        rospy.loginfo("[USB4750(Input)] Connecting ...")
        self.instantDiCtrl = InstantDiCtrl(self.deviceDescription) 
        self.instantDiCtrl.loadProfile = self.profilePath

        ret, _ = self.readData(port=0)
        if BioFailed(ret):
            # 에러 발생할 경우 닫기
            rospy.loginfo("[USB4750(Input)] Connection failed")
            self.closeDi(ret)
        else:
            rospy.loginfo("[USB4750(Input)] Connection complete")
            self.connect_di = True

    # Input 인스턴스 닫기
    def closeDi(self, ret=None):
        self.connect_di = False
        if ret is not None:
            enumStr = AdxEnumToString("ErrorCode", ret.value, 256)
            rospy.logwarn("[USB4750(Input)] Some error occurred. And the last error code is %#x. [%s]" % (ret.value, enumStr))
        self.instantDiCtrl.dispose()   # Input 인스턴스 종료

    # 비트 전체 읽기
    def readAllData(self):
        ret, read_data = self.instantDiCtrl.readAny(self.startPort, self.portCount)
        if BioFailed(ret):
            # 에러 발생 시 USB4750 연결 해제
            self.closeDi(ret)
        else:
            read_data_list: list = []
            for i in read_data:
                read_data_list += getBitData(i)
            self.DIs = read_data_list
            self.inputDataProcess()  # 입력 데이터 처리
            self.sendInputData()

    # 비트 읽기
    def readData(self, port: int):
        return self.instantDiCtrl.readBit(port//8, port%8)

    ########################################
    #           ROS 관련 함수 선언
    ########################################
    # 연결 상태 보내기
    def sendConnect(self):
        if self.connect_di is False or self.connect_do is False:
            # 입력/출력 하나라도 접속해제되어있다면
            if self.disconnect_count >= 5:
                self.connect_flag = False
            else:
                self.disconnect_count += 1
        else:
            # 입력/출력 접속되어있다면
            self.connect_flag = True
            self.disconnect_count = 0

        connect_data: Bool = Bool()
        connect_data.data = self.connect_flag
        self.connect_pub.publish(connect_data)

    # 입력 데이터 처리
    @abstractmethod
    def inputDataProcess(self):
        pass

    # 출력 데이터 처리
    @abstractmethod
    def outputDataProcess(self):
        pass

    # 입력 데이터 처리
    @abstractmethod
    def sendInputData(self):
        pass

    # 출력 데이터 처리
    @abstractmethod
    def sendOuputData(self):
        pass