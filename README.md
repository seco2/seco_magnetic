# SECO AGV

[COM_PORT]
* front-wheel-moter-driver : /dev/ttyFRONT
* rear-wheel-moter-driver : /dev/ttyREAR
* IMU : /dev/ttyIMU
* PGV : /dev/ttyPGV
* BMS : /dev/ttyBMS



[IP_INFO]
#### agv#1
* pc port1: 192.168.101.101 
* pc port2: 192.168.2.101
* acsys : ?



#### agv#2
* pc port1: 192.168.101.102 
* pc port2: 192.168.2.102
* acsys : ?



#### hub
* front-lidar : 192.168.101.11 
* rear-lidar  : 192.168.101.10 
* MOXA ioLogic E1240  : 192.168.101.33
