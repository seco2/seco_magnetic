import os
import datetime
import shutil
import zipfile
import threading


# 로그 저장
def saveLogFile(log_topic: str, log_type: str, log_data: str):
    now_time: datetime.datetime = datetime.datetime.now()
    year_text: str = now_time.strftime('%Y')
    month_text: str = now_time.strftime('%m')
    day_text: str = now_time.strftime('%d')
    date_text: str = now_time.strftime('%Y-%m-%d')
    time_data: str = now_time.strftime('%Y-%m-%d %H:%M:%S')
    try:
        # 로그 폴더 확인 및 만들기
        pass
        # log_path: str = f"./log/{year_text}년/{month_text}월/{day_text}일/"
        # os.makedirs(log_path, exist_ok=True)

        # # 로그 파일 생성
        # log_path += f"[{date_text}]{log_topic}.log"
        # with open(log_path, 'a') as file:
        #     file.write(f"[{time_data}] [{log_type}] {log_data}\n")
    except Exception:
        return


# 로그 압축파일 저장
def saveLogZip():
    pass
    # threading.Thread(target=logZipProcess).start()


# 로그 압축파일 생성
def logZipProcess():
    yesterday = datetime.datetime.now() - datetime.timedelta(1)

    year_text = yesterday.strftime('%Y')
    month_text = yesterday.strftime('%m')
    day_text = yesterday.strftime('%d')

    # 로그 폴더 있는지 확인
    try:
        # 로그 폴더 확인 및 만들기
        log_path: str = f"./log/{year_text}년/{month_text}월/{day_text}일/"
        log_zip_path: str = log_path + '.zip'

        if not os.path.isdir(log_path) or os.path.exists(log_zip_path):
            # 압축 파일 경로에 폴더가 없거나 압축 파일이 있으면 종료
            return

        # 어제 일자 압축하기
        log_zips: zipfile.ZipFile = zipfile.ZipFile(log_zip_path, 'w')

        for folder, subfolders, files in os.walk(log_path):
            for file in files:
                log_zips.write(
                    os.path.join(folder, file),
                    os.path.relpath(os.path.join(folder, file), log_path),
                    compress_type=zipfile.ZIP_DEFLATED)
        log_zips.close()

        # 기존 폴더 삭제
        if os.path.exists(log_path):
            shutil.rmtree(log_path)
    except Exception:
        return
