import datetime
import socket
import sys
import time
import threading
import multiprocessing as mp
import struct

from abc import *   # 추상 클래스 사용
from typing import List, Dict, Any


# 소켓 클래스 틀
class SocketFrame(metaclass=ABCMeta):
    host: str                       # 서버 주소
    port: int                       # 포트 번호
    socket_type: str                # 소켓 타입 (예시: acs_client)
    socket_start: bool = True       # 소켓 시작 여부 (False 일 경우 종료)

    server_socket: socket = None   # 서버 소켓
    client_socket: socket = None   # 연결된 클라이언트 소켓
    client_connect: bool = False   # 소켓 클라이언트 연결 여부

    received_queue: mp.Queue = None     # 수신 데이터 큐
    send_queue: mp.Queue = None         # 송신 데이터 큐

    # 생성자
    # host : 서버 주소
    # port : 서버 포트
    # socket_type : 소켓 타입 (예시: acs_client)
    # received_queue : 받은 데이터를 저장하는 큐
    # send_queue : 서버에 전송할 데이터 큐
    def __init__(self, host: str, port: int, socket_type: str, received_queue: mp.Queue, send_queue: mp.Queue):
        self.host = host                        # 서버 주소
        self.port = port                        # 포트 번호
        self.socket_type = socket_type          # 소켓 타입 (예시: acs_client)
        self.recieved_queue = received_queue    # 수신 데이터 큐
        self.send_queue = send_queue            # 송신 데이터 큐

        self.init_socket()  # 클라이언트 초기화
        threading.Thread(target=self.send_process).start()  # 송신 처리 쓰레드
        self.recieved_process()     # 수신 처리

    # 소멸자
    def __del__(self):
        # 소켓 열려있으면 닫기
        self.close_socket()

    # 소켓 초기화 (추상 메소드)
    @abstractmethod
    def init_socket(self):
        pass

    # 소켓 닫기 (추상 메소드)
    @abstractmethod
    def close_socket(self):
        pass

    # 서버 재시작
    def client_restart(self):
        self.send_state("재시작하기 위해 종료합니다.", state_type='info')
        self.close_socket()  # 클라이언트 닫기
        self.init_socket()   # 서버 초기화

    # 수신 프로세스
    # 만약에 빈 데이터가 50개 들어오면 끊겼다고 판단하고 서버 재접속 시도
    # 예외 발생 시 서버가 끊겼다고 판단하고 서버 재접속 시도
    def recieved_process(self):
        not_data_count: int = 0
        while self.socket_start is True:
            if self.client_connect is True:
                # 클라이언트가 열려있다면
                try:
                    # 서버 수신 데이터 처리
                    received_data: bytes = self.client_socket.recv(1024)
                    if not received_data:
                        # 빈 데이터 (빈데이터가 50개 들어오면 닫혔다고 판단)
                        if not_data_count >= 50:
                            not_data_count = 0
                            raise Exception("빈 데이터가 연속으로 들어옴")
                        not_data_count += 1
                    else:
                        self.send_state(received_data, state_type='receive')
                except Exception as e:
                    if self.socket_start is True and self.client_connect is True:
                        self.send_state('데이터 수신 도중 예외가 발생하여 재시작합니다. 예외 메시지: ' + str(e), state_type='error')
                        self.client_restart()
            time.sleep(0.05)

    # 송신 프로세스
    # send_queue에 데이터가 있을 경우 해당 데이터를 송신함
    def send_process(self):
        while self.socket_start is True:
            try:
                # 서버 수신 데이터 처리
                if self.send_queue.qsize() > 0:
                    # 보낼 데이터가 들어있을 경우
                    send_data: bytes = self.send_queue.get()
                    if send_data == bytes(0):
                        # 데이터가 0 바이트로 들어왔다면 재시작
                        self.send_state('재시작 명령이 들어와 재시작 합니다.', state_type='info')
                        self.client_restart()
                        continue
                    elif send_data == bytes(1):
                        # 데이터가 1 바이트로 들어왔다면 종료
                        self.socket_start = False
                        self.close_socket()
                        return
                    else:
                        if self.client_connect is True:
                            # 클라이언트가 열려있다면 데이터 보내기
                            self.client_socket.sendall(send_data)
                        else:
                            # 닫혀있다면 다시 넣기
                            self.send_queue.put(send_data)
            except Exception as e:
                self.send_state('예외가 발생했습니다. 예외 메시지: ' + str(e), state_type='error')
            time.sleep(0.05)

    # 상태 데이터 보내기
    # type => 데이터 타입
    #     info => 소켓 메시지
    #     connect => 연결 여부
    #     error => 소켓 에러
    #     receive => 수신 데이터
    # time => 시간
    # socket_type => 소켓 타입 (예시: acs_client)
    # data => 데이터
    def send_state(self, data: any, state_type: str):
        state_data = {
            'type': state_type,
            'time': datetime.datetime.now(),
            'socket_type': self.socket_type,
            'data': data
        }
        self.recieved_queue.put(state_data)


# 소켓 클라이언트 클래스
class SocketClient(SocketFrame):

    # 클라이언트 초기화
    def init_socket(self):
        self.send_state("클라이언트를 시작합니다.", state_type='info')
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        while True:
            # 클라이언트가 서버에 접속할때까지 2초마다 반복
            time.sleep(2)
            try:
                self.send_state("서버에 연결을 시도합니다...", state_type='info')
                self.client_socket.connect((self.host, self.port))
                break
            except Exception as e:
                # 접속 실패 시 2초 후 다시 접속
                self.send_state('서버에 연결 도중 예외가 발생하였습니다. 예외 메시지: ' + str(e), state_type='error')

        # 접속 성공
        self.send_state("서버에 연결되었습니다.", state_type='info')
        self.client_connect = True
        self.send_state(True, state_type='connect')  # 연결 여부 보내기

    # 클라이언트 닫기
    def close_socket(self):
        self.client_connect = False
        self.client_socket.close()
        self.send_state(False, state_type='connect')  # 연결 여부 보내기


# 소켓 서버 클래스
class SocketServer(SocketFrame):

    # 서버 초기화
    def init_socket(self):
        self.send_state("서버를 시작합니다.", state_type='info')

        while True:
            try:
                self.send_state("서버를 설정합니다...", state_type='info')
                self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

                self.server_socket.bind((self.host, self.port))

                # 서버가 클라이언트의 접속 허용
                self.server_socket.listen()

                # accept 함수에서 대기하다가 클라이언트가 접속하면 새로운 소켓을 리턴합니다.
                self.client_socket, connect_addr = self.server_socket.accept()
                self.send_state("클라이언트가 접속 했습니다. 클라이언트 주소 :" + str(connect_addr), state_type='info')
                break
            except Exception as e:
                self.send_state("서버 설정 도중 예외가 발생했습니다. 2초 뒤 다시 설정합니다. 예외 메시지: " + str(e), state_type='error')
                time.sleep(2)

        self.client_connect = True
        self.send_state(True, state_type='connect')  # 연결 여부 보내기

    # 소켓 닫기
    def close_socket(self):
        self.client_connect = False
        self.client_socket.close()
        self.server_socket.close()
        self.send_state(False, state_type='connect')  # 연결 여부 보내기


# ====================================================
#                  소켓 수신 처리 부분
# ====================================================
# 데이터 분리
def separationData(received_data: bytes) -> Dict:
    data_object: Dict = {
        'protocol_code': received_data[0:8].decode(),       # 프로토콜 코드
        'send_time': received_data[8:22].decode(),          # 보낸 시간
        'length': int(received_data[22:26].decode()),       # 길이
        'data': bytearray(received_data[30:])               # 받은 데이터
    }
    return data_object


# get_data => 가져올 데이터 (딕셔너리 리스트 형태)
#   item_type (데이터 타입) : char, float, bit, int, num, bool
#   length (해당 데이터 총 길이[byte])
def getProtocolData(protocol_data: bytearray, get_data_info: List = []) -> List[Any]:
    return_data: List[Any] = []
    first_index: int = 0      # 가져올 첫번째 인덱스
    last_index: int = 0       # 가져올 마지막 인덱스
    process_data: bytearray = bytearray()

    for get_item_info in get_data_info:
        first_index = last_index
        last_index += get_item_info['length']
        process_data = protocol_data[first_index:last_index]

        if get_item_info['item_type'] == 'char':
            # char 데이터 타입이라면
            return_data.append(getCharData(process_data))
        elif get_item_info['item_type'] == 'float':
            # float 데이터 타입이라면
            return_data.append(getFloatData(process_data))
        elif get_item_info['item_type'] == 'bit':
            # bit 데이터 타입이라면
            return_data.append(getBitData(process_data))
        elif get_item_info['item_type'] == 'int':
            # int 데이터 타입이라면
            return_data.append(getIntData(process_data))
        elif get_item_info['item_type'] == 'num':
            # 문자형 숫자 데이터 타입이라면
            return_data.append(getNumData(process_data))
        elif get_item_info['item_type'] == 'bool':
            # 문자형 Bool 데이터 타입이라면
            return_data.append(getBoolData(process_data))
        else:
            # 데이터 타입 알 수 없음
            raise Exception("데이터 타입을 알 수 없습니다.")
    return return_data


# char 데이터 타입 가져오기
def getCharData(data_item: bytearray) -> str:
    return data_item.decode()


# float 데이터 타입 가져오기
def getFloatData(data_item: bytearray) -> float:
    return round(struct.unpack('f', data_item)[0], 4)


# bit 데이터 타입 가져오기
def getBitData(data_item: bytearray) -> List[bool]:
    return [False if bin(ord(data_item) & (1 << i)) == '0b0' else True for i in range(0, 8)]


# int 데이터 타입 가져오기
def getIntData(data_item: bytearray) -> int:
    return struct.unpack("B" if len(data_item) == 1 else ">H", data_item)[0]


# Num 타입 가져오기 (문자형 숫자)
def getNumData(data_item: bytearray) -> int:
    return int(data_item)


# Bool 타입 가져오기 (문자형 Bool)
def getBoolData(data_item: bytearray) -> bool:
    return True if getCharData(data_item) == "1" else False


# Word 데이터 가져오기 (2바이트 bit) [MC 프로토콜]
def getWordData(data_item: int) -> List[bool]:
    # bytearray 로 바꾸기
    int_data: bytearray = setIntData(data_item, 2)

    bit_data: list = []
    for i in int_data:
        bit_data += getBitData(setIntData(i))

    return bit_data


# ====================================================
#                  소켓 송신 처리 부분
# ====================================================
# 빈 공간 채우기
def setSpareData(byte_data: bytearray, length: int) -> bytearray:
    len_data = len(byte_data)
    if len_data >= length:
        # 데이터 길이가 총 길이보다 크거나 같으면
        byte_data = byte_data[:length]
    else:
        # 그렇지 않다면
        byte_data += bytearray(length - len_data)
    return byte_data


# 헤더 설정
def setHeader(protocol_code: str, now_time: datetime.datetime, length: int) -> bytearray:
    header_data: bytearray = setCharData(protocol_code, 8)
    header_data += setCharData(now_time.strftime('%Y%m%d%H%M%S'), 14)
    header_data += setNumData(length, 4)
    return setSpareData(header_data, 30)


# 데이터 설정
# code => 프로토콜 코드
#     AGV000 = 소켓 메시지
# now_time => 현재 시간
# length => 총 길이
# send_data => 보낼 데이터 (딕셔너리 리스트 형태)
#   item_type (데이터 타입) : char, float, bit, int, num, bool
#   length (해당 데이터 총 길이[byte])
#   data_item (데이터)
def setProtocolData(protocol_code: str, length: int = 30, send_data: List = [], header_flag: bool = True) -> Dict:
    protocol_data: bytearray = bytearray()
    process_flag: bool = True   # 처리 여부
    now_time: datetime.datetime = datetime.datetime.now()
    log_msg: str = f"프로토콜 코드: {protocol_code}, 보낼 데이터: {str(send_data)}"

    try:
        if header_flag is True:
            # 헤더 여부가 True일 경우 헤더 설정
            protocol_data += setHeader(protocol_code, now_time, length)
        protocol_data += setSocketData(send_data)
        protocol_data = setSpareData(protocol_data, length)
    except Exception as e:
        # 에러 메시지 설정
        process_flag = False
        log_msg += f", 에러 메시지: {str(e)}"

    return {"protocol_data": protocol_data, "process_flag": process_flag, "log_msg": log_msg}


# 소켓 데이터 설정
def setSocketData(send_data: List = []) -> bytearray:
    protocol_data: bytearray = bytearray()
    for send_item in send_data:
        if send_item['item_type'] == 'char':
            # char 데이터 타입이라면
            protocol_data += setCharData(send_item['data_item'], send_item['length'])
        elif send_item['item_type'] == 'float':
            # float 데이터 타입이라면
            protocol_data += setFloatData(send_item['data_item'])
        elif send_item['item_type'] == 'bit':
            # bit 데이터 타입이라면
            protocol_data += setBitData(send_item['data_item'], send_item['length'])
        elif send_item['item_type'] == 'int':
            # int 데이터 타입이라면
            protocol_data += setIntData(send_item['data_item'], send_item['length'])
        elif send_item['item_type'] == 'num':
            # 문자형 숫자 데이터 타입이라면
            protocol_data += setNumData(send_item['data_item'], send_item['length'])
        elif send_item['item_type'] == 'bool':
            # 문자형 Bool 데이터 타입이라면
            protocol_data += setBoolData(send_item['data_item'])
        else:
            raise Exception("데이터 타입을 알 수 없습니다.")
    return protocol_data


# char 데이터 타입 설정
def setCharData(data_item: Any, length: int) -> bytearray:
    return bytearray(str(data_item).ljust(length, '0')[:length].encode())


# float 데이터 타입 설정
def setFloatData(data_item: float) -> bytearray:
    return bytearray(struct.pack('f', data_item))


# bit 데이터 타입 설정
def setBitData(data_item: List[bool], length: int) -> bytearray:
    return setIntData(int(''.join(['1' if i is True else '0' for i in data_item[::-1]]).rjust(8, '0')[:8], 2), length)


# int 데이터 타입 설정
def setIntData(data_item: int, length: int = 1) -> bytearray:
    return bytearray(struct.pack("B" if length == 1 else ">H", data_item))


# Num 타입 설정 (문자형 숫자)
def setNumData(data_item: int, length: int):
    return setCharData(format(data_item, f'0{length}'), length)


# Bool 타입 설정 (문자형 Bool)
def setBoolData(data_item: bool, length: int = 1):
    return setCharData("1" if data_item is True else "0", length)


# Word 타입 설정 (2바이트 bit) [MC 프로토콜]
def setWordData(data_item: List[bool]) -> int:
    return int(''.join(['1' if i is True else '0' for i in data_item[::-1]]).rjust(16, '0')[:16], 2)
