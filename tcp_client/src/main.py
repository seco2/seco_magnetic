#!/usr/bin/env python3.8
import datetime
import rospy
import multiprocessing as mp
import os

from tcp_socket import *
from log_process import *
from typing import Dict, List

from geometry_msgs.msg import Pose2D
from std_msgs.msg import Bool, UInt8, Int32MultiArray, Float64MultiArray
from move_control.msg import teleop, cmdMove
from motor_driver_msgs.msg import Command
from usb_dio.msg import io_in, io_out, lift_ctrl, mag_detect, mag_mark
from obstacle_detector.msg import obstacles_detect
from tabos_bms.msg import bat_data


class TCPClient:
    # ====================================================
    #                    변수 선언 부분
    # ====================================================
    agv_no: int = 1                     # AGV 번호
    acs_ip_address: str = "192.168.2.100"   # ACS IP 주소
    acs_port_number: int = 7200         # ACS 포트 번호
    yesterday_day: int = datetime.datetime.now().day     # 어제 일자
    now_time: datetime.datetime = datetime.datetime.now()  # 현재 시간
    acs_emergency_stop: bool = False    # ACS 비상 정지 여부
    acs_emergency_mode: bool = False    # ACS 비상 모드 여부
    cmd_emergency_stop: bool = False    # 자동 이동 비상 정지 여부
    precision_mode: bool = False        # 정밀 모드 여부
    emergency_stop: bool = False        # 비상 정지 여부
    wheel_moter_run: List[bool] = [False, False, False, False]  # 바퀴 작동 여부
    lift_moter_run: bool = False        # 리프트 작동 여부
    manual_mode: bool = True            # 자동 모드 여부 (False = 메뉴얼 모드)
    cmd_flag: bool = False              # 명령 시작 여부 (명령 수행 시작시 True)
    work_flag: bool = False             # 작업 진행 여부
    cmd_number: int = 0                 # 명령 번호
    chg_flag: bool = False              # 충전 여부
    bump_flag: bool = False             # 범퍼 감지 여부
    ems_flag: bool = False              # 비상 정지 감지 여부
    move_error: bool = False            # 자동 이동 에러 여부 (마그네틱 이탈 여부)
    mag_count: int = 0
    lift_detect_flag: list = [False, False, False, False]     # 리프트 센서 감지 여부
    lift_limit: dict = {
        'up': False,
        'down': False
    }
    magnetic_detect_flag: dict = {      # 마그네틱 인식 여부
        'front': False,
        'rear': False,
        'left': False,
        'right': False
    }
    mark_detect_flag: dict = {      # 마그네틱 마크 인식 여부
        'front': False,
        'decel': False,
        'left': False,
    }
    obstacle_detector_stop: bool = False         # Obstacle 감지 정지 여부 (LRF 감지)
    obstacle_detector_slow: bool = False         # Obstacle 감지 슬로우 여부 (LRF 감지)
    cmd_precise_mode: bool = False         # 자동 정밀 모드 여부 (slow)
    joy_emergency_stop: bool = False        # 조이스틱 비상 정지 여부
    joy_emergency_mode: bool = False        # 조이스틱 비상 모드 여부
    joy_precise_mode: bool = False         # 조이스틱 정밀 모드 여부
    
    restart_btn_time: datetime.datetime = datetime.datetime.now()   # 재시작 버튼 ON 시간

    # ====================================================
    #                    AGV 위치 부분
    # ====================================================
    current_pose: Pose2D = Pose2D()     # 현재 위치
    current_pose.x = 0.0                # AGV 현재 X 축
    current_pose.y = 0.0                # AGV 현재 Y 축
    current_pose.theta = 0.0            # AGV 현재 방향

    # ====================================================
    #                  배터리 상태 부분
    # ====================================================
    bat_volt: float = 0.0               # 배터리 전압
    bat_current: float = 0.0            # 배터리 전류

    bat_volt_lift: float = 0.0               # 배터리 전압
    bat_current_lift: float = 0.0            # 배터리 전류 
    # ====================================================
    #                   연결 상태 부분
    # ====================================================
    joy_connect: bool = False               # 조이스틱 연결 여부
    usb4750_connect: bool = True            # USB4750 연결 여부
    moxa_connect: bool = True               # MOXA 모듈 연결 여부
    lrf_front_connect: bool = True          # LRF 앞 센서 연결 여부
    lrf_rear_connect: bool = True           # LRF 뒤 센서 연결 여부
    wheel_front_connect: bool = True        # 바퀴 앞 연결 여부
    wheel_rear_connect: bool = True         # 바퀴 뒤 연결 여부
    
    joy_connect_count: int = 0                  # 조이스틱 연결 카운트
    usb4750_connect_count: int = 0              # USB4750 연결 카운트
    moxa_connect_count: int = 0                 # MOXA 모듈 연결 카운트
    lrf_front_connect_count: int = 0            # LRF 앞 센서 연결 카운트
    lrf_rear_connect_count: int = 0             # LRF 뒤 센서 연결 카운트
    wheel_front_connect_count: int = 0          # 바퀴 앞 연결 카운트
    wheel_rear_connect_count: int = 0           # 바퀴 뒤 연결 카운트

    # ====================================================
    #                AGV 모터, 전원 기록 부분
    # ====================================================
    moter_dist_data: List = [0.0, 0.0, 0.0, 0.0, 0.0]
    agv_power_data: List = [0, 0, 0, 0]

    # ====================================================
    #                  ROS Publish 부분
    # ====================================================
    cmd_pub: rospy.Publisher                    # 자동 명령 Publish
    acs_emergency_stop_pub: rospy.Publisher     # ACS 비상 정지 Publish
    acs_emergency_mode_pub: rospy.Publisher     # ACS 비상 모드 Publish
    chg_pub: rospy.Publisher                    # 충전 Publish

    # ====================================================
    #                  소켓 송/수신 부분
    # ====================================================
    tcp_client_processing: mp.Queue
    healthy_receive_time: datetime.datetime = datetime.datetime.now()   # healthy 전문 수신 시간
    healthy_send_time: datetime.datetime = datetime.datetime.now()      # healthy 전문 송신 시간
    agv_state_info_send_time: datetime.datetime = datetime.datetime.now()    # AGV 상태 전문 송신 시간

    client_send_queue: mp.Queue = mp.Queue()          # 송신 데이터 큐
    client_received_queue:  mp.Queue = mp.Queue()     # 수신 데이터 큐
    agv_client_connect = False                        # 클라이언트 연결 상태

    def __init__(self):
        rospy.init_node('tcp_client')

        # 변수 설정
        rate = rospy.Rate(10)           # 10hz

        # Parameter 가져오기
        self.agv_no = int(rospy.get_param("/agv_no", self.agv_no))
        self.acs_ip_address = str(rospy.get_param("/acs_ip_address", self.acs_ip_address))
        self.acs_port_number = int(rospy.get_param("/acs_port_number", self.acs_port_number))

        # Publish 설정
        self.cmd_pub = rospy.Publisher('/MOVE/CMD', cmdMove, queue_size=10)                         # 명령 Publish
        self.acs_emergency_stop_pub = rospy.Publisher('/EMERGENCY/ACS', Bool, queue_size=10)        # 비상 정지 Publish
        self.cmd_emergency_stop_pub = rospy.Publisher('/EMERGENCY/CMD', Bool, queue_size=10)        # 자동이동 비상 정지 Publish
        self.acs_emergency_mode_pub = rospy.Publisher('/EMERGENCY/MODE', Bool, queue_size=10)       # 비상 모드 Publish
        self.chg_pub = rospy.Publisher('/CHG', UInt8, queue_size=10)                                # 충전 Publish

        # Subscriber 설정
        rospy.Subscriber("/cmd_vel_joy", teleop, self.cmdVelJoyCallback)            # 조이스틱 명령값 (수동모드인지 자동모드 파악)
        rospy.Subscriber("/lift/ctrl", lift_ctrl, self.liftCtrlCallback)            # 리프트 작동 여부 가져오기
        rospy.Subscriber("/FRONT/cmd", Command, self.cmdFrontCallback)              # 앞 모터 작동 여부 가져오기
        rospy.Subscriber("/REAR/cmd", Command, self.cmdRearCallback)                # 뒤 모터 작동 여부 가져오기
        rospy.Subscriber("/MOVE/STATE", cmdMove, self.moveStateCallback)            # 자동 이동 상태 가져오기
        rospy.Subscriber("/EMERGENCY/STATE", Bool, self.emergencyStateCallback)     # 비상 정지 여부 가져오기
        rospy.Subscriber("/joy/CONNECT", Bool, self.joyConnectCallback)             # 조이스틱 연결 상태 가져오기
        rospy.Subscriber("/joy/PRECISE", Bool, self.joyPreciseCallback)             # 조이스틱 정밀 모드 상태 가져오기
        rospy.Subscriber("/FRONT/CONNECT", Bool, self.connectFrontCallback)         # 앞 모터 연결 여부 가져오기
        rospy.Subscriber("/REAR/CONNECT", Bool, self.connectRearCallback)           # 뒤 모터 연결 여부 가져오기
        rospy.Subscriber("/USB4750/CONNECT", Bool, self.usb4750ConnectCallback)     # USB4750 연결 여부 가져오기
        rospy.Subscriber("/moxa/connect", Bool, self.moxaConnectCallback)           # Moxa e1240 연결 여부 가져오기
        rospy.Subscriber("/front/scanner/CONNECT", Bool, self.lrfFrontConnectCallback)  # LRF 앞 센서 연결 여부 가져오기
        rospy.Subscriber("/rear/scanner/CONNECT", Bool, self.lrfRearConnectCallback)    # LRF 뒤 센서 연결 여부 가져오기
        rospy.Subscriber("/io_in", io_in, self.ioinCallback)            # 범퍼, 비상 정지 버튼 상태 가져오기
        rospy.Subscriber("/io_out", io_out, self.ioOutCallback)         # 충전 상태 가져오기
        rospy.Subscriber("/obstacle_info", obstacles_detect, self.obstacleDetectCallback)             # 물체 감지 여부
        rospy.Subscriber("/obstacle_info/slow", obstacles_detect, self.obstacleDetectSlowCallback)    # 슬로우 구간 물체 감지 여부
        rospy.Subscriber("/RECORD/MOTER", Float64MultiArray, self.recodeMoterCallback)       # 바퀴 이동 거리
        rospy.Subscriber("/RECORD/POWER", Int32MultiArray, self.recodePowerCallback)     # 전원 ON 시간
        rospy.Subscriber("/MOVE/PRECISE", Bool, self.cmdPreciseCallback)                # 자동 정밀 모드 여부
        rospy.Subscriber("/EMERGENCY/JOY/MODE", Bool, self.emergencyJoyModeCallback)     # 조이스틱 비상 모드 여부
        rospy.Subscriber("/EMERGENCY/JOY", Bool, self.emergencyJoyStopCallback)          # 조이스틱 비상 정지 여부
        rospy.Subscriber("/main_pw/battery_data", bat_data, self.tabosBMSCallback)        # 배터리 정보
        rospy.Subscriber("/lift_pw/battery_data", bat_data, self.tabosBMSLiftCallback)        # 배터리 정보
        rospy.Subscriber("/MAG/DETECT", mag_detect, self.magDetectCallback)         # 마그네틱 인식 여부
        rospy.Subscriber("/MAG/MARK", mag_mark, self.magMarkCallback)               # 마그네틱 마크 인식 여부
        rospy.Subscriber("/USB4750/restart", Bool, self.restartCallback)            # 재시작 버튼 여부

        # Set dir
        data_path: str = "/home/sis/seco_ws/data/"
        os.makedirs(data_path, exist_ok=True)
        os.chdir(data_path)

        # 소켓 클라이언트 시작
        self.tcp_client_processing = mp.Process(target=self.socket_client_start, args=(
            self.acs_ip_address, self.acs_port_number+self.agv_no, "acs_client", self.client_received_queue, self.client_send_queue))
        self.tcp_client_processing.start()

        while not rospy.is_shutdown():
            self.update()
            rate.sleep()

        # 노드 종료 시 소켓 프로세스 종료
        self.tcp_client_processing.kill()

    # 반복 작업
    def update(self):
        self.now_time = datetime.datetime.now()  # 현재 시간
        self.socketDataProcess()      # 소켓 데이터 처리
        self.timerCheck()              # 타이머 체크

    # 타이머 체크
    # 2초 마다 Healthy 전문 송신
    # 1초 마다 AGV 상태 송신
    # 10초 지나도록 Healthy 전문 수신 못받을 시 재접속
    def timerCheck(self):
        healthy_send_diff: datetime.datetime = self.now_time - self.healthy_send_time           # agv healthy 전문 송신 시간 차이
        healthy_receive_diff: datetime.datetime = self.now_time - self.healthy_receive_time     # acs healthy 전문 수신 시간 차이
        agv_state_info_send_diff: datetime.datetime = self.now_time - self.agv_state_info_send_time  # AGV 상태 정보 전문 수신 시간 차이

        if self.agv_client_connect is True and healthy_send_diff.seconds >= 2:
            # healthy 전문 보내고 2초 지나면 송신
            self.sendDataProcess(f"SCAGV{self.agv_no}00")
            self.healthy_send_time = self.now_time
            self.cmdStateSend()

        if self.agv_client_connect is True and agv_state_info_send_diff.seconds >= 1:
            # healthy 전문 보내고 2초 지나면 송신
            self.agvStateSend()
            self.agv_state_info_send_time = self.now_time

        if self.agv_client_connect is True and healthy_receive_diff.seconds >= 10:
            # 10초 지나도록 acs 전문 수신 못하면 재접속
            self.agv_client_connect = False
            self.client_send_queue.put(bytes(0))

            log_data: str = f"10초가 지나도록 ACS Healthy 전문을 받지 못했습니다. TCP 서버 재접속 요청합니다."
            saveLogFile(log_topic='error', log_type='acs_client', log_data=log_data)
        
        if self.yesterday_day != self.now_time.day:
            # 오늘 날짜와 어제 날짜가 다를 경우 로그 파일 압축
            self.yesterday_day = self.now_time.day
            saveLogZip()

    # ====================================================
    #                  소켓 송/수신 부분
    # ====================================================
    # L2 소켓 서버 실행
    @staticmethod
    def socket_client_start(host: str, port: int, socket_type: str, received_queue: mp.Queue, send_queue: mp.Queue):
        print("소켓 클라이언트 실행")
        SocketClient(host, port, socket_type, received_queue, send_queue)

    # 소켓 데이터 처리 함수
    # type => 데이터 타입
    #     info => 소켓 메시지
    #     connect => 연결 여부
    #     error => 소켓 에러
    #     receive => 수신 데이터
    # time => 시간
    # socket_type => 소켓 타입 (예시: acs_client)
    # data => 데이터
    def socketDataProcess(self):
        while self.client_received_queue.qsize() > 0:
            # 수신 데이터가 있을 경우
            received_data: Dict = self.client_received_queue.get()
            socket_data: any = received_data['data']
            data_type: str = received_data['type']
            socket_type: str = received_data['socket_type']
        
            try:
                if data_type == 'error':
                    # 타입이 에러 타입이라면
                    saveLogFile(log_topic='error', log_type=socket_type, log_data=socket_data)
                elif data_type == 'receive':
                    # 타입이 수신 데이터라면
                    saveLogFile(log_topic=socket_type, log_type=f'{socket_type}_receive', log_data=f"수신 데이터: {str(socket_data)}")
                    self.receivedDataProcess(received_data)
                elif data_type == 'info':
                    # 타입이 정보 알림일 경우
                    saveLogFile(log_topic=socket_type, log_type=socket_type, log_data=socket_data)
                elif data_type == 'connect':
                    # 타입이 연결 정보일 경우
                    self.agv_client_connect = socket_data
                    if self.agv_client_connect is True:
                        # 연결 시 송수신 시간 초기화
                        self.healthy_receive_time = self.now_time
                        self.healthy_send_time = self.now_time
                        self.agv_state_info_send_time = self.now_time
                else:
                    raise Exception("소켓 데이터의 타입을 알 수 없습니다.")
            except Exception as e: 
                # 예외 발생 시
                log_data: str = f"소켓 수신 데이터를 처리 중 예외가 발생했습니다\n예외 메시지: {e}\n소켓 데이터: {str(received_data)}"
                saveLogFile(log_topic='error', log_type=socket_type, log_data=log_data)

    # ====================================================
    #                  소켓 수신 처리 부분
    # ====================================================
    # 수신 데이터 처리 함수
    def receivedDataProcess(self, received_data: Dict):
        data_object: Dict = separationData(received_data["data"])
        
        if data_object["protocol_code"] == f"SCAGV{self.agv_no}10":
            # Healthy 전문 수신
            self.healthyReceive()
        elif data_object["protocol_code"] == f"SCAGV{self.agv_no}11":
            # 명령 지시 수신
            self.cmdReceive(data_object["data"])
        elif data_object["protocol_code"] == f"SCAGV{self.agv_no}12":
            # 비상 정지 명령 수신
            self.emergencyStopReceive(data_object["data"])
        elif data_object["protocol_code"] == f"SCAGV{self.agv_no}13":
            # 비상 모드 명령 수신
            self.emergencyModeReceive(data_object["data"])
        elif data_object["protocol_code"] == f"SCAGV{self.agv_no}14":
            # 충전 명령 수신
            self.chargeReceive(data_object["data"])
        elif data_object["protocol_code"] == f"SCAGV{self.agv_no}15":
            # 자동 이동 비상 정지 명령 수신
            self.cmdEmergencyStopReceive(data_object["data"])
        else:
            raise Exception(f"프로토콜 코드를 알 수 없습니다.\n수신한 프로토콜 코드: {data_object['protocol_code']}")

    # Healthy 전문 수신 (SCAGV010)
    def healthyReceive(self):
        self.healthy_receive_time = self.now_time

    # 명령 지시 수신 (SCAGV011)
    def cmdReceive(self, cmd_data: bytearray):
        get_data_info: List[Dict] = [
            {"item_type": 'bool', "length": 1},        # 명령 지시 여부
            {"item_type": 'int', "length": 1},         # 작업 수행 타입
            {"item_type": 'int', "length": 1},         # 작업 수행 번호
            {"item_type": 'float', "length": 4},       # x
            {"item_type": 'float', "length": 4},       # y
            {"item_type": 'float', "length": 4},       # 각도
            {"item_type": 'bool', "length": 1},        # 정밀모드 상태
            {"item_type": 'int', "length": 1},         # 마그네틱 인식 개수
            {"item_type": 'bool', "length": 1},        # decel flag
        ]
        get_data_list: List[Any] = getProtocolData(cmd_data, get_data_info)
        
        self.move_error = False
        cmd_data: cmdMove = cmdMove()
        if get_data_list[0] is True:
            # 명령 지시 일 경우
            cmd_data.move = 1
            cmd_data.type = get_data_list[1]
            cmd_data.number = get_data_list[2]
            cmd_data.precise = get_data_list[6]
            cmd_data.X = get_data_list[3]
            cmd_data.Y = get_data_list[4]
            cmd_data.A = get_data_list[5]
            cmd_data.mag_count = get_data_list[7]
            cmd_data.decel_flag = get_data_list[8]

            # 명령 수행중
            self.cmd_flag = True     

            # 명령 지시 시작 보내기
            self.work_flag = False
            self.cmd_number = get_data_list[2] 
            self.cmdStateSend()
        else:
            # 명령 지시 정지 일 경우
            cmd_data.move = 0
            cmd_data.type = 0
            cmd_data.number = 0
            cmd_data.precise = False
            cmd_data.X = 0.0
            cmd_data.Y = 0.0
            cmd_data.A = 0.0
            cmd_data.mag_count = 0
            cmd_data.decel_flag = False
            self.cmd_flag = False
        self.cmd_pub.publish(cmd_data)

    # 비상 정지 명령 수신 (SCAGV012)
    def emergencyStopReceive(self, cmd_data: bytearray):
        get_data_info: List[Dict] = [
            {"item_type": 'bool', "length": 1}        # 비상 정지 여부
        ]
        get_data_list: List[Any] = getProtocolData(cmd_data, get_data_info)
        self.acs_emergency_stop = get_data_list[0]
        self.acs_emergency_stop_pub.publish(Bool(self.acs_emergency_stop))

    # 비상 모드 명령 수신 (SCAGV013)
    def emergencyModeReceive(self, cmd_data: bytearray):
        get_data_info: List[Dict] = [
            {"item_type": 'bool', "length": 1}        # 비상 모드 여부
        ]
        get_data_list: List[Any] = getProtocolData(cmd_data, get_data_info)
        self.acs_emergency_mode = get_data_list[0]
        self.acs_emergency_mode_pub.publish(Bool(self.acs_emergency_mode))

    # 충전 명령 수신 (SCAGV014)
    def chargeReceive(self, cmd_data: bytearray):
        get_data_info: List[Dict] = [
            {"item_type": 'bool', "length": 1}        # 충전 명령 여부
        ]
        get_data_list: List[Any] = getProtocolData(cmd_data, get_data_info)
        self.chg_flag = get_data_list[0]
        self.chg_pub.publish(UInt8(1 if self.chg_flag is True else 0))

    # 비상 정지 명령 수신 (SCAGV015)
    def cmdEmergencyStopReceive(self, cmd_data: bytearray):
        get_data_info: List[Dict] = [
            {"item_type": 'bool', "length": 1}        # 비상 정지 여부
        ]
        get_data_list: List[Any] = getProtocolData(cmd_data, get_data_info)
        self.cmd_emergency_stop = get_data_list[0]
        self.cmd_emergency_stop_pub.publish(Bool(self.cmd_emergency_stop))

    # ====================================================
    #                  소켓 송신 처리 부분
    # ====================================================
    # 송신 데이터 처리 함수
    def sendDataProcess(self, protocol_code: str):
        protocol_data: Dict = {}
        if protocol_code == f"SCAGV{self.agv_no}00":
            # Healthy 전문 송신
            protocol_data = self.setHealthySendData(protocol_code)
        elif protocol_code == f"SCAGV{self.agv_no}01":
            # AGV 상태 전문 송신
            protocol_data = self.setAgvStateSendData(protocol_code)
        elif protocol_code == f"SCAGV{self.agv_no}02":
            # 명령 수행 시작/종료 전문 송신
            protocol_data = self.setCmdStateSendData(protocol_code)
        elif protocol_code == f"SCAGV{self.agv_no}03":
            # 명령 재시작 전문 송신
            protocol_data = self.setRestartSendData(protocol_code)
        # elif protocol_code == f"SCAGV{self.agv_no}03":
        #     # 명령 수행 에러 전문 송신
        #     protocol_data = self.setCmdErrorSendData(protocol_code)
        else:
            protocol_data = {"process_flag": False, "log_msg": f"프로토콜 코드를 알 수 없습니다. (입력한 프로토콜 코드: {protocol_code}"}

        if protocol_data["process_flag"] is True:
            # 전문 보내기
            self.client_send_queue.put(bytes(protocol_data["protocol_data"]))
            saveLogFile(log_topic='acs_client', log_type='acs_client_send', log_data=protocol_data["log_msg"])
        else:
            # 예외 발생 시
            saveLogFile(log_topic='error', log_type='acs_client_send', log_data=protocol_data["log_msg"])

    # Healthy 전문 송신 데이터 설정 (SCAGV000)
    def setHealthySendData(self, protocol_code: str) -> Dict:
        return setProtocolData(protocol_code)

    # AGV 상태 전문 송신 데이터 설정 (SCAGV001)
    #   item_type (데이터 타입) : char, float, bit, int, num
    #   length (해당 데이터 총 길이[byte])
    #   data_item (데이터)
    def setAgvStateSendData(self, protocol_code: str) -> Dict:
        send_data: List[Dict] = [
            {"item_type": 'char', "length": 1, "data_item": "0"},        # Spare
            {"item_type": 'char', "length": 1, "data_item": "0"},        # Spare
            {"item_type": 'char', "length": 1, "data_item": "0"},        # Spare
            {"item_type": 'bit', "length": 1, "data_item": [     # AGV 상태 관련
                self.manual_mode,          # 자동 모드 여부
                self.chg_flag,           # 충전 여부
                True if True in self.wheel_moter_run else False,      # 바퀴 구동 여부 (하나라도 동작 한다면 True)
                self.lift_moter_run,     # 리프트 모터 구동 여부
                self.cmd_flag,           # 자동 명령 여부
                self.emergency_stop,     # 비상 정지 여부
                self.acs_emergency_stop,   # ACS 비상 정지 여부
                self.acs_emergency_mode,   # ACS 비상 모드 여부
            ]},    
            {"item_type": 'float', "length": 4, "data_item": self.bat_volt},           # 배터리 전압
            {"item_type": 'float', "length": 4, "data_item": self.bat_current},        # 배터리 전류
            {"item_type": 'int', "length": 1, "data_item": self.cmd_number},              # 자동 명령 번호
            {"item_type": 'bit', "length": 1, "data_item": [     # 비상 정지 / 센서 감지 관련 1
                self.lift_detect_flag[0],   # 리프트 감지 1
                self.lift_detect_flag[1],   # 리프트 감지 2
                self.lift_detect_flag[2],   # 리프트 감지 3
                self.lift_detect_flag[3],   # 리프트 감지 4
                self.lift_limit['up'],      # 리프트 업 리미트
                self.lift_limit['down'],    # 리프트 다운 리미트
                self.bump_flag,          # 범퍼 감지 여부
                self.ems_flag,           # 비상 스위치 여부
            ]},    
            {"item_type": 'bit', "length": 1, "data_item": [     # 비상 정지 / 센서 감지 관련 2
                self.obstacle_detector_stop,  # LRF 감지 (정지 범위)
                self.obstacle_detector_slow,  # LRF 감지 (슬로우 범위)
                self.cmd_precise_mode,        # 자동 정밀 모드 여부 (slow)
                self.move_error,              # 자동 이동 에러 발생 여부 (마그네틱 이탈)    
                self.cmd_emergency_stop,      # 자동 이동 비상 정지 여부
            ]}, 
            {"item_type": 'bit', "length": 1, "data_item": [     # 센서 연결 여부
                self.wheel_front_connect,           # 앞 모터 연결 여부
                self.wheel_rear_connect,            # 뒤 모터 연결 여부
                self.moxa_connect,            # e1240 연결 여부
                self.usb4750_connect,               # USB4750 연결 여부
                self.lrf_front_connect,             # LRF 앞 센서 연결 여부
                self.lrf_rear_connect,              # LRF 뒤 센서 연결 여부
                self.joy_connect,     # 조이스틱 연결 여부
            ]}, 
            {"item_type": 'bit', "length": 1, "data_item": [     # 마그네틱 인식 관련
                self.magnetic_detect_flag['front'],       # 전방 마그네틱 인식 여부
                self.magnetic_detect_flag['rear'],        # 후방 마그네틱 인식 여부
                self.magnetic_detect_flag['left'],        # 좌측 마그네틱 인식 여부
                self.magnetic_detect_flag['right'],       # 우측 마그네틱 인식 여부
                self.mark_detect_flag['front'],           # 전방 마크 인식 여부
                self.mark_detect_flag['decel'],           # 감속 마크 인식 여부
                self.mark_detect_flag['left'],            # 좌측 마크 인식 여부
            ]}, 
            {"item_type": 'float', "length": 4, "data_item": self.moter_dist_data[0]},        # 앞 왼쪽 바퀴 이동 거리
            {"item_type": 'float', "length": 4, "data_item": self.moter_dist_data[1]},        # 앞 오른쪽 바퀴 이동 거리
            {"item_type": 'float', "length": 4, "data_item": self.moter_dist_data[2]},        # 뒤 왼쪽 바퀴 이동 거리
            {"item_type": 'float', "length": 4, "data_item": self.moter_dist_data[3]},        # 뒤 오른쪽 바퀴 이동 거리
            {"item_type": 'int', "length": 2, "data_item": self.agv_power_data[0]},           # 전원 ON 일
            {"item_type": 'int', "length": 1, "data_item": self.agv_power_data[1]},           # 전원 ON 시
            {"item_type": 'int', "length": 1, "data_item": self.agv_power_data[2]},           # 전원 ON 분
            {"item_type": 'int', "length": 1, "data_item": self.agv_power_data[3]},           # 전원 ON 초
            {"item_type": 'bit', "length": 1, "data_item": [     # AGV 조이스틱 관련
                self.joy_emergency_stop,                         # 조이스틱 비상 정지 여부
                self.joy_emergency_mode,                         # 조이스틱 비상 모드 여부
                self.joy_precise_mode,                           # 조이스틱 정밀 모드 여부
            ]}, 
        ]

        # 충전 중일때 바퀴 동작 시 충전 해제
        if True in self.wheel_moter_run and self.chg_flag is True:
            self.chg_flag = False
            self.chg_pub.publish(UInt8(1 if self.chg_flag is True else 0))
            
        return setProtocolData(protocol_code=protocol_code, length=90, send_data=send_data)

    # 명령 수행 시작/종료 전문 송신 데이터 설정 (SCAGV002)
    def setCmdStateSendData(self, protocol_code: str) -> Dict:
        send_data: List[Dict] = [
            {"item_type": 'bool', "length": 1, "data_item": self.work_flag},    # 자동 명령 여부
            {"item_type": 'int', "length": 1, "data_item": self.cmd_number},    # 자동 명령 번호
            {"item_type": 'int', "length": 1, "data_item": self.mag_count},     # 마그네틱 인식 개수
            {"item_type": 'bool', "length": 1, "data_item": self.move_error},     # 에러 여부
        ]
        return setProtocolData(protocol_code=protocol_code, length=40, send_data=send_data)

    # 명령 수행 에러 전문 송신 데이터 설정 (SCAGV003)
    def setRestartSendData(self, protocol_code: str) -> Dict:
        return setProtocolData(protocol_code)

    # # 명령 수행 에러 전문 송신 데이터 설정 (SCAGV003)
    # def setCmdErrorSendData(self, protocol_code: str) -> Dict:
    #     send_data: List[Dict] = [
    #         {"item_type": 'int', "length": 1, "data_item": self.cmd_number},    # 자동 명령 번호
    #     ]
    #     return setProtocolData(protocol_code=protocol_code, length=40, send_data=send_data)

    # Healthy 전문 송신 (SCAGV000)
    def healthySend(self):
        self.sendDataProcess(f"SCAGV{self.agv_no}00")

    # AGV 상태 전문 송신 (SCAGV001)
    def agvStateSend(self):
        self.sendDataProcess(f"SCAGV{self.agv_no}01")

    # 명령 수행 시작/종료 전문 송신 (SCAGV002)
    def cmdStateSend(self):
        self.sendDataProcess(f"SCAGV{self.agv_no}02")
    
    # 명령 재시작 전문 송신 (SCAGV003)
    def restartSend(self):
        self.sendDataProcess(f"SCAGV{self.agv_no}03")

    # # 명령 수행 에러 전문 송신 (SCAGV003)
    # def cmdErrorSend(self):
    #     self.sendDataProcess(f"SCAGV{self.agv_no}03")

    # ====================================================
    #                  콜백 함수 정의 부분
    # ====================================================
    # 조이스틱 값 받아와 자동 모드 여부 설정
    def cmdVelJoyCallback(self, joy_data: teleop):
        # control_mode가 1 일 경우 메뉴얼 모드
        if joy_data.control_mode == 1:
            # 메뉴얼 모드로 전환 시 메뉴얼 모드 설정
            self.manual_mode = True
        else:
            self.manual_mode = False

    # 컨베이어 작동 여부 가져오기
    def liftCtrlCallback(self, lift_ctrl_data: lift_ctrl):
        self.lift_moter_run = lift_ctrl_data.run 

    # 앞 바퀴 작동 여부 가져오기
    def cmdFrontCallback(self, cmd_front_data: Command):
        self.wheel_moter_run[0] = True if cmd_front_data.speed_left != 0 else False
        self.wheel_moter_run[1] = True if cmd_front_data.speed_right != 0 else False

    # 뒷 바퀴 작동 여부 가져오기
    def cmdRearCallback(self, cmd_rear_data: Command):
        self.wheel_moter_run[2] = True if cmd_rear_data.speed_left != 0 else False
        self.wheel_moter_run[3] = True if cmd_rear_data.speed_right != 0 else False

    # 자동 이동 상태 가져오기
    # def moveStateCallback(self, move_state_data: cmdMove):
    #     pre_work_flag: bool = self.work_flag
    #     current_work_flag = True if move_state_data.type == 4 else False
    #     # self.cmd_flag = True if move_state_data.move == 1 else False  # 명령 시작 여부 가져오기
    #     self.cmd_number = move_state_data.number      # 명령 번호 가져오기
    #     if current_work_flag != pre_work_flag:
    #         # 명령 수행 여부가 다를 경우 전문 전송
    #         print("send")
    #         self.cmdStateSend()
    #     self.work_flag = current_work_flag
        
    # 자동 이동 상태 가져오기
    def moveStateCallback(self, move_state_data: cmdMove):
        if self.cmd_flag is True:
            # 명령 수행 중이라면
            # self.cmd_flag = True if move_state_data.move == 1 else False  # 명령 시작 여부 가져오기
            # self.cmd_number = move_state_data.number      # 명령 번호 저장
            if move_state_data.type == 8 and self.cmd_number == move_state_data.number:
                # 위치 도달할 경우 송신
                self.work_flag = True
                self.mag_count = 0
                self.cmdStateSend()
            elif move_state_data.move == 2 and self.move_error is False:
                # 에러 발생 시 에러 발생 송신
                self.move_error = True
                self.cmdStateSend()
            # elif self.work_flag is True and self.cmd_number == move_state_data.number:
            #     # 위치 도달했는데 명령 번호가 그대로라면 다시 송신
            #     self.cmdStateSend()
            elif self.mag_count != move_state_data.mag_count:
                # 마그네틱 카운트 수와 다르고 인식이 풀렸을때 송신
                self.mag_count = move_state_data.mag_count
                self.cmdStateSend()

        elif move_state_data.move != 0 and move_state_data.move != 2:
            # 명령 수행 중이지 않다면 강제 정지 (안전 장치)
            cmd_data: cmdMove = cmdMove()
            cmd_data.move = 0
            cmd_data.type = 0
            cmd_data.number = 0
            cmd_data.precise = False
            cmd_data.X = 0.0
            cmd_data.Y = 0.0
            cmd_data.A = 0.0
            cmd_data.mag_count = 0
            cmd_data.decel_flag = False
            self.cmd_pub.publish(cmd_data)

        if move_state_data.move == 2:
            # 에러 발생 시
            self.move_error = True
        else:
            self.move_error = False
        
    # 비상 정지 상태 가져오기
    def emergencyStateCallback(self, emergency_state_data: Bool):
        self.emergency_stop = emergency_state_data.data

    # 조이스틱 연결 여부 가져오기
    def joyConnectCallback(self, joy_connect_data: Bool):
        if joy_connect_data.data is True:
            # 연결이 되어있으면
            self.joy_connect_count = 0
            self.joy_connect = True
        else:
            # 연결이 되어있지 않다면
            if self.joy_connect_count >= 5:
                # 카운트가 5 이상이면
                self.joy_connect = False
            else:
                self.joy_connect_count += 1

    # 앞 바퀴 연결 여부 가져오기
    def connectFrontCallback(self, connect_front_data: Bool):
        if connect_front_data.data is True:
            # 연결이 되어있으면
            self.wheel_front_connect_count = 0
            self.wheel_front_connect = True
        else:
            # 연결이 되어있지 않다면
            if self.wheel_front_connect_count >= 5:
                # 카운트가 5 이상이면
                self.wheel_front_connect = False
            else:
                self.wheel_front_connect_count += 1

    # 뒤 바퀴 연결 여부 가져오기
    def connectRearCallback(self, connect_rear_data: Bool):
        if connect_rear_data.data is True:
            # 연결이 되어있으면
            self.wheel_rear_connect_count = 0
            self.wheel_rear_connect = True
        else:
            # 연결이 되어있지 않다면
            if self.wheel_rear_connect_count >= 5:
                # 카운트가 5 이상이면
                self.wheel_rear_connect = False
            else:
                self.wheel_rear_connect_count += 1

    # USB4750 연결 여부 가져오기
    def usb4750ConnectCallback(self, usb4750_connect_data: Bool):
        if usb4750_connect_data.data is True:
            # 연결이 되어있으면
            self.usb4750_connect_count = 0
            self.usb4750_connect = True
        else:
            # 연결이 되어있지 않다면
            if self.usb4750_connect_count >= 5:
                # 카운트가 5 이상이면
                self.usb4750_connect = False
            else:
                self.usb4750_connect_count += 1

    # MOXA 모듈 연결 여부 가져오기
    def moxaConnectCallback(self, moxa_connect_data: Bool):
        if moxa_connect_data.data is True:
            # 연결이 되어있으면
            self.moxa_connect_count = 0
            self.moxa_connect = True
        else:
            # 연결이 되어있지 않다면
            if self.moxa_connect_count >= 5:
                # 카운트가 5 이상이면
                self.moxa_connect = False
            else:
                self.moxa_connect_count += 1

    # LRF 센서 앞 연결 여부 가져오기
    def lrfFrontConnectCallback(self, lrf_front_connect_data: Bool):
        if lrf_front_connect_data.data is True:
            # 연결이 되어있으면
            self.lrf_front_connect_count = 0
            self.lrf_front_connect = True
        else:
            # 연결이 되어있지 않다면
            if self.lrf_front_connect_count >= 5:
                # 카운트가 5 이상이면
                self.lrf_front_connect = False
            else:
                self.lrf_front_connect_count += 1

    # LRF 센서 뒤 연결 여부 가져오기
    def lrfRearConnectCallback(self, lrf_rear_connect_data: Bool):
        if lrf_rear_connect_data.data is True:
            # 연결이 되어있으면
            self.lrf_rear_connect_count = 0
            self.lrf_rear_connect = True
        else:
            # 연결이 되어있지 않다면
            if self.lrf_rear_connect_count >= 5:
                # 카운트가 5 이상이면
                self.lrf_rear_connect = False
            else:
                self.lrf_rear_connect_count += 1

    # 범퍼, 비상 정지 버튼 상태 가져오기
    def ioinCallback(self, ioin_data: io_in):
        self.bump_flag = True if ioin_data.bump != 0 else False     # 범퍼 센서 상태 가져오기
        self.ems_flag = True if ioin_data.emc != 0 else False       # 비상 정지 버튼 상태 가져오기

        # 리프트 감지 센서 가져오기
        self.lift_detect_flag = [
            True if ioin_data.limSen1 != 0 else False,
            True if ioin_data.limSen2 != 0 else False,
            True if ioin_data.limSen3 != 0 else False,
            True if ioin_data.limSen4 != 0 else False
        ]

        # 리프트 리미트 센서 가져오기
        self.lift_limit['up'] = True if ioin_data.lim_up != 0 else False
        self.lift_limit['down'] = True if ioin_data.lim_down != 0 else False

    # 충전 상태 가져오기
    def ioOutCallback(self, ioOut: io_out):
        self.chg_flag = True if ioOut.chg == 1 else False

    # 물체 감지 여부
    def obstacleDetectCallback(self, obstacle_detect_data: obstacles_detect):
        self.obstacle_detector_stop = False if obstacle_detect_data.number == 0 else True

    # 슬로우 구간 물체 감지 여부
    def obstacleDetectSlowCallback(self, obstacle_detect_slow_data: obstacles_detect):
        self.obstacle_detector_slow = False if obstacle_detect_slow_data.number == 0 else True

    # 모터 이동거리 가져오기
    def recodeMoterCallback(self, recode_moter_data: Float64MultiArray):
        self.moter_dist_data = recode_moter_data.data

    # 전원 ON 시간 가져오기
    def recodePowerCallback(self, recode_power_data: Int32MultiArray):
        self.agv_power_data = recode_power_data.data

    # 자동 정밀 모드 여부 가져오기
    def cmdPreciseCallback(self, precise_data: Bool):
        self.cmd_precise_mode = precise_data.data
        
    # 조이스틱 비상 정지 여부
    def emergencyJoyStopCallback(self, joyStopData):
        self.joy_emergency_mode = joyStopData.data

    # 조이스틱 비상 모드 여부
    def emergencyJoyModeCallback(self, joyModeData):
        self.joy_emergency_stop = joyModeData.data

    # 조이스틱 정밀모드 여부
    def joyPreciseCallback(self, joyPreciseData):
        self.joy_precise_mode = joyPreciseData.data

    # main 배터리 정보 가져오기
    def tabosBMSCallback(self, tabos_bms_data):
        self.bat_volt = tabos_bms_data.voltage
        self.bat_current = tabos_bms_data.current
    
    # lift 배터리 정보 가져오기
    def tabosBMSLiftCallback(self, tabos_bms_data):
        self.bat_volt_lift = tabos_bms_data.voltage
        self.bat_current_lift = tabos_bms_data.current

    # 마그네틱 인식 여부
    def magDetectCallback(self, mag_detect_data: mag_detect):
        self.magnetic_detect_flag['front'] = mag_detect_data.front
        self.magnetic_detect_flag['rear'] = mag_detect_data.rear
        self.magnetic_detect_flag['left'] = mag_detect_data.left
        self.magnetic_detect_flag['right'] = mag_detect_data.right

    # 마그네틱 마크 인식 여부
    def magMarkCallback(self, mag_mark_data: mag_mark):
        self.mark_detect_flag['front'] = mag_mark_data.front
        self.mark_detect_flag['decel'] = mag_mark_data.decel
        self.mark_detect_flag['left'] = mag_mark_data.left

    # 재시작 버튼 여부 가져오기
    def restartCallback(self, restart_data):
        if restart_data.data is True:
            if self.restart_btn_time + datetime.timedelta(seconds=5) <= self.now_time:
                self.restartSend()
            self.restart_btn_time = self.now_time

if __name__ == '__main__':
    TCPClient()