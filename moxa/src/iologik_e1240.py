#!/usr/bin/env python3.8

#
# https://pymodbus.readthedocs.io/en/latest/source/example/synchronous_client.html?highlight=ModbusClient#synchronous-client-example
#

import rospy
import time
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
from std_msgs.msg import Bool, Float64MultiArray


class IologikE1240:
    ip_address: str = '192.168.127.254'
    port: int = 502
    modbus_flag = False             # 모드버스 접속 여부

    client: ModbusClient            # 모드버스 클라이언트

    magnetic_x_pub: rospy.Publisher
    magnetic_y_pub: rospy.Publisher
    connect_pub: rospy.Publisher

    bit_hz: int = 2**15

    def __init__(self):
        rospy.init_node('iologik_e1240')

        # Get Parameter
        self.ip_address = str(rospy.get_param("~ip_address", self.ip_address))    # IP 주소
        self.port = int(rospy.get_param("~port", self.port))                # port 주소

        rate = rospy.Rate(10)               # 10hz

        # Set Publish
        self.magnetic_pub = rospy.Publisher("/MAG", Float64MultiArray, queue_size=10)
        self.connect_pub = rospy.Publisher("/moxa/connect", Bool, queue_size=10)

        # 모드버스 설정
        self.set_modbus_init()

        # Loop
        while not rospy.is_shutdown():
            if self.modbus_flag is True:
                # 모드버스가 열려있으면
                self.update()
            else:
                # 닫혀있으면 연결 시도
                self.set_modbus_init()
            rate.sleep()

    # 모드버스 초기화
    def set_modbus_init(self):
        while True:
            try:
                self.client = ModbusClient(self.ip_address, port=self.port)
                self.client.connect()
                self.modbus_flag = True
                print('connect')
                break
            except Exception:
                self.modbus_flag = False
                self.moxa_connect_publish(False)
                print("not connect")
                time.sleep(1)

    # 반복 프로세스
    def update(self):
        try:
            readAnalog = self.client.read_input_registers(0, 4, unit=0x1)

            # 에러라면 Exception 발생
            if readAnalog.isError():
                raise Exception(readAnalog)

            # 마그네틱 값 가져오기
            magnetic_data: Float64MultiArray = Float64MultiArray()
            magnetic_data.data = [
                self.convert_raw_data(((readAnalog.getRegister(0)-self.bit_hz)*-1)+self.bit_hz),        # front
                self.convert_raw_data(readAnalog.getRegister(1)),   # rear
                self.convert_raw_data(((readAnalog.getRegister(2)-self.bit_hz)*-1)+self.bit_hz),    # left
                self.convert_raw_data(readAnalog.getRegister(3)),   # right
            ]

            self.magnetic_pub.publish(magnetic_data)
            self.moxa_connect_publish(True)

        except Exception as e:
            print("error: ", e)
            self.modbus_flag = False
            self.moxa_connect_publish(False)
        
    # 연결 여부 publish
    def moxa_connect_publish(self, flag):
        flag_data = Bool()
        flag_data.data = flag
        self.connect_pub.publish(flag_data)

    # 원시 데이터 변환
    # return round((((raw_data*(5/(2**15)))-5)*35)/1000, 4)
    def convert_raw_data(self, raw_data: int) -> float:
        return raw_data*(5/(2**15))


if __name__=='__main__':
    IologikE1240()