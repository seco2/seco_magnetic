/**
*@filename : bms_tabos.cpp
*/
#include "tabos_bms.h"

bms::bms_tabos::bms_tabos()
  : connected_(false), serial_(NULL), bms_thread_(NULL), port_("/dev/ttyUSB3"), baud_(19200)
{
  ros::NodeHandle nhLocal("~");
  nhLocal.param<std::string>("port", port_, port_);
  nhLocal.param<int32_t>("baud", baud_, baud_);
  nhLocal.param<int32_t>("battery_address", batteryAddress,0);
  //printf("Port: %s, Baud: %d \n",port_.c_str(), baud_);

  //timer[0]=n.createTimer(ros::Duration(1.0),&request_timer);
  //read_thread_=new boost::thread(boost::bind(&readMessage));

  pub_batteryData=nh_.advertise<tabos_bms::bat_data>("/battery_data", 1);
}

bms::bms_tabos::~bms_tabos() {
}


bool bms::bms_tabos::run_bms()
{
  if(connected_== false) return false;

  if(bms_thread_==NULL)
  {
   bms_thread_= new boost::thread(boost::bind(&bms::bms_tabos::readMessage,this));
  }

  return true;
}

void bms::bms_tabos::stop_bms()
{
  is_running_=false;
  if(bms_thread_!=NULL){
    bms_thread_->join();
    delete bms_thread_;
    bms_thread_=NULL;
  }
}


uint8_t bms::bms_tabos::calculateChecksum(uint8_t *data, int length)
{
	int sum=0;
	printf("\n");
	for(int i=0; i<length; i++){
		sum+=data[i];	
	}		

	return sum&0xFF;
}


//////////////////////////////////////////////////////////////////////
/// \fn       void send_command()     
/// \brief    command
///              'Header' + 'Header' +'Header'+'Header' + 0x01 + '1' + 1 + 0 + 33
///               (Header:4) + addr(1) + cmd(1) + datasize(1) + data
//////////////////////////////////////////////////////////////////////


bool bms::bms_tabos::req_status()
{
  bool result=false;
  int32_t txCount=0;
  uint8_t *cmdBuf=NULL;
  uint8_t calCRC=0;
  uint16_t cmdSize=sizeof(header_t)+sizeof(reqState_t)+sizeof(tail_t);
  uint8_t  pCRC=cmdSize-3;

  cmdBuf=new uint8_t [cmdSize];
  memset(&cmdBuf[0],0x00,cmdSize);

  header_t *header=(header_t *)&cmdBuf[0];
  tail_t *tail=(tail_t *)&cmdBuf[pCRC];
  reqState_t *body=(reqState_t *)&cmdBuf[sizeof(header_t)];

  header->start[0]=0xAF;
  header->start[1]=0xFA;

  header->length=sizeof(reqState_t)+3;
  header->address=batteryAddress+0x60;
  header->command=REQ_STATUS;  //SET_COMMAND
  header->order=header->address;

  data1.fields.reserved=0; //: 1;
  data1.fields.temperature=1;
  data1.fields.discharge_complete_time=1; //1;
  data1.fields.charging_complete_time=1; //:1;
  data1.fields.state=1; //: 1;
  data1.fields.SOC=1;
  data1.fields.current=1; //1; //BIT:1
  data1.fields.voltage=1;  //BIT:0

  data2.fields.reserved=0; //:5;
  data2.fields.residual_energy=1; //: //1;
  data2.fields.residual_capacity=1; //:1;
  data2.fields.SOH=1; //:1;

  body->kind1.Data=data1.Data&0xFF;
  body->kind2.Data=data2.Data&0xFF;

  calCRC =calculateChecksum(&cmdBuf[2],(cmdSize-5));
  tail->CRC=(calCRC&0xFF);
  tail->end[0]=0xAF;
  tail->end[1]=0xA0;

  txCount=send_data(&cmdBuf[0],cmdSize);
	if(txCount >= cmdSize ) {
		result=true;
	}else{
		result=false;
	}
	delete cmdBuf;

	return result;
}



int bms::bms_tabos::parse_message(unsigned char *data)
{	
  header_t *header=NULL;
  tail_t *tail=NULL;

  int data_len_ = 20;
  int header_len_ = 6;
  int calCRC = 0;

  header=(header_t *)&data[0];
  if((header->start[0] == 0xAF) && (header->start[1] == 0xFA) && (header->address == (batteryAddress+0x60)))
  {

    switch(header->command)
    {
      case 0x03:
      {
         int cmdSize=sizeof(header_t)+data_len_+sizeof(tail_t);
         int pCRC=cmdSize-3;

         calCRC=calculateChecksum(&data[2],(cmdSize-5));

         tail_t *tail=(tail_t *)&data[pCRC];
         res_data *res_data_=(res_data *)&data[sizeof(header_t)];

         if((header->order==(0x60+batteryAddress)) && (calCRC == tail->CRC) && (tail->end[0] == 0xAF) && (tail->end[1] == 0xA0))
         {

           voltage_ = conv_hex2dec(res_data_->voltage)/100.0f;
           current_ = conv_hex2dec(res_data_->current)/100.0f;
           soc_ = conv_hex2dec(res_data_->soc);
           status_ = conv_hex2dec(res_data_->status);
           chag_comp_time_ = conv_hex2dec(res_data_->charge_completed_time);
           dischag_comp_time_ = conv_hex2dec(res_data_->discharge_completed_time);
           temp_ = conv_hex2dec(res_data_->temperature)/10.0f;
           soh_ = conv_hex2dec(res_data_->soh);
           res_cap_ = conv_hex2dec(res_data_->residual_capacity)/100.0f;
           res_ene_ = conv_hex2dec(res_data_->residual_energy)/10.0f;

           res_bat_status bat_status;
           bat_status.Data = res_data_->status;

           over_voltage_ = bat_status.fields.over_voltage;
           low_voltage_ = bat_status.fields.low_voltage;
           over_charging_ampher_ = bat_status.fields.over_charging_ampher;
           over_discharging_ampher_ = bat_status.fields.over_discharging_ampher;
           high_temperature_ = bat_status.fields.high_temperature;
           low_temperature_ = bat_status.fields.low_temperature;
           bmu_error_ = bat_status.fields.bmu_error;

         }
         else {
           voltage_ = 0;
           current_ = 0;
           soc_ = 0;
           status_ = 0;
           chag_comp_time_ = 0;
           dischag_comp_time_ = 0;
           temp_ = 0;
           soh_ = 0;
           res_cap_ = 0;
           res_ene_ = 0;

           over_voltage_ = false;
           low_voltage_ = false;
           over_charging_ampher_ = false;
           over_discharging_ampher_ = false;
           high_temperature_ = false;
           low_temperature_ = false;
           bmu_error_ = false;
         }

      }
      break;
    case 0x1F:
      {
         ROS_INFO("Error Mes");
      }
      break;
    default: break;
    };

  }
  else {
    printf("No data \n");
  }

}

void bms::bms_tabos::publish_bat_data()
{
  tabos_bms::bat_data BatteryData;

  BatteryData.voltage=voltage_;
  BatteryData.current=current_;

  BatteryData.soc=soc_;
  BatteryData.status=status_;

  BatteryData.charge_completed_time=chag_comp_time_;
  BatteryData.discharge_completed_time=dischag_comp_time_;

  BatteryData.temperature=temp_;
  BatteryData.soh=soh_;

  BatteryData.residual_capacity=res_cap_;
  BatteryData.residual_energy=res_ene_;

  BatteryData.over_voltage =  over_voltage_;
  BatteryData.low_voltage =  low_voltage_;
  BatteryData.over_charging_ampher =  over_charging_ampher_;
  BatteryData.over_discharging_ampher =  over_discharging_ampher_;
  BatteryData.high_temperature =  high_temperature_;
  BatteryData.low_temperature =  low_temperature_;
  BatteryData.bmu_error =  bmu_error_;

  pub_batteryData.publish(BatteryData);
}

/**
 * @function: void readMessage()
 * @brief   : 
 */	
void bms::bms_tabos::readMessage()
{
  int result; int i=0;
  unsigned char RECVBUF[30]={0,};
  int rlen=0;
  is_running_=true;

    while(is_running_)
    {
      if(serial_==NULL) break;
      if(serial_->isOpen()==false) break;

      if((result=serial_->read((uint8_t *)&RECVBUF[i],BMS_LEN-rlen)) > 0)
      {
          rlen+=result;
          if(rlen <BMS_LEN){
            i=rlen;
            continue;
          }else{
            //Verify packet heading information
            if(RECVBUF[0]!=0xAF || RECVBUF[1]!=0xFA)
            {
              i=0;rlen=0;
              memset(RECVBUF,0x00,30);
              continue;
            }
            parse_message((unsigned char *)&RECVBUF[0]);
          }
      }
      i=0;rlen=0;
      memset(RECVBUF,0x00,30);
            usleep(5000);  //5ms
    } //while()
   is_running_ = false;
}


int32_t bms::bms_tabos::send_data(uint8_t *data, int32_t length)
{

	int32_t result=0;
  //int32_t txCount=0;
  if(serial_!=NULL)
  {
    if(serial_->isOpen()==true)
    {
      result=serial_->write((const uint8_t *)&data[0],length);
    }
  }

  if(result <= 0)
  {
    printf("ERROR!!!! \n");
  }
  else
  {
    //printf("Write!!!");
  }

  return result;
}

bool bms::bms_tabos::connect()
{

  if (!serial_) serial_ = new serial::Serial();

  serial::Timeout to(serial::Timeout::simpleTimeout(500));
  serial_->setTimeout(to);
  serial_->setPort(port_.c_str());
  serial_->setBaudrate(baud_);

  printf(" name=%s baud_=%d \n",port_.c_str(),baud_);

    for (int tries = 0; tries < 5; tries++) {

      try {
          serial_->open();
      } catch (serial::IOException) {

      }

    if (serial_->isOpen()) {
        connected_ = true;
        printf("Port open \n");
        return true;

    } else {
        connected_ = false;
        ROS_INFO("Bad Connection with serial port Error %s,%d",port_.c_str(),baud_);
        return false;
    }
    }
}

bool bms::bms_tabos::disconnect()
{

  if(serial_)
  {

      if (serial_->isOpen())
      {
          printf("CLOSED!! \n");
          serial_->close();
          connected_ = false;
      }

      serial_->close();
      connected_ = false;

      delete serial_;
      serial_=NULL;
  }
}

int bms::bms_tabos::run()
{

  ros::Rate loop_rate(5);

  while (ros::ok())
  {
    connect();
    if (connected_)
    {
      ROS_DEBUG("Successfully connect to serial device \n");
      /*
      bool req_result = req_status();
      if(req_result) ROS_DEBUG("Sent request successfully");
      else ROS_DEBUG("Cannot request bms");
      usleep(500000);
      */
      while (ros::ok())
      {
        bool req_result = req_status();
        if(req_result)
         {
            run_bms();
            publish_bat_data();
        }
        ros::spinOnce();
        loop_rate.sleep();
      }
    }
    else
    {
      ROS_DEBUG("Problem connecting to serial device.");
      ROS_ERROR_STREAM_ONCE("Problem connecting to port " << port_ << ". Trying again every 1 second.");
      sleep(1);
    }
  }
  stop_bms();
  disconnect();
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "~");
  bms::bms_tabos tabos_bat_management;
  tabos_bat_management.run();
  return 0;
}
