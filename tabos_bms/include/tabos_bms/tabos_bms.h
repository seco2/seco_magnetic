#include <assert.h>
#include <math.h>
#include <iostream>
#include "ros/ros.h"
#include "ros/time.h"
#include "boost/thread/thread.hpp"
//#include "serial_port/lightweightserial.h"
#include "serial/serial.h"
#include "std_msgs/UInt8.h"
#include "std_msgs/Float32.h"

#include "yaml-cpp/yaml.h"
#include <iostream>
#include <fstream>

#include "tabos_bms/bat_data.h"

#define MAX_SAVE_BUF_SIZE 256
#define MAX_BUFFER 2048
#define BMS_LEN 29

using namespace std;

namespace bms {

enum{
REQ_STATUS=0x01,
SET_COMMAND=0x02,
RSP_STATUS=0x03,
ERROR_REPORT=0x1F,
};

typedef union {
  uint8_t Data;
  struct{
      uint8_t voltage:1;  //BIT:0
      uint8_t current: 1; //BIT:1
      uint8_t SOC: 1;
      uint8_t state: 1;
      uint8_t charging_complete_time:1;
      uint8_t discharge_complete_time:1;
      uint8_t temperature: 1;
      uint8_t reserved: 1;
  }fields;
}kindReg1_t;

typedef union {
    uint8_t Data;
    struct{
      uint8_t SOH:1;               //Bit0
      uint8_t residual_capacity:1; //Bit1
      uint8_t residual_energy: 1;  //Bit2
      uint8_t reserved:5;
    }fields;
}kindReg2_t;

typedef struct {
  uint8_t start[2];
  uint8_t address;
  uint8_t length;
  uint8_t command;
  uint8_t order;
}header_t;


typedef struct {
  uint8_t data[10];
}Body_t;


typedef struct {
  uint8_t CRC;
  uint8_t end[2];
}tail_t;

typedef struct {
  kindReg1_t kind1;
  kindReg2_t kind2;
}reqState_t;


typedef union {
    uint16_t Data;
    struct{
      bool over_voltage:1; //bit 0
      bool low_voltage:1;
      bool over_charging_ampher:1;
      bool over_discharging_ampher:1;
      bool high_temperature;
      bool low_temperature: 1;
      bool bmu_error: 1;
      bool reserved:9;
    }fields;
}res_bat_status;

typedef struct {
  uint16_t voltage;
  uint16_t current;

  uint16_t soc;
  uint16_t status;

  uint16_t charge_completed_time;
  uint16_t discharge_completed_time;

  uint16_t temperature;
  uint16_t soh;

  uint16_t residual_capacity;
  uint16_t residual_energy;
}res_data;

uint16_t conv_hex2dec(uint16_t in)
{
  return (((in&0xFF00)>>8)|((in&0x00FF)<<8));
}
class bms_tabos
{
public:
  bms_tabos();
  ~bms_tabos();
  int run();

private:
  ros::NodeHandle nh_;

  ros::Publisher pub_batteryData;
  //ros::Timer timer[2];


  boost::thread *bms_thread_;
  //LightweightSerial* serial_;
  serial::Serial  *serial_;

  bool connected_ = false;
  bool is_running_ = false;
  std::string port_;
  int baud_;

  kindReg1_t  data1;
  kindReg2_t  data2;
  res_data *res_data_;


  uint8_t SAVE[MAX_BUFFER];
  uint16_t rxBufCount=0;
  uint16_t rxBufPos=0;

  int32_t batteryAddress=0;

  char receviedBuf[1000]={0x00,};
  int   receviedByte=0;
  uint8_t  saveBuf[MAX_SAVE_BUF_SIZE];
  uint16_t pBufEnd=0;  //count
  uint16_t pBufStr=0;

  uint8_t *cmdBuf=NULL;
  uint16_t cmdSize;
  uint8_t  pCRC;

  float voltage_;
  float current_;
  int16_t soc_;
  int16_t status_;
  int16_t chag_comp_time_;
  int16_t dischag_comp_time_;
  float temp_;

  int16_t  soh_;
  float res_cap_;
  float res_ene_;

  bool over_voltage_;
  bool low_voltage_;
  bool over_charging_ampher_;
  bool over_discharging_ampher_;
  bool high_temperature_;
  bool low_temperature_;
  bool bmu_error_;

  // functions
  bool connect();
  bool disconnect();
  bool run_bms();
  void stop_bms();
  bool req_status();
  int parse_message(unsigned char *data);
  void publish_bat_data();
  void readMessage();
  uint8_t calculateChecksum(uint8_t *data, int length);
  int32_t send_data(uint8_t *data, int32_t length);
};

}




