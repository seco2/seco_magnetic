﻿#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>
#include <std_msgs/String.h>
#include "ros/time.h"

#include "move_control/WheelSpeed.h"
//#include <move_control/reset_odom.h>




class OdomTrans
{
public:
	OdomTrans();
	~OdomTrans() {};

  void publishOdom(const ros::TimerEvent& e);
  // void publish_abnormal_message(int flag, float gyro, float encoder);
  void AGV_StateCallback(const move_control::WheelSpeed wheel_data);
//	void resetOdomCallback(const move_control::reset_odom::ConstPtr& Data);
private:
	ros::NodeHandle n;
	boost::mutex publish_mutex_;
    double x, y, th;
    double vx, vy, vth;
    double dt;

    double AGV_len_;
    double AGV_width_;
    double wheel_radius_;
    double wheel_width_;
	double md_encoder_pulse_;
	double gear_ratio_;
    double max_rpm_;
    bool pub_tf;
    double scale_x,scale_y, scale_theta;
    tf::TransformBroadcaster odom_broadcaster;

    ros::Subscriber sub_motorData; //
	ros::Publisher odom_pub_;
//	ros::Subscriber reset_odom_sub_;
	ros::Time current_time, last_time;
	ros::Timer timer;

};



OdomTrans::OdomTrans() : x(.0), y(.0), th(.0), AGV_len_(1.77), AGV_width_(1.404), wheel_width_(0.18), wheel_radius_(0.173), pub_tf(false),scale_x(1),scale_y(1), scale_theta(1)
{
  ros::NodeHandle nhLocal("~");
  nhLocal.param("AGV_length", AGV_len_, AGV_len_);
  nhLocal.param("AGV_width", AGV_width_, AGV_width_);
  nhLocal.param("wheel_radius", wheel_radius_, wheel_radius_);
  nhLocal.param("gear_ratio", gear_ratio_, gear_ratio_);
  nhLocal.param("wheel_width", wheel_width_, wheel_width_);
  nhLocal.param("scale_x", scale_x, scale_x);
  nhLocal.param("scale_y", scale_y, scale_y);
  nhLocal.param("scale_theta", scale_theta, scale_theta);
  nhLocal.param("pub_tf", pub_tf, pub_tf);


  odom_pub_ = n.advertise<nav_msgs::Odometry>("odom_encoder", 30, true);

  sub_motorData = n.subscribe<move_control::WheelSpeed>("/WheelSpeed", 10, &OdomTrans::AGV_StateCallback, this);
//	reset_odom_sub_ = n.subscribe<move_control::reset_odom>("reset_odom", 10, &OdomTrans::resetOdomCallback, this);

  timer = n.createTimer(ros::Duration(0.05), &OdomTrans::publishOdom, this);

  ROS_INFO("wheel_distance:%lf\n", AGV_len_);
  ROS_INFO("wheel_radius:%lf\n", wheel_radius_);
  ROS_INFO("gear_ratio:%f\n", gear_ratio_);
  ROS_INFO("pub_tf:%d\n", pub_tf);
}



void OdomTrans::AGV_StateCallback(const move_control::WheelSpeed wheel_data)
{

	boost::mutex::scoped_lock lock(publish_mutex_);

  current_time = wheel_data.header.stamp;
  dt = (current_time - last_time).toSec();


  double scale_value = (2 * M_PI * wheel_radius_) / (60*gear_ratio_);

  double vfl = (double) (wheel_data.front_left_speed * scale_value); // speed in m/s
  double vfr = (double) (wheel_data.front_right_speed * scale_value);
  double vrl = (double) (wheel_data.rear_left_speed * scale_value);
  double vrr = (double) (wheel_data.rear_right_speed * scale_value);

	/*printf("data : %f, %f, %f, %f\n", vfl, vfr, vrl, vrr);*/

  vx = (1*vfl + 1*vfr + 1*vrl + 1*vrr) / 4 *scale_x;
  vy = (1*vfl + (-1)*vfr + (-1)*vrl + 1*vrr) / 4 *scale_y;

  vth = (1*vfl + (-1)*vfr + 1*vrl + (-1)* vrr) / ((AGV_len_+AGV_width_)* 2) * scale_theta;

  double delta_x = ((vx*cos(th)) - (vy*sin(th))) * dt; // linear x direction change (m)
  double delta_y = ((vx*sin(th)) + (vy*cos(th))) * dt; // linear y direction change (m)
  double delta_th_en = vth*dt;                         // encoder angular change (rad)



  x += delta_x;
  y += delta_y;
  th += delta_th_en;

  last_time = current_time;
}

void OdomTrans::publishOdom(const ros::TimerEvent& e)
{

    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(th);

    geometry_msgs::TransformStamped odom_trans;
    odom_trans.header.stamp = current_time;
    // odom_trans.header.stamp = ros::Time::now();
    odom_trans.header.frame_id = "odom";
    odom_trans.child_frame_id = "base_link";

    odom_trans.transform.translation.x = x;
    odom_trans.transform.translation.y = y;
    odom_trans.transform.translation.z = 0.0;
    odom_trans.transform.rotation = odom_quat;
    if(pub_tf)
    {    
         // tf::TransformBroadcaster odom_broadcaster;
          odom_broadcaster.sendTransform(odom_trans);
    }

    nav_msgs::Odometry odom;
    odom.header.stamp = current_time;
    //odom.header.stamp = ros::Time::now();
    odom.header.frame_id = "odom";

    odom.pose.pose.position.x = x;
    odom.pose.pose.position.y = y;
    odom.pose.pose.orientation = odom_quat;

    odom.child_frame_id = "base_link";
    odom.twist.twist.linear.x = vx;
    odom.twist.twist.linear.y = vy;
    odom.twist.twist.angular.z = vth;

    odom_pub_.publish(odom);
  }

/*
void OdomTrans::resetOdomCallback(const move_control::reset_odom::ConstPtr& Data)
{
	x = 0;
	y = 0;
	th = 0;
	printf("RESET CALLBACK\n");
}
*/



int main(int argc, char** argv)
{
	ros::init(argc, argv, "odom_trans");
	OdomTrans odom_trans;
	ros::spin();
}

