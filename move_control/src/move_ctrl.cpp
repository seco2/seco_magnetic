
/**
 * @file : roboteq_interface.cpp
 * @author : SIS
 * @date : 2020.02.06
 * @brief : CONTROL
 * @todo
 * @bug
 * @warning
 */

#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <math.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include "boost/thread/thread.hpp"
#include "ros/ros.h"
#include "ros/time.h"

#include <std_msgs/UInt8.h>

// include message in roboteq driver
#include <motor_driver_msgs/Channel_value.h>
#include <motor_driver_msgs/Command.h>

// include message to advertise wheel speed data
#include "geometry_msgs/Twist.h"
#include "move_control/WheelSpeed.h"
#include "move_control/teleop.h"

#include <std_msgs/Bool.h>

// #define PI 3.1415927

using namespace std;
using namespace ros;

string node_name("sisAGVmoveCtrl");

class sisAGV
{

  enum
  {
    FRONT_RIGHT = 0,
    REAR_RIGHT = 1,
    REAR_LEFT = 2,
    FRONT_LEFT = 3,
    FRONT = 0,
    REAR = 1
  };
  ros::NodeHandle nh_;

  ros::Publisher pub_moveCmd[2];

  ros::Publisher pub_wheel_speed;

  ros::Subscriber sub_joyMoveCmd, sub_nav_cmd, sub_emergency_stop, sub_emergency_slow, sub_move_precise;

  ros::Subscriber sub_speed_AGV[2];

  ros::Subscriber sub_encoder_AGV[2];

  double measured_speed[4];
  double encoder_abs[4];
  double pre_measured_speed[4];
  double cal_acc[4];
  double set_speed[4];

  // AGV data 
  double AGV_len = 0.95;
  double AGV_wid = 0.95;
  double wheel_wid = 0.205;
  double wheel_rad = 0.18;
  double gear_ratio = 25;
  double scale_theta = 1;
  double driver_vel_count = 0.0;  // 속도 카운트 (천천히 가속 추가)

  double scale_x = 1.0, scale_y = 1.0, slow_scale = 10;
  double front_right_nav = 0, 
         front_left_nav = 0, 
         rear_left_nav = 0,
         rear_right_nav = 0;
  double front_right_joy = 0, 
         front_left_joy = 0, 
         rear_left_joy = 0,
         rear_right_joy = 0;

  // 최저 속도
  double minimum_speed = 100.0;

  bool emcStop_ = false;
  bool emcSlow_ = false; 

public:
  /**
   * @brief Constructor
   */
  // sisAGV(string portName_, int portBaudrate_ )
  sisAGV()
  {
    // Add 200813
    ros::NodeHandle nhLocal("~");

    nhLocal.param<double>("scale_x", scale_x, scale_x);
    nhLocal.param<double>("scale_y", scale_y, scale_y);
    nhLocal.param<double>("scale_theta", scale_theta, scale_theta);
    nhLocal.param<double>("gear_ratio", gear_ratio, gear_ratio);
    nhLocal.param<double>("slow_scale", slow_scale, slow_scale);
    nhLocal.param<double>("minimum_speed", minimum_speed, minimum_speed);
    nhLocal.param("AGV_length", AGV_len, AGV_len);
    nhLocal.param("AGV_width", AGV_wid, AGV_wid);
    nhLocal.param("wheel_radius", wheel_rad, wheel_rad);

    set_speed[FRONT_LEFT] = 0;
    set_speed[FRONT_RIGHT] = 0;
    set_speed[REAR_LEFT] = 0;
    set_speed[REAR_RIGHT] = 0;

    measured_speed[FRONT_LEFT] = 0;
    measured_speed[FRONT_RIGHT] = 0;
    measured_speed[REAR_LEFT] = 0;
    measured_speed[REAR_RIGHT] = 0;

    encoder_abs[FRONT_LEFT] = 0;
    encoder_abs[FRONT_RIGHT] = 0;
    encoder_abs[REAR_LEFT] = 0;
    encoder_abs[REAR_RIGHT] = 0;

    pre_measured_speed[FRONT_LEFT] = 0;
    pre_measured_speed[FRONT_RIGHT] = 0;
    pre_measured_speed[REAR_LEFT] = 0;
    pre_measured_speed[REAR_RIGHT] = 0;

    cal_acc[FRONT_LEFT] = 0;
    cal_acc[FRONT_RIGHT] = 0;
    cal_acc[REAR_LEFT] = 0;
    cal_acc[REAR_RIGHT] = 0;

    front_right_nav = 0, front_left_nav = 0, rear_left_nav = 0, rear_right_nav = 0;
    front_right_joy = 0, front_left_joy = 0, rear_left_joy = 0, rear_right_joy = 0;

    // subcribe speed data from driver
    sub_speed_AGV[FRONT] = nh_.subscribe("motor_driver_front/speed_data", 1, &sisAGV::front_speed_callback, this);
    sub_speed_AGV[REAR] = nh_.subscribe("motor_driver_rear/speed_data", 1, &sisAGV::rear_speed_callback, this);

    // subcribe absolute encoder data from driver
    sub_encoder_AGV[FRONT] = nh_.subscribe("motor_driver_front/encoder_data", 1, &sisAGV::front_encoder_callback, this);
    sub_encoder_AGV[REAR] = nh_.subscribe("motor_driver_rear/encoder_data", 1, &sisAGV::rear_encoder_callback, this);

    // subcribe navigation command
    sub_nav_cmd = nh_.subscribe("cmd_vel", 1, &sisAGV::moveControlCallback, this);

    // subcribe commanded velocity
    sub_joyMoveCmd = nh_.subscribe("cmd_vel_joy", 1, &sisAGV::joystickControlCallback, this);

    // advertise message about motor command
    pub_moveCmd[FRONT] = nh_.advertise<motor_driver_msgs::Command>("/FRONT/cmd", 1, true);
    pub_moveCmd[REAR] = nh_.advertise<motor_driver_msgs::Command>("/REAR/cmd", 1, true);

    // advertise wheel speed to odom node
    pub_wheel_speed = nh_.advertise<move_control::WheelSpeed>("/WheelSpeed", 1, true);

    sub_emergency_stop = nh_.subscribe("/EMERGENCY/STOP", 10, &sisAGV::emergencyStopCallback, this);
    sub_emergency_slow = nh_.subscribe("/EMERGENCY/SLOW", 10, &sisAGV::emergencySlowCallback, this);
  }

  ~sisAGV() {}

  int spin()
  {
    ros::Rate loop_rate(10); //100
    while (ros::ok())
    {
      publish_cmd();
      publish_Speed_data();
      ros::spinOnce();
      loop_rate.sleep();
    }
  }

private:
  void front_speed_callback(const motor_driver_msgs::Channel_value Speed_Value)
  {
    measured_speed[FRONT_LEFT] = Speed_Value.left_wheel;
    measured_speed[FRONT_RIGHT] = Speed_Value.right_wheel;
  }

  void rear_speed_callback(const motor_driver_msgs::Channel_value Speed_Value)
  {
    measured_speed[REAR_LEFT] = Speed_Value.left_wheel;
    measured_speed[REAR_RIGHT] = Speed_Value.right_wheel;
  }

  void front_encoder_callback(const motor_driver_msgs::Channel_value Encoder_Value)
  {
    encoder_abs[FRONT_LEFT] = Encoder_Value.left_wheel;
    encoder_abs[FRONT_RIGHT] = Encoder_Value.right_wheel;
  }

  void rear_encoder_callback(const motor_driver_msgs::Channel_value Encoder_Value)
  {
    encoder_abs[REAR_LEFT] = Encoder_Value.left_wheel;
    encoder_abs[REAR_RIGHT] = Encoder_Value.right_wheel;
  }

  void emergencyStopCallback(const std_msgs::Bool::ConstPtr& emergencyStopData) {
    emcStop_ = emergencyStopData->data;
  }

  void emergencySlowCallback(const std_msgs::Bool::ConstPtr& emergencySlowData) {
    emcSlow_ = emergencySlowData->data;
  }

  void publish_Speed_data()
  {
    move_control::WheelSpeed wheel_data_;

    if (pre_measured_speed[FRONT_LEFT] == 0)
    {
      cal_acc[FRONT_LEFT] = 0;
    }
    else
    {
      int temp = (measured_speed[FRONT_LEFT] - pre_measured_speed[FRONT_LEFT]);
      cal_acc[FRONT_LEFT] = temp;
    }
    pre_measured_speed[FRONT_LEFT] = measured_speed[FRONT_LEFT];

    if (pre_measured_speed[FRONT_RIGHT] == 0)
    {
      cal_acc[FRONT_RIGHT] = 0;
    }
    else
    {
      int temp = (measured_speed[FRONT_RIGHT] - pre_measured_speed[FRONT_RIGHT]);
      cal_acc[FRONT_RIGHT] = 1 * temp;
    }
    pre_measured_speed[FRONT_RIGHT] = measured_speed[FRONT_RIGHT];

    if (pre_measured_speed[REAR_LEFT] == 0)
    {
      cal_acc[REAR_LEFT] = 0;
    }
    else
    {
      // int temp=abs(measured_speed[REAR_LEFT]-pre_measured_speed[REAR_LEFT]);
      int temp = (measured_speed[REAR_LEFT] - pre_measured_speed[REAR_LEFT]);
      cal_acc[REAR_LEFT] = temp;
    }
    pre_measured_speed[REAR_LEFT] = measured_speed[REAR_LEFT];

    if (pre_measured_speed[REAR_RIGHT] == 0)
    {
      cal_acc[REAR_RIGHT] = 0;
    }
    else
    {
      // int
      // temp=abs(measured_speed[REAR_RIGHT]-pre_measured_speed[REAR_RIGHT]);
      int temp = (measured_speed[REAR_RIGHT] - pre_measured_speed[REAR_RIGHT]);
      cal_acc[REAR_RIGHT] = 1 * temp;
    }
    pre_measured_speed[REAR_RIGHT] = measured_speed[REAR_RIGHT];

    wheel_data_.header.stamp = ros::Time::now();

    wheel_data_.front_left_speed = measured_speed[FRONT_LEFT];
    wheel_data_.front_right_speed = measured_speed[FRONT_RIGHT];
    wheel_data_.rear_left_speed = measured_speed[REAR_LEFT];
    wheel_data_.rear_right_speed = measured_speed[REAR_RIGHT];

    wheel_data_.front_left_acc = cal_acc[FRONT_LEFT];
    wheel_data_.front_right_acc = cal_acc[FRONT_RIGHT];
    wheel_data_.rear_left_acc = cal_acc[REAR_LEFT];
    wheel_data_.rear_right_acc = cal_acc[REAR_RIGHT];

    wheel_data_.front_left_encoder = encoder_abs[FRONT_LEFT];
    wheel_data_.front_right_encoder = encoder_abs[FRONT_RIGHT];
    wheel_data_.rear_left_encoder = encoder_abs[REAR_LEFT];
    wheel_data_.rear_right_encoder = encoder_abs[REAR_RIGHT];

    pub_wheel_speed.publish(wheel_data_);
  }

  //// function declare which information that driving_one_motor node subcribed
  ///from teleop node
  void moveControlCallback(const geometry_msgs::Twist::ConstPtr& motorCmd_)
  {

    double linear_x = motorCmd_->linear.x * scale_x * 120;
    double linear_y = motorCmd_->linear.y * scale_y * 120;
    double angular_z = motorCmd_->angular.z * scale_theta * 120;
    // double front_left = 0, front_right = 0, rear_left = 0, rear_right = 0;
    
    double wheel_cir = 2 * M_PI * wheel_rad;

    front_left_nav = (1 / wheel_cir) *
                      (linear_x - linear_y - (AGV_len + AGV_wid)/2 * angular_z) * gear_ratio;

    front_right_nav = (1 / wheel_cir) *
                      (linear_x + linear_y + (AGV_len + AGV_wid)/2 * angular_z) * gear_ratio;

    rear_left_nav = (1 / wheel_cir) *
                      (linear_x + linear_y - (AGV_len + AGV_wid)/2 * angular_z) * gear_ratio;

    rear_right_nav = (1 / wheel_cir) *
                      (linear_x - linear_y + (AGV_len + AGV_wid)/2 * angular_z) * gear_ratio;


    
    /*
    set_speed[FRONT_LEFT] = front_right;
    set_speed[FRONT_RIGHT] = front_right;
    set_speed[REAR_LEFT] = rear_left;
    set_speed[REAR_RIGHT] = rear_right;

    setVelocityMoveCommand();
    */
    return;
  }
  void joystickControlCallback(const move_control::teleop::ConstPtr& motorCmd_)
  {

    if (motorCmd_->control_mode == 1)
    { 
      // joy max val= 1
      double linear_x = motorCmd_->linear_x * scale_x * 120;  // m/sec
      double linear_y = motorCmd_->linear_y * scale_y * 120;  // m/sec
      double angular_z = motorCmd_->angular_z * scale_theta * 120 ; // m/sec, deg/sec
      // double front_left = 0, front_right = 0, rear_left = 0, rear_right = 0;
   
      double wheel_cir = 2 * M_PI * wheel_rad;

      front_left_joy = (1 / wheel_cir) *
                      (linear_x - linear_y - (AGV_len + AGV_wid)/2 * angular_z) * gear_ratio;

      front_right_joy =  (1 / wheel_cir) *
                      (linear_x + linear_y + (AGV_len + AGV_wid)/2 * angular_z) * gear_ratio;

      rear_left_joy =  (1 / wheel_cir) *
                      (linear_x + linear_y - (AGV_len + AGV_wid)/2 * angular_z) * gear_ratio;

      rear_right_joy =  (1 / wheel_cir) *
                      (linear_x - linear_y + (AGV_len + AGV_wid)/2 * angular_z) * gear_ratio;

      //ROS_INFO("%f, %f, %f", linear_x, linear_y, angular_z);
      //ROS_INFO("%f, %f, %f, %f", front_left_joy, front_right_joy, rear_left_joy, rear_right_joy);
      front_left_nav = 0;
      front_right_nav = 0;
      rear_left_nav = 0;
      rear_right_nav = 0;
    }
    else
    {
      front_left_joy = 0;
      front_right_joy = 0;
      rear_left_joy = 0;
      rear_right_joy = 0;
 
      // setVelocityMoveCommand();
    }

    return;
  }
  void publish_cmd()
  {
    set_speed[FRONT_LEFT] = front_left_joy + front_left_nav;
    set_speed[FRONT_RIGHT] = front_right_joy + front_right_nav;
    set_speed[REAR_LEFT] = rear_left_joy + rear_left_nav;
    set_speed[REAR_RIGHT] = rear_right_joy + rear_right_nav;

    if (emcSlow_) {
      set_speed[FRONT_LEFT] /= slow_scale;
      set_speed[FRONT_RIGHT] /= slow_scale;
      set_speed[REAR_LEFT] /= slow_scale;
      set_speed[REAR_RIGHT] /= slow_scale;
    }


    if (emcStop_)
    {
      setStopMoveCommand();
    }
    else
    {
      if ((set_speed[FRONT_LEFT] == 0) && (set_speed[FRONT_RIGHT] == 0) &&
          (set_speed[REAR_LEFT] == 0) && (set_speed[REAR_RIGHT] == 0))
      {
        setStopMoveCommand();
      }
      else
      {
        setVelocityMoveCommand();
      }
    }
  }

  // set mode and speed command for the channel
  bool setVelocityMoveCommand()
  {

    motor_driver_msgs::Command DRIVER[2];

    DRIVER[FRONT].mode = motor_driver_msgs::Command::MODE_VELOCITY;
    DRIVER[FRONT].speed_left = set_speed[FRONT_LEFT];
    DRIVER[FRONT].speed_right = set_speed[FRONT_RIGHT];

    DRIVER[REAR].mode = motor_driver_msgs::Command::MODE_VELOCITY;
    DRIVER[REAR].speed_left = set_speed[REAR_LEFT];
    DRIVER[REAR].speed_right = set_speed[REAR_RIGHT];

    if (driver_vel_count < 50) {
      // 카운트가 50 이하라면 천천히 가속
      double driver_vel_ratio = (driver_vel_count / 50);
      DRIVER[FRONT].speed_left *= driver_vel_ratio;
      DRIVER[FRONT].speed_right *= driver_vel_ratio;
      DRIVER[REAR].speed_left *= driver_vel_ratio;
      DRIVER[REAR].speed_right *= driver_vel_ratio;
      driver_vel_count = driver_vel_count + 1.0;
    }

    pub_moveCmd[FRONT].publish(DRIVER[FRONT]);
    pub_moveCmd[REAR].publish(DRIVER[REAR]);
  }

  //
  bool setStopMoveCommand()
  {

    motor_driver_msgs::Command DRIVER[2];

    DRIVER[FRONT].mode = motor_driver_msgs::Command::MODE_STOPPED;
    DRIVER[FRONT].speed_left = 0;
    DRIVER[FRONT].speed_right = 0;

    DRIVER[REAR].mode = motor_driver_msgs::Command::MODE_STOPPED;
    DRIVER[REAR].speed_left = 0;
    DRIVER[REAR].speed_right = 0;

    driver_vel_count = 0;

    pub_moveCmd[FRONT].publish(DRIVER[FRONT]);
    pub_moveCmd[REAR].publish(DRIVER[REAR]);
  }
};

int main(int argc, char** argv)
{
  ros::init(argc, argv, node_name);

  sisAGV agvCtrl;
  return agvCtrl.spin();
}
