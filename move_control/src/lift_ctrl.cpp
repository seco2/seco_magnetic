/**
* @file : lift_management.cpp
* @author : SIS
* @date : 2020.12.20
* @brief : lift control
* @todo
* @bug
* @warning
*/

// Lift state :  0(stop in the middle), 1(lift up),2(lift down), 3(stop at upper limit), 4(stop at lower limit)

#include "ros/ros.h"
#include "ros/time.h"
#include "boost/thread/thread.hpp"

// include message to advertise wheel speed data

#include "move_control/lift_man.h"
//#include "move_control/lift_alm.h"
#include "usb_dio/lift_ctrl.h"
#include "usb_dio/io_in.h"

#include <std_msgs/Int8.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/Bool.h>

using namespace std;
using namespace ros;

#define MODE_STOP 0
#define MODE_UP 1
#define MODE_DOWN 2
#define MODE_EMC 3

class liftMan {

      ros::NodeHandle nh_;
      ros::Publisher publiftCmd_, publiftStatus_;

      ros::Subscriber subliftAuto_, subliftJoy_, subliftSens_, sub_emergency_stop, sub_emergency_mode_acs, sub_emergency_mode_joy; // Motor driver movement control command
      ros::ServiceClient clientlift_;

private:
      int8_t dir_auto_ = 0, dir_joy_ = 0;
      bool manual_mode = false;
      bool lim_up_, lim_down_;
      std_msgs::UInt8 state_lift;
      int16_t liftUp_cnt_ = 0, liftDown_cnt_ = 0;
      int16_t cnt_=0;
     
      bool emcStop_ = false;
      bool emcModeAcs_ = false;
      bool emcModeJoy_ = false;

public:

    /**
    * @brief Constructor
    */
    liftMan()
    {
          // subcribe navigation command
          subliftAuto_ = nh_.subscribe("/lift/CMD", 1, &liftMan::liftAutoCallback, this);
          // subcribe joy command
          subliftJoy_ = nh_.subscribe("/lift/man", 1, &liftMan::liftJoyCallback, this);
          subliftSens_ = nh_.subscribe("/io_in", 1, &liftMan::liftSensCallback, this);
          //clientlift_ = nh_.serviceClient<move_control::lift_alm>("/lift_alam", this);

          // advertise message about lift cmd
          publiftCmd_ = nh_.advertise<usb_dio::lift_ctrl>("/lift/ctrl", 1, true);
          // advertise message about lift status
          publiftStatus_ = nh_.advertise<std_msgs::UInt8>("/lift/status", 1, true);

          sub_emergency_stop = nh_.subscribe("/EMERGENCY/STOP", 10, &liftMan::emergencyStopCallback, this);
          sub_emergency_mode_acs = nh_.subscribe("/EMERGENCY/JOY", 10, &liftMan::emergencyModeAcsCallback, this);
          sub_emergency_mode_joy = nh_.subscribe("/EMERGENCY/JOY/MODE", 10, &liftMan::emergencyModeJoyCallback, this);
        
          lim_up_ = true;
          lim_down_ = false;
          state_lift.data = 4;
    }

    ~liftMan()
    {

    }

    void emergencyStopCallback(const std_msgs::Bool::ConstPtr& emergencyStopData) {
        emcStop_ = emergencyStopData->data;
    }

    void emergencyModeAcsCallback(const std_msgs::Bool::ConstPtr& emergencyModeData) {
        emcModeAcs_ = emergencyModeData->data;
    }

    void emergencyModeJoyCallback(const std_msgs::Bool::ConstPtr& emergencyModeData) {
        emcModeJoy_ = emergencyModeData->data;
    }

    void liftAutoCallback(const move_control::lift_man msg)
    {
        if (msg.mode == 1 && manual_mode == false)
        // 모드가 1이고 메뉴얼 모드가 아닐 경우
        {
            dir_auto_ = msg.dir;
            dir_joy_ = 0;
        }
        else
        {
            dir_auto_ = 0;
        }
    }

    void liftSensCallback(const usb_dio::io_in msg)
    {
        /*
        lim_up_ = msg.lim_up;
        lim_down_ = msg.lim_down;
        */
        if(msg.lim_up == false)
        {
            liftUp_cnt_ ++;
            if(liftUp_cnt_>=5)
            {
                liftUp_cnt_ = 5;
            }
            if(liftUp_cnt_ == 5)
                lim_up_ = false;
        }
        else
        {
            lim_up_ = true;
            liftUp_cnt_ = 0;
        }

        if(msg.lim_down == false)
        {
            liftDown_cnt_ ++;
            if(liftDown_cnt_>=5)
            {
                liftDown_cnt_ = 5;
            }
            if(liftDown_cnt_ == 5)
                lim_down_ = false;
        }
        else
        {
            lim_down_ = true;
            liftDown_cnt_ = 0;
        }
    }

    void liftJoyCallback(const move_control::lift_man msg)
    {
        if (msg.mode == 1)
        {
            manual_mode = true;
            dir_joy_ = msg.dir;
            dir_auto_ = 0;
        }
        else
        {
            manual_mode = false;
            dir_joy_ = 0;
        }
    }

    void pubCmd(int8_t cmd)
    {
        usb_dio::lift_ctrl liftCtrl_;
        switch(cmd)
        {
            case 0:
            {
                // Stop
                liftCtrl_.run = false;
                liftCtrl_.up = false;
            }
            break;
            case 1:
            {
                // Up
                liftCtrl_.run = true;
                liftCtrl_.up = true;
            }
            break;
            case -1:
            {
                // Down
                liftCtrl_.run = true;
                liftCtrl_.up = false;
            }
            break;
            default:
            {
                liftCtrl_.run = false;
                liftCtrl_.up = false;
            }
            break;
        }
        publiftCmd_.publish(liftCtrl_);
    }

    int run()
    {
        ros::Rate loop_rate(20);
        //first_run = true;

        while(ros::ok())
        {

            if (emcStop_ && (emcModeJoy_ == false || emcModeAcs_ == false))
            {
                pubCmd(0);
                state_lift.data = MODE_EMC;
            } // ems state.
            else
            {
                if (dir_auto_ == 0 && dir_joy_ == 0)
                { // command stop case
                // 속도가 0일 경우
                    pubCmd(0);
                    state_lift.data = MODE_STOP;
                }
                else
                {
                    if (dir_auto_ == 1 || dir_joy_ == 1)
                    {
                        // 리프트 상승
                        if (lim_up_ && (emcModeJoy_ == false || emcModeAcs_ == false)) {
                            // 리미트 걸려있다면
                            pubCmd(0);
                            state_lift.data = MODE_STOP;
                        } else {
                            // 리미트, 안걸려있다면
                            pubCmd(1);
                            state_lift.data = MODE_UP;
                        }
                    }
                    else if (dir_auto_ == -1 || dir_joy_ == -1)
                    { 
                        // 리프트 하강
                        if (lim_down_ && (emcModeJoy_ == false || emcModeAcs_ == false)) {
                            // 리미트 걸려있다면
                            pubCmd(0);
                            state_lift.data = MODE_STOP;
                        } else {
                            // 리미트, 안걸려있다면
                            pubCmd(-1);
                            state_lift.data = MODE_DOWN;
                        }
                    } else {
                        // 멈춤
                        pubCmd(0);
                        state_lift.data = MODE_STOP;
                    }
                }
            } 
            publiftStatus_.publish(state_lift);

            ros::spinOnce();
            loop_rate.sleep();
        }
    }

};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "Lift_management");
    liftMan liftSeCom_;
    return liftSeCom_.run();
}
