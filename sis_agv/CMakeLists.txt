cmake_minimum_required(VERSION 2.8.3)
project(sis_agv)

# if CMAKE_BUILD_TYPE is not specified, take 'Release' as default
IF(NOT CMAKE_BUILD_TYPE)
    SET(CMAKE_BUILD_TYPE Release)
ENDIF(NOT CMAKE_BUILD_TYPE)

## Find catkin and any catkin packages
find_package(catkin REQUIRED COMPONENTS
             roscpp
             sensor_msgs
             std_msgs
             geometry_msgs
             message_generation
             )

generate_messages(DEPENDENCIES std_msgs geometry_msgs)


## Declare a catkin package
catkin_package()

include_directories(include ${catkin_INCLUDE_DIRS})

## Build
#add_executable(naviCtl src/naviCtl.cpp)
#target_link_libraries(naviCtl ${catkin_LIBRARIES})
#target_link_libraries(sis_agv_vision ${OpenCV_LIBS})
## INSTALL##
## Mark executables and/or libraries for installation

#install(DIRECTORY srv/
#  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/srv
#  FILES_MATCHING PATTERN "*.srv"
#)


install(
  DIRECTORY launch
  config maps rviz urdf
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)
