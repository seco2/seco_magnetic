#include "ros/ros.h"
#include "ros/time.h"
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include "cruizcore_xg6000/xg6000_data.h"
#include "boost/thread/thread.hpp"
#include "serial_port/lightweightserial.h"
#include <sensor_msgs/Imu.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include "cruizcore_xg6000/gyroReset.h"


#define GYRO_SENSORDATA_LENGTH 26

namespace XG6000
{


class imu_xg6000
{



public:
  imu_xg6000(const char *port, int baud,std::string parent_frame_id,std::string frame_id);
  ~imu_xg6000();

  bool connect();
  bool run_xg6000();
  void stop_xg6000();
  bool imu_reset_command();
  void setConfig();
  bool disconnect();

  int parse_data(unsigned char *data);
  void read_imu_Thread();
  bool resetGyroCallback(cruizcore_xg6000::gyroReset::Request &req, cruizcore_xg6000::gyroReset::Response &res);

  //Define constants
  // const char COMM_PORT[] = "/dev/ttyUSB1";

  bool connected_;
  ros::NodeHandle nh_;
  ros::Publisher imu_pub_;
  ros::Publisher imu_data_pub_, imu_data_raw_pub_;
  ros::ServiceServer gyro_reset;
  void publish_imuData();
  void publish_imuData2();
  std::string parent_frame_id_;
  std::string frame_id_;

private:

  bool is_running_;
  float gyro_x_float,gyro_y_float,gyro_z_float;
  float roll_float,pitch_float,yaw_float;
  float acc_x_float,acc_y_float,acc_z_float;

  const char *port_;
  int baud_;

  LightweightSerial* serial_;

  ros::Timer data_publish_timer;
  ros::Time last_dataUpdatedTime_;
  boost::thread *imu_scan_thread_;
  tf::TransformBroadcaster broadcaster_;

};

imu_xg6000::imu_xg6000(const char *port, int baud,std::string parent_frame_id,std::string frame_id)
  : nh_("~"), port_(port), baud_(baud), connected_(false), is_running_(false), serial_(NULL),
    imu_scan_thread_(NULL), roll_float(0xFF), gyro_x_float(0xFF),parent_frame_id_(parent_frame_id),frame_id_(frame_id)
{
  //data_publish_timer=nh_.createTimer(ros::Duration(0.05),&imu_xg6000::publish_imuData,this);
  //data_publish_timer2=nh_.createTimer(ros::Duration(0.05),&imu_xg6000::publish_imuData2,this);
  imu_pub_ = nh_.advertise<cruizcore_xg6000::xg6000_data>("imu_data", 1);
  imu_data_pub_ = nh_.advertise<sensor_msgs::Imu>("imu/data", 30, true);
  //imu_data_raw_pub_ = nh_.advertise<sensor_msgs::Imu>("imu/data_raw", 1);
  gyro_reset = nh_.advertiseService("/reset_gyro", &imu_xg6000::resetGyroCallback, this);


}

imu_xg6000::~imu_xg6000() {
}


void imu_xg6000::publish_imuData()
{
    cruizcore_xg6000::xg6000_data IMU_data;

    if(is_running_)
    {
      IMU_data.header.stamp=last_dataUpdatedTime_;
      if(roll_float != 0xFF){
      IMU_data.status=0x01;   ////////////ok
      IMU_data.roll=roll_float;
      IMU_data.pitch=pitch_float;
      IMU_data.yaw=yaw_float;
      IMU_data.gyro_x=gyro_x_float;
      IMU_data.gyro_y=gyro_y_float;
      IMU_data.gyro_z=gyro_z_float;
      IMU_data.acc_x=acc_x_float;
      IMU_data.acc_y=acc_y_float;
      IMU_data.acc_z=acc_z_float;
    }
      else
    {
      IMU_data.status=0x00;   ////////////ERROR
      IMU_data.roll=0;
      IMU_data.pitch=0;
      IMU_data.yaw=0;
      IMU_data.gyro_x=0;
      IMU_data.gyro_y=0;
      IMU_data.gyro_z=0;
      IMU_data.acc_x=0;
      IMU_data.acc_y=0;
      IMU_data.acc_z=0;
    }
    imu_pub_.publish(IMU_data);
  }
}

bool imu_xg6000::resetGyroCallback(cruizcore_xg6000::gyroReset::Request &req, cruizcore_xg6000::gyroReset::Response &res)
{
    if(req.reset)
    {
        if(imu_reset_command())
            res.response = true;
        else
            res.response = false;
    }
    else
        res.response = false;
}

void imu_xg6000::publish_imuData2()
{

    // Publish ROS msgs.
    sensor_msgs::Imu imu_data_raw_msg;
    sensor_msgs::Imu imu_data_msg;
    double roll, pitch, yaw;
    if(is_running_)
    {

      // Set covariance value of each measurements.
      imu_data_raw_msg.linear_acceleration_covariance[0] =
      imu_data_raw_msg.linear_acceleration_covariance[4] =
      imu_data_raw_msg.linear_acceleration_covariance[8] =
      imu_data_msg.linear_acceleration_covariance[0] =
      imu_data_msg.linear_acceleration_covariance[4] =
      imu_data_msg.linear_acceleration_covariance[8] = -1;

      imu_data_raw_msg.angular_velocity_covariance[0] =
      imu_data_raw_msg.angular_velocity_covariance[4] =
      imu_data_raw_msg.angular_velocity_covariance[8] =
      imu_data_msg.angular_velocity_covariance[0] =
      imu_data_msg.angular_velocity_covariance[4] =
      imu_data_msg.angular_velocity_covariance[8] = -1;

      imu_data_msg.orientation_covariance[0] =
      imu_data_msg.orientation_covariance[4] =
      imu_data_msg.orientation_covariance[8] = -1;

      static double convertor_d2r = M_PI / 180.0; // for angular_velocity (degree to radian)
      static double convertor_r2d = 180.0 / M_PI; // for easy understanding (radian to degree)


      roll = 0.0;
      pitch = 0.0;
      yaw = yaw_float * convertor_d2r;



      // Get Quaternion fro RPY.
      tf::Quaternion orientation = tf::createQuaternionFromRPY(roll, pitch, yaw);

      ros::Time now = ros::Time::now();

      imu_data_raw_msg.header.stamp =
      imu_data_msg.header.stamp = now;

      imu_data_raw_msg.header.frame_id =
      imu_data_msg.header.frame_id = frame_id_;
      imu_data_raw_msg.header.frame_id =
      imu_data_msg.header.frame_id = frame_id_;
/*
      imu_data_raw_msg.header.frame_id =
      imu_data_msg.header.frame_id = parent_frame_id_;
      imu_data_raw_msg.header.frame_id =
      imu_data_msg.header.frame_id = parent_frame_id_;
*/
      // orientation
      imu_data_msg.orientation.x = orientation[0];
      imu_data_msg.orientation.y = orientation[1];
      imu_data_msg.orientation.z = orientation[2];
      imu_data_msg.orientation.w = orientation[3];

      // original data used the g unit, convert to m/s^2
      imu_data_raw_msg.linear_acceleration.x =
      imu_data_msg.linear_acceleration.x = acc_x_float;
      imu_data_raw_msg.linear_acceleration.y =
      imu_data_msg.linear_acceleration.y = acc_y_float;
      imu_data_raw_msg.linear_acceleration.z =
      imu_data_msg.linear_acceleration.z = acc_z_float;

      // imu.gx gy gz.
      // original data used the degree/s unit, convert to radian/s
      imu_data_raw_msg.angular_velocity.x =
      imu_data_msg.angular_velocity.x = gyro_x_float * convertor_d2r;
      imu_data_raw_msg.angular_velocity.y =
      imu_data_msg.angular_velocity.y = gyro_y_float * convertor_d2r;
      imu_data_raw_msg.angular_velocity.z =
      imu_data_msg.angular_velocity.z = gyro_z_float * convertor_d2r;


    }
      else
    {
      roll = 0, pitch = 0, yaw = 0;
      // orientation
      imu_data_msg.orientation.x = 0;
      imu_data_msg.orientation.y = 0;
      imu_data_msg.orientation.z = 0;
      imu_data_msg.orientation.w = 0;

      // original data used the g unit, convert to m/s^2
      imu_data_raw_msg.linear_acceleration.x =
      imu_data_msg.linear_acceleration.x = 0;
      imu_data_raw_msg.linear_acceleration.y =
      imu_data_msg.linear_acceleration.y = 0;
      imu_data_raw_msg.linear_acceleration.z =
      imu_data_msg.linear_acceleration.z = 0;

      // imu.gx gy gz.
      // original data used the degree/s unit, convert to radian/s
      imu_data_raw_msg.angular_velocity.x =
      imu_data_msg.angular_velocity.x = 0;
      imu_data_raw_msg.angular_velocity.y =
      imu_data_msg.angular_velocity.y = 0;
      imu_data_raw_msg.angular_velocity.z =
      imu_data_msg.angular_velocity.z = 0;


    }
    // publish the IMU data
    //imu_data_raw_pub_.publish(imu_data_raw_msg);
    imu_data_pub_.publish(imu_data_msg);

    // publish tf
    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(yaw);

    geometry_msgs::TransformStamped imu_trans;
    imu_trans.header.stamp = ros::Time::now();
    imu_trans.header.frame_id = parent_frame_id_;
    imu_trans.child_frame_id = frame_id_;

    imu_trans.transform.translation.x = 0;
    imu_trans.transform.translation.y = 0;
    imu_trans.transform.translation.z = 0.0;
    imu_trans.transform.rotation = odom_quat;

    broadcaster_.sendTransform(imu_trans);

  }



bool imu_xg6000::connect()
{
  if (!serial_) serial_ =new LightweightSerial(port_,baud_);

  if(serial_!=NULL)
  {
   if (serial_->is_ok())
    {
      connected_ = true;
      return true;
    }
      delete serial_;
      serial_ = NULL;
  }
  connected_ = false;
  ROS_INFO("Bad Connection with serial port Error %s",port_);
  return false;

}

bool imu_xg6000::disconnect()
{
  if(!serial_){
    delete serial_;
    serial_=NULL;
  }
  connected_=false;
  return true;
}

int imu_xg6000::parse_data(unsigned char *data)
{
  int checksum=0;
  signed short int roll_int=0, pitch_int=0, yaw_int = 0, gyro_x_int = 0, gyro_y_int = 0, gyro_z_int = 0, acc_x_int = 0,acc_y_int = 0,acc_z_int = 0;

  last_dataUpdatedTime_=ros::Time::now();

  //Verify packet heading information
  if(data[0]!=0xA6 || data[1]!=0xA6){
    return -1;
  }

  roll_int=(((data[4]&0xFF)<<8)|(data[3]&0xFF));
  pitch_int=(((data[6]&0xFF)<<8)|(data[5]&0xFF));
  yaw_int=(((data[8]&0xFF)<<8)|(data[7]&0xFF));

  gyro_x_int=(((data[10]&0xFF)<<8)|(data[9]&0xFF));
  gyro_y_int=(((data[12]&0xFF)<<8)|(data[11]&0xFF));
  gyro_z_int=(((data[14]&0xFF)<<8)|(data[13]&0xFF));

  acc_x_int=(((data[16]&0xFF)<<8)|(data[15]&0xFF));
  acc_y_int=(((data[18]&0xFF)<<8)|(data[17]&0xFF));
  acc_z_int=(((data[20]&0xFF)<<8)|(data[19]&0xFF));

  char	i, temp;
  for (i = 0; i < 23; i++)
  {
    memcpy(&temp, data + i + 2, sizeof(char));
    checksum = checksum + temp;
  }

  /*
  checksum=(roll_int+pitch_int+yaw_int+gyro_x_int+gyro_y_int+gyro_z_int+acc_x_int+acc_y_int+acc_z_int+data[2]+data[21]+data[22]+data[23]+data[24])&0xFFFF;
  */
  if((checksum&0xFF)!=data[25])
  {
    printf("Checksum error \n");
    return -1; //
  }

  roll_float=roll_int/100.0;
  pitch_float=pitch_int/100.0;
  yaw_float=yaw_int/100.0;

  gyro_x_float=gyro_x_int/100.0;
  gyro_y_float=gyro_y_int/100.0;
  gyro_z_float=gyro_z_int/100.0;

  acc_x_float=acc_x_int/100.0;
  acc_y_float=acc_y_int/100.0;
  acc_z_float=acc_z_int/100.0;


  // printf("roll_int=%04x, angle=%04x checksum=%04x gyro_x_float=%f roll_float=%f \n",roll_int, Angle,checksum,gyro_x_float,roll_float);
  // printf("gyro_x_float=%f roll_float=%f \n",gyro_x_float,roll_float);
  return 1;
}



void imu_xg6000::read_imu_Thread()
{
 int result; int i=0;
 unsigned char RECVBUF[30]={0,};
 int rlen=0;
 is_running_=true;

   while(is_running_)
   {
     if(serial_==NULL) break;
     if(serial_->is_ok()==false) break;

     if((result=serial_->read_block((uint8_t *)&RECVBUF[i],GYRO_SENSORDATA_LENGTH-rlen)) > 0)
     {
         rlen+=result;
         if(rlen <GYRO_SENSORDATA_LENGTH){
           i=rlen;
           continue;
         }else{
           //Verify packet heading information
           if(RECVBUF[0]!=0xA6)
           {
             i=0;rlen=0;
             memset(RECVBUF,0x00,30);
             continue;
           }
           parse_data((unsigned char *)&RECVBUF[0]);
         }
     }
     i=0;rlen=0;
     memset(RECVBUF,0x00,30);
           usleep(5000);  //5ms
   } //while()
  is_running_ = false;
 }
 void imu_xg6000::setConfig()
 {
   int result=0;
    char cmdstr[100]={0x0,};
    int cmdLength=0;

   sprintf(&cmdstr[0],"$MIA,I,B,38400,R,100,D,Y,Y*C4");
   cmdLength=strlen(cmdstr);
   cmdstr[cmdLength]='\0';

    if(serial_!=NULL)
    {
    if(serial_->is_ok()==true)
    {
      result=serial_->write_block((const uint8_t *)&cmdstr[0],cmdLength);
     }
    }
 }

 bool imu_xg6000::run_xg6000()
 {
   if(connected_== false) return false;

   if(imu_scan_thread_==NULL)
   {
    imu_scan_thread_= new boost::thread(boost::bind(&imu_xg6000::read_imu_Thread,this));
   }

   return true;
 }

 void imu_xg6000::stop_xg6000()
 {
   is_running_=false;
   if(imu_scan_thread_!=NULL){
     imu_scan_thread_->join();
     delete imu_scan_thread_;
     imu_scan_thread_=NULL;
   }
 }

 bool imu_xg6000::imu_reset_command()
 {
    int result=0;
       char cmdstr[100]={0x0,};
     int cmdLength=0;

    sprintf(&cmdstr[0],"$MIB,RESET*87");
    cmdLength=strlen(cmdstr);
    cmdstr[cmdLength]='\0';

     if(serial_!=NULL)
     {
     if(serial_->is_ok()==true)
     {
       result=serial_->write_block((const uint8_t *)&cmdstr[0],cmdLength);
       if(result > 0 )
       {
         // printf("[OK] Send 'RESET' COMMAND! --> %s\n",cmdstr);
         return true;
        }
      }
     }
    // printf("[FAILED] Send 'RESET' COMMAND! --> %s\n",cmdstr);
   return false;
 }

} // namespace XG6000



int main(int argc, char **argv)
{

  // int baud;
  // std::string port;

  // gyro_port=argv[1];
  // gyro_baud=atoi(argv[2]);


  ros::init(argc, argv, "~");
  ros::NodeHandle nh("~");

  std::string port = "/dev/ttyUSB3";
  int32_t baud = 38400;

  std::string parent_frame_id = "base_footprint";
  std::string frame_id = "imu";

  //std::string parent_frame_id = "imu";
 // std::string frame_id = "base_footprint";

  nh.param<std::string>("port", port, port);
  nh.param<int32_t>("baud", baud, baud);
  nh.param<std::string>("frame_id", frame_id, frame_id);
  nh.param<std::string>("parent_frame_id", parent_frame_id, parent_frame_id);

  XG6000::imu_xg6000  IMU_xg6000(port.c_str(),baud,parent_frame_id,frame_id);
  // imu_xg6000 IMU_xg6000;
#if 1
  while (ros::ok())
  {
    ROS_DEBUG("Attempting connection to %s at %i baud.", port.c_str(), baud);
    IMU_xg6000.connect();
    if (IMU_xg6000.connected_)
    {
      ROS_DEBUG("Successfully connect to serial device \n");
      IMU_xg6000.imu_reset_command();
      usleep(500000);
      IMU_xg6000.setConfig();
      usleep(500000);
      ros::Rate loopRate(100);
      while (ros::ok())
      {
        IMU_xg6000.run_xg6000();
        IMU_xg6000.publish_imuData();
        IMU_xg6000.publish_imuData2();
        ros::spinOnce();
        loopRate.sleep();
      }
    } else
    {
      ROS_DEBUG("Problem connecting to serial device.");
      ROS_ERROR_STREAM_ONCE("Problem connecting to port " << port << ". Trying again every 1 second.");
      sleep(1);
    }
  }
  IMU_xg6000.stop_xg6000();
  IMU_xg6000.disconnect();
#endif


  return 0;
}
