#include "ros/ros.h"
#include "std_msgs/String.h"
#include "pgv/vision_msg.h"

void QRCallback(const pgv::vision_msg& msg)
{
    float x_pos = 0, y_pos = 0, theta_ang = 0;
    x_pos = msg.X;
    y_pos = msg.Y;
    theta_ang = msg.theta;
    ROS_INFO("X: %0.2f, Y: %0.2f, theta: %0.2f", x_pos, y_pos, theta_ang);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "pgvQR_reader");
  ros::NodeHandle nh;

  ros::Subscriber sub = nh.subscribe("/pgv_data", 10, QRCallback);

  ros::spin();

  return 0;
}
