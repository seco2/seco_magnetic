#include "vision_sensor/visionSensor.h"

string intToStrAttachZero(int32_t num)
{
  string str;

  if (num < 10)
  {
    str = "0" + to_string(num);
  }
  else
  {
    str = to_string(num);
  }
  return str;
}

string getCurrentTime()
{
  string year, month, day, hour, min, sec;
  string currentTime = "";
  time_t curr_time;
  struct tm* curr_tm;

  curr_time = time(NULL);
  curr_tm = localtime(&curr_time);

  if (curr_tm != NULL)
  {
    year = intToStrAttachZero(curr_tm->tm_year + 1900);
    month = intToStrAttachZero(curr_tm->tm_mon + 1);
    day = intToStrAttachZero(curr_tm->tm_mday);
    hour = intToStrAttachZero(curr_tm->tm_hour);
    min = intToStrAttachZero(curr_tm->tm_min);
    sec = intToStrAttachZero(curr_tm->tm_sec);
    currentTime = year + month + day + "_" + hour + ":" + min + ":" + sec;
  }

  return currentTime;
}

string getFileName()
{
  string year, month, day, hour, min, sec;
  string fileName = "";
  time_t curr_time;
  struct tm* curr_tm;

  curr_time = time(NULL);
  curr_tm = localtime(&curr_time);

  if (curr_tm != NULL)
  {
    year = intToStrAttachZero(curr_tm->tm_year + 1900);
    month = intToStrAttachZero(curr_tm->tm_mon + 1);
    day = intToStrAttachZero(curr_tm->tm_mday);
    hour = intToStrAttachZero(curr_tm->tm_hour);
    min = intToStrAttachZero(curr_tm->tm_min);
    sec = intToStrAttachZero(curr_tm->tm_sec);
    fileName = year + "-" + month + "-" + day + "-" + hour + "-" + min + "-" +
               sec + ".txt";
  }

  return fileName;
}

uint32_t convertToIndex(uint32_t index)
{
  uint32_t result = 0;

  while ((index = index >> 1))
    ++result;

  return result;
}

uint32_t convertBitToAngle(uint8_t num1, uint8_t num2)
{
  uint32_t result;
  result = (num1 << 7) + num2;

  return result;
}

int32_t convertBitToPose(uint8_t num1, uint8_t num2)
{
  int32_t result;
  if ((num1 & 0x7f) == 0x7f)
  {
    result = (0x7f - (num2 & 0x7f)) * -1;
  }
  else
  {
    result = num2 & 0x7f;
  }

  return result;
}

uint32_t calculateID(uint8_t num1, uint8_t num2, uint8_t num3, uint8_t num4)
{
  uint32_t result;
  result = (num1 & 0x7f) << 21 | (num2 & 0x7f) << 14 | (num3 & 0x7f) << 7 |
           (num4 & 0x7f);

  return result;
}

uint32_t calculateID(uint8_t num1, uint8_t num2)
{
  uint32_t result;
  result = (num1 & 0x07) << 7 | (num2 & 0x7f);

  return result;
}

Vision::Vision(string portName_, int portBaudrate_) : _serialPort(NULL)
{
  isMoving = true;
  ros::NodeHandle _ph("~");

  m_connected = false;
  missedPortState = true;
  m_logFile = LOG_FILE_PATH + getFileName();

  _ph.param("deviceID", m_deviceID, 0);
  _ph.param<string>("portname", m_portName, portName_);
  _ph.param<int32_t>("baudrate", m_baudRate, portBaudrate_);

  // printf("m_deviceID=%d, m_portName=%s\n", m_deviceID, m_portName.c_str());
  fflush(stdout);

  pub = n.advertise<pgv::vision_msg>("/pgv_data", 1);
  pubSig = n.advertise<std_msgs::UInt8>("/pgv/status", 1);
  pubConnectSig = n.advertise<std_msgs::Bool>("/pgv/CONNECT", 1);

  // sub[0] = n.subscribe("/pgv_head_cmd", 1, &Vision::visionHeadCmdCallback,
  // this); sub[1] = n.subscribe("/pgv_pose_cmd", 1,
  // &Vision::visionPoseCmdCallback, this); sub[2] = n.subscribe("/pgv_dir_cmd",
  // 1, &Vision::visionDirCmdCallback, this); sub[3] =
  // n.subscribe("/pgv_color_cmd", 1, &Vision::visionColorCmdCallback, this);

  timer[0] = n.createTimer(ros::Duration(0.02), &Vision::controlTimer, this);
  timer[1] = n.createTimer(ros::Duration(0.02), &Vision::poseTimer, this);
}

Vision::~Vision()
{
  stop_Vision();
  portClose();
}

void Vision::controlTimer(const ros::TimerEvent& e)
{
  if (missedPortState == true)
  {
    portClose();
  }
  if (m_connected == false)
  {
    if (portOpen() == true)
    {
      usleep(50000);
      run_Vision();
    }
  }
  return;
}

void Vision::poseTimer(const ros::TimerEvent& e)
{
  if (m_connected == true)
  {
    send_pose();
  }

  std_msgs::Bool pgv_connect;
  pgv_connect.data = m_connected == 1 ? true : false;
  pubConnectSig.publish(pgv_connect);
  return;
}

void Vision::run_Vision()
{
  send_color(COLOR_RED);
  send_dir(DIR_STRAIGHT);
  return;
}

void Vision::stop_Vision()
{
  is_running_ = false;
  return;
}

bool Vision::portOpen()
{
  if (!_serialPort)
    _serialPort = new serial::Serial();

  serial::Timeout to(serial::Timeout::simpleTimeout(500));
  _serialPort->setTimeout(to);
  _serialPort->setPort(m_portName.c_str());
  _serialPort->setBaudrate(m_baudRate);
  _serialPort->setParity(serial::parity_t(2));
  try
  {
    _serialPort->open();
  }

  catch (serial::IOException)
  {
  }

  if (_serialPort->isOpen())
  {
    m_connected = true;
    missedPortState = false;
    printf("Port open ...\n");
    fflush(stdout);
    return true;
  }
  else
  {
    m_connected = false;
    missedPortState = true;
    // printf("Serial Error! %s, %d\n", m_portName.c_str(), m_baudRate);
    fflush(stdout);
    return false;
  }
}

bool Vision::portClose()
{
  if (_serialPort)
  {
    if (_serialPort->isOpen())
    {
      printf("Serial Closed!?\n");
      fflush(stdout);
      _serialPort->close();
      m_connected = false;
    }

    _serialPort->close();
    m_connected = false;
    delete _serialPort;
    _serialPort = NULL;
  }
  return true;
}

int32_t Vision::send_data(uint8_t* datas, int32_t length)
{
  int32_t txCount = 0;
  int32_t result = 0;

  ros::NodeHandle _n;
  ros::Rate r(100.0);

  double txStartedTime = ros::Time::now().toSec();
  while (_n.ok())
  {
    if (ros::isShuttingDown())
    {
      break;
    }
    ros::spinOnce();
    r.sleep();

    if (_serialPort == NULL)
    {
      break;
    }

    if (_serialPort->isOpen() == false)
    {
      break;
    }

    if (fabs(txStartedTime - ros::Time::now().toSec()) >= 1.0f)
    {
      printf("Response Timeout!\n");
      fflush(stdout);
      missedPortState = true;
      break;
    }

    result = _serialPort->write((const uint8_t*)&datas[0], length);

    if (result <= 0)
    {
      break;
    }

    else
    {
      txCount += result;
      if (txCount >= length)
      {
        /*
        printf("Send data : ");
        for (int i = 0; i < result; i++) {
          printf("%02x ", datas[i]);
        }
        printf(", length : %d\n", result);
*/
        read_data();
        break;
      }
    }
  }
  return txCount;
}

int Vision::parse_data(uint8_t* data, uint16_t length)
{
  int16_t checksum = 0;
  int16_t seq = 0;

  struct vision_log log;

  if (length == 2)
  { // Head, Color
    if (data[0] == 0x00 && data[1] == 0x00)
    { // Head
      printf("ack!\n");
    }

    else if ((data[0] & 0x7f) == 0x01 && (data[1] & 0x7f) == 0x01)
    {
      printf("Color : blue assigned!\n");
    }

    else if ((data[0] & 0x7f) == 0x02 && (data[1] & 0x7f) == 0x02)
    {
      printf("Color : green assigned!\n");
    }

    else if ((data[0] & 0x7f) == 0x04 && (data[1] & 0x7f) == 0x04)
    {
      printf("Color : red assigned!\n");
    }
    else
    { // Error
      printf("Error : wrong data!\n");
    }
  }

  else if (length == 21)
  { // Pose
    scanData.header.stamp = ros::Time::now();
    scanData.header.frame_id = "SIS_PGV";
    log.logType = LOG_POSE;

    // Tag Detected
    if ((data[1] & 0x40) == 0x40)
    {
      scanData.Tag = 1;
      scanData.TagID = calculateID(data[14], data[15], data[16], data[17]);

      scanData.Line = 0;
      scanData.LineID = 0;

      scanData.Control = 0;
      scanData.ControlID = 0;
    }

    // Control Detected
    else if ((data[0] & 0x08) == 0x08)
    {
      scanData.Tag = 0;
      scanData.TagID = 0;

      scanData.Line = 0;
      scanData.LineID = 0;

      scanData.Control = 1;
      scanData.ControlID = calculateID(data[14], data[15]);
    }

    else
    { // nl, np
      scanData.Tag = 0;
      scanData.TagID = 0;

      if ((data[0] & 0x02) != 0x02)
      { // DataMatrix Detected
        scanData.Line = 1;
      }
      else if ((data[1] & 0x04) != 0x04 && (data[0] & 0x02) == 0x02)
      { // Line Detected
        scanData.Line = 2;
      }
      else
      {
        scanData.Line = 0;
      }

      scanData.Control = 0;
      scanData.ControlID = 0;
    }

    // Fault & X pose & Line ID
    if ((data[0] & 0x01) == 0x01)
    {
      scanData.X = 0.0;
      scanData.Y = 0.0;
      scanData.theta = 0.0;
      if ((data[2] & 0x07) == 0x00 && (data[3] & 0x7f) == 0x00 &&
          (data[4] & 0x7f) == 0x00)
      {
        if ((data[5] & 0x7f) == 0x02)
        {
          scanData.Fault = 2;
        }
        else if ((data[5] & 0x05) == 0x05)
        {
          scanData.Fault = 5;
        }
        else if ((data[5] & 0x06) == 0x06)
        {
          scanData.Fault = 6;
        }
        else
        {
          scanData.Fault = -1;
        }
      }
      else
      {
        scanData.Fault = -1;
      }
    }

    else
    {
      scanData.Fault = 0;
      if ((data[1] & 0x40) == 0x40)
      {
        scanData.LineID = 0;
        scanData.X = convertBitToPose((data[4] & 0x7f), (data[5] & 0x7f));
        scanData.Y = convertBitToPose((data[6] & 0x7f), (data[7] & 0x7f));
        scanData.theta =
            convertBitToAngle((data[10] & 0x7f), (data[11] & 0x7f));
      }
      else
      {
        if ((data[0] & 0x02) != 0x02)
        {
          scanData.LineID = calculateID(data[2], data[3], data[4], data[5]);
        }
        else
        {
          scanData.LineID = 0;
        }
        scanData.X = 0;
        scanData.Y = convertBitToPose((data[6] & 0x7f), (data[7] & 0x7f));
        scanData.theta =
            convertBitToAngle((data[10] & 0x7f), (data[11] & 0x7f));
      }
    }

    // Warning
    if ((data[0] & 0x04) == 0x04)
    {
      if ((data[18] & 0x7f) != 0x00)
      {
        scanData.Warning = convertToIndex(data[18] & 0x7f) + 6;
      }
      else if ((data[19] & 0x7f) != 0x00)
      {
        scanData.Warning = convertToIndex(data[19] & 0x7f);
      }
      else
      {
        scanData.Warning = 0;
      }
    }
    else
    {
      scanData.Warning = 0;
    }
    pub.publish(scanData);

    std_msgs::UInt8 status;
    status.data = 1;
    pubSig.publish(status);
  }

  else if (length == 3)
  { // Dir
    if ((data[1] & 0x03) == 0x01)
    {
      printf("Follow right-hand lane!\n");
    }

    else if ((data[1] & 0x03) == 0x02)
    {
      printf("Follow left-hand lane!\n");
    }

    else if ((data[1] & 0x03) == 0x03)
    {
      printf("Straight ahead!\n");
    }

    else
    { // Error
      printf("Error : wrong data!\n");
    }
  }

  else
  { // Error
    printf("Error : wrong length!\n");
  }

  log.tagDetected = scanData.Tag;
  log.tagID = scanData.TagID;
  log.lineDetected = scanData.Line;
  log.lineID = scanData.LineID;
  log.controlDetected = scanData.Control;
  log.controlID = scanData.ControlID;
  log.fault = scanData.Fault;
  log.warning = scanData.Warning;
  log.x = scanData.X;
  log.y = scanData.Y;
  log.theta = scanData.theta;

  writeLog(log);
  return 0;
}

void Vision::read_data()
{
  int result;
  uint8_t buf[21] = {
      0,
  };

  ros::NodeHandle _n;
  float frequency = 100.0;
  ros::Rate r(frequency);

  double rxStartedTime = ros::Time::now().toSec();
  while (_n.ok())
  {
    if (ros::isShuttingDown())
      break;
    ros::spinOnce();
    r.sleep();

    if (fabs(rxStartedTime - ros::Time::now().toSec()) >= 1.0f)
    {
      // printf("Response Timeout!\n");
      missedPortState = true;
      break;
    }

    if (_serialPort == NULL)
      break;
    if (_serialPort->isOpen() == false)
      break;

    memset(buf, 0x00, sizeof(buf));
    result = _serialPort->read((uint8_t*)&buf[0], sizeof(buf));
    if (result > 0)
    {
      /*
      printf("Receive data : ");
            for (int i = 0; i < result; i++) {
              printf("%02x ", buf[i]);
            }
            printf(", length : %d\n", result);
      */
      fflush(stdout);
      rxStartedTime = ros::Time::now().toSec();
      parse_data((uint8_t*)&buf[0], result);
      memset(&buf[0], 0x00, sizeof(&buf));
      break;
    }
    memset(&buf[0], 0x00, sizeof(&buf));
  }
  return;
}

void Vision::visionHeadCmdCallback(const pgv::vision_msg& msg)
{
  send_head();
  return;
}

void Vision::visionPoseCmdCallback(const pgv::vision_msg& msg)
{
  send_pose();
  return;
}

void Vision::visionDirCmdCallback(const pgv::vision_msg& msg)
{
  send_dir(msg.command);
  return;
}

void Vision::visionColorCmdCallback(const pgv::vision_msg& msg)
{
  send_color(msg.command);
  return;
}

bool Vision::send_head()
{
  // 2bytes Receive
  // 1000 0000 : 80, use ack
  int result;
  uint8_t buf[2] = {
      0,
  };
  memset(buf, 0x00, 2);
  buf[0] = 0x80;    // request
  buf[1] = ~buf[0]; // checksum

  send_data(&buf[0], 2);
  return true;
}

bool Vision::send_pose()
{
  // 21 bytes Receive
  // 1100 1000 : c8, Position inquiry
  int result;
  uint8_t buf[2] = {
      0,
  };
  memset(buf, 0x00, 2);
  buf[0] = 0xc8;    // request
  buf[1] = ~buf[0]; // checksum

  send_data(&buf[0], 2);
  return true;
}

bool Vision::send_dir(int cmd)
{
  // 3bytes Receive
  // 1110 0000 : e0, Error 5
  // 1110 0100 : e4, Right lane
  // 1110 1000 : e8, Left lane
  // 1110 1100 : ec, Straight ahead
  int result;
  uint8_t buf[2] = {
      0,
  };
  memset(buf, 0x00, 2);
  if (cmd == DIR_STRAIGHT)
  { // Straight ahead
    buf[0] = 0xec;
  }
  else if (cmd == DIR_LEFT)
  { // Left lane
    buf[0] = 0xe8;
  }
  else if (cmd == DIR_RIGHT)
  { // Right lane
    buf[0] = 0xe4;
  }
  else
  { // error
    return false;
  }
  buf[1] = ~buf[0]; // checksum

  send_data(&buf[0], 2);
  return true;
}

bool Vision::send_color(int cmd)
{
  // 2bytes Receive
  // 1001 0000 : 90, Red
  // 1000 1000 : 88, Green
  // 1100 0100 : c4, Blue
  int result;
  uint8_t buf[2] = {
      0,
  };
  memset(buf, 0x00, 2);
  if (cmd == COLOR_RED)
  { // Red
    buf[0] = 0x90;
  }
  else if (cmd == COLOR_GREEN)
  { // Green
    buf[0] = 0x88;
  }
  else if (cmd == COLOR_BLUE)
  { // Blue
    buf[0] = 0xc4;
  }
  else
  { // error
    return false;
  }
  buf[1] = ~buf[0]; // checksum
  send_data(&buf[0], 2);
  return true;
}

void Vision::writeLog(struct vision_log log)
{
  if (log.logType == LOG_POSE)
  {
    string time = "";
    string content = "";
    string fullLog = "";  // Full log records
    string filePath = ""; // Full file path

    time = getCurrentTime();

    content = ", " + to_string(log.logType) + ", " +
              to_string(log.tagDetected) + ", " + to_string(log.tagID) + ", " +
              to_string(log.lineDetected) + ", " + to_string(log.lineID) +
              ", " + to_string(log.controlDetected) + ", " +
              to_string(log.controlID) + ", " + to_string(log.fault) + ", " +
              to_string(log.warning) + ", " + to_string(log.x) + ", " +
              to_string(log.y) + ", " + to_string(log.theta);

    fullLog = time + content;

    // printf("m_logFile : %s\n", m_logFile.c_str());
    ofstream logfile(m_logFile, ios::app);
    if (logfile.is_open())
    {
      logfile << fullLog << endl;
      logfile.close();

      ifstream ifs(m_logFile, ios::in | ios::binary);
      if (ifs.is_open())
      {
        int fileSize = 0;
        ifs.seekg(0, ios::end);
        fileSize = ifs.tellg();

        if (fileSize > MAX_LOG_FILE_SIZE)
        {
          m_logFile = LOG_FILE_PATH + getFileName();
        }
        ifs.close();
      }
    }
    else
    {
      // printf("Log File Create Failed.\n");
    }
  }
}

int main(int argc, char** argv)
{
  string portName = "/dev/ttyUSB4";
  int baudRate = 115200;

  ros::init(argc, argv, node_name);
  /*
    if (argc > 2) {
      portName = argv[1];
      printf("%s interface name=%s \n", node_name.c_str(), portName.c_str());
    }
   */
  ros::NodeHandle nhLocal("~");

  nhLocal.param<std::string>("port", portName, portName);
  nhLocal.param("baud", baudRate, baudRate);


  Vision visiondrvLOC(portName, baudRate);

  if (visiondrvLOC.portOpen() == true)
  {
    usleep(500000);
    visiondrvLOC.run_Vision();
  }

  float frequency = 100.0;
  ros::Rate r(frequency);

  while (!ros::isShuttingDown())
  {
    ros::spinOnce();
    r.sleep();
  }

  return 0;
}
