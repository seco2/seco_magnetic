/*
 * PGV qrcode  reader
 * Copyright (c) 2020, SIS, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the names of SIS, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* Author: Thi-Trang Tran */

#include <assert.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <dirent.h>
#include "ros/ros.h"
#include "ros/time.h"
#include "angles/angles.h"
#include "boost/thread/thread.hpp"
#include "serial/serial.h"

#include "std_msgs/UInt8.h"
#include "std_msgs/Bool.h"
#include "std_msgs/String.h"
#include "pgv/vision_msg.h"

#define COLOR_RED 0
#define COLOR_GREEN 1
#define COLOR_BLUE 2

#define DIR_STRAIGHT 0
#define DIR_LEFT 1
#define DIR_RIGHT 2

#define LOG_NONE 0
#define LOG_POSE 1

#define LOG_FILE_PATH "/home/trang/logs/vision_data/"

#define MAX_LOG_FILE_SIZE 5242880

using namespace std;

string node_name("pgvQR");

struct vision_log{
  int logType;

  uint8_t tagDetected;
  int tagID;

  int lineDetected;
  int lineID;

  uint8_t controlDetected;
  int controlID;

  int fault;
  int warning;

  float x;
  float y;
  float theta;


  vision_log() : logType(0){
    tagDetected = 0;
    tagID = 0;

    lineDetected = 0;
    lineID = 0;

    controlDetected = 0;
    controlID = 0;

    fault = 0;
    warning = 0;

    x = 0.0;
    y = 0.0;
    theta = 0.0;
  }
};

class Vision
{
public:
  ros::NodeHandle n;

  ros::Timer timer[2];
  ros::Publisher pub;
  ros::Publisher pubSig;
  ros::Publisher pubConnectSig;

  bool is_running_;
  bool missedPortState;

  std::string m_portName;
  std::string m_logFile;
  int m_baudRate;
  bool m_connected;

private:
  serial::Serial        *_serialPort;
  int m_deviceID;
  bool isMoving;
  pgv::vision_msg  scanData;

public:

  Vision(std::string portName_, int portBaudrate_);
  ~Vision();


  void controlTimer(const ros::TimerEvent& e);
  void poseTimer(const ros::TimerEvent& e);


  void run_Vision();
  void stop_Vision();


  bool portOpen();
  bool portClose();


  int32_t send_data(uint8_t *datas, int32_t length);
  void read_data();
  int parse_data(uint8_t *data, uint16_t length);
  int16_t calculateChecksum(char *data, unsigned short len);


  void visionHeadCmdCallback(const pgv::vision_msg& msg); // 2bytes
  void visionPoseCmdCallback(const pgv::vision_msg& msg); // 21bytes
  void visionDirCmdCallback(const pgv::vision_msg& msg); // 3bytes
  void visionColorCmdCallback(const pgv::vision_msg& msg); // 2bytes

  //functions
  bool send_head();
  bool send_pose();
  bool send_dir(int cmd);
  bool send_color(int cmd);

  //log
  void writeLog(struct vision_log log);
  void createLogFile();
};
