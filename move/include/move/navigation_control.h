#include "ros/time.h"
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Twist.h>
#include <if_node_msgs/cmdMove.h>
#include <if_node_msgs/waypoint.h>
#include <obstacle_detector/obs_detect.h>
#include <obstacle_detector/obstacles_detect.h>
#include <pgv/vision_msg.h>
#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/UInt8.h>
#include <tf/transform_broadcaster.h>
#include <vector>

#include <fstream>
#include <iostream>
#include <math.h>

#include "move_control/teleop.h"
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf/LinearMath/Transform.h>
#include <tf2_ros/transform_listener.h>

#include "move_control/lift_man.h"
#include "usb_dio/io_in.h"
#include "tabos_bms/bat_data.h"

#define NO_WHERE 0
#define H_AGV1 1
#define H_AGV2 2
#define CHG1 3
#define CHG2 4
#define MBR1 5
#define MBR2 6
#define MBR3 7
#define WGN1 8
#define WGN2 9
#define WGN3 10
#define CONV 11
#define PL1 12
#define PL2 13
#define M_WP1 14
#define M_WP2 15
#define W_WP1 16
#define W_WP2 17
#define M_WP 18
#define W_WP 19
#define W_WP0 20
#define PL_WP1 21
#define PL_WP2 22
#define CO_NW 23
#define CO_NE 24
#define CO_SW 25
#define CO_SE 26
#define PL_WP 29
#define CONV_WP1 30
#define CONV_WP2 37
#define STP_E 31
#define STP_IN 32
#define STP_OUT 33
#define MBR 34
#define WGN 35
#define PL 36
#define STP_CONV 38
#define START 39

#define QR_STR_OUT 1
#define QR_STR_LINE 2
#define QR_STR_IN 3

#define QR_SIDE_OUT 1
#define QR_SIDE_LINE 2
#define QR_SIDE_IN 3
/*
#define H1_ID 0
#define CHG1_ID 8
#define CONVWP1_ID 71
#define CONV_ID 7
#define H2_ID 1
#define W_WP11_ID 9
#define CHG2_ID 0
#define WGN2_ID 11
#define W_WP12_ID 12
#define W_WP0_ID 1
#define WGN3_ID 14
#define W_WP13_ID 13
#define PL_WP11_ID 1
#define PL1_ID 9
#define PL_WP12_ID 9
#define PL2_ID 5
#define M_WP11_ID 72
#define MBR1_ID 2
#define M_WP12_ID 0
#define MBR2_ID 4
#define M_WP13_ID 5
#define MBR3_ID 6
*/

#define H1_ID 0     // 10
#define CHG1_ID 8   // 9
#define CONVWP1_ID 1
#define CONV_ID 0
#define H2_ID 1
#define W_WP11_ID 2
#define CHG2_ID 0
#define WGN2_ID 11
#define W_WP12_ID 12
#define W_WP0_ID 1
#define WGN3_ID 14
#define W_WP13_ID 13
#define PL_WP11_ID 16
#define PL1_ID 15
#define PL_WP12_ID 18
#define PL2_ID 17
#define M_WP11_ID 7
#define MBR1_ID 8
#define M_WP12_ID 3
#define MBR2_ID 4
#define M_WP13_ID 5
#define MBR3_ID 6

using namespace std;

namespace agv_wp
{

tf2_ros::Buffer tfBuffer;

enum MODE_AGV
{
  mode_stop_ = 1,
  mode_start_ = 2,
  mode_obs_ = 3,
  mode_emc_ = 4,
  mode_qrfail_ = 5,
  mode_lrffail_ = 6,
  mode_recover_ = 7
};

enum WP
{
  NO_MOVE = 0,
  M_H1_,
  H1_M_,
  C1_M_,
  H1_NW_,
  M_NW_,
  C1_NW_,
  NW_SW_,
  W_H2_,
  H2_W_,
  H2_SW_,
  W_SW_,
  SW_CONV_,
  SW_STPI_,
  STPI_PL_,
  PL_STPO_,
  STPO_STPE_,
  STPE_NE_,
  NE_M_,
  NE_W_,
  NE_H2_,
  NE_H1_,
  H1_CHG1_,
  H2_CHG2_,
  CHG1_H1_,
  CHG2_H2_,
  CONV_STPI_,
  CONV_NE_,
  NW_STPCNV_,
  H2_STPCNV_,
  W_STPCNV_,
  STPCNV_SW_,
  INIT_
};

enum AGV_STATUS
{
  AGV_STP = 0,
  WP_REACH = 2,
  GOAL_REACH = 3,
  QR_IN_COMP = 10,
  LRF_RUN = 1,
  QR_RUN = 6,
  OBS = 11,
  MANUAL = 12,
  LRF_XY_COMP = 8,
  QR_FAIL = 4,
  LRF_FAIL = 5,
  SEN_FAIL = 13,
  LRF_RECOVER = 14,
  LRF_ER_ = 15
};

enum DIR_QR
{
  NO_DIR,
  LEFT,
  RIGHT,
  FORW,
  BACKW,
  OUTCALL_IN,
  OUTCALL_OUT,
  CENTER
};

enum MODE_MOVE
{
  MOVE_ZERO_ = 0,
  MOVE_LRF_ = 1,
  MOVE_QR_ = 2,
};

enum LRF_MOVE
{
  LRF_NONE_ = 0,
  LRF_XY_ = 1,
  LRF_YAW_ = 2
};

enum QR_MOVE
{
  QR_NONE_,
  QR_STR_,
  QR_SIDE_
};

enum QR_STEP
{
  QR_NONE,
  QR_OUT,
  QR_LINE,
  QR_LINE_STP,
  QR_IN
};

uint8_t move_point_;
MODE_AGV mode_agv_;
uint8_t agvPos_;
int8_t agvGoal_;
int8_t agvSubGoal_;
AGV_STATUS agvStatus_;
MODE_MOVE moveMode_;
LRF_MOVE lrfMove_;

int M_H1[3] = {M_WP2, M_WP, H_AGV1};
int H1_M[3] = {M_WP, M_WP2, MBR};
int C1_M[3] = {M_WP, M_WP2, MBR};
// int H2_W[6] = {W_WP0, W_WP1, W_WP, W_WP2, W_WP1, WGN};
int H2_W[3] = {W_WP, W_WP2, WGN};
// int W_H2[6] = {W_WP1, W_WP2, W_WP, W_WP1, W_WP0, H_AGV2};
int W_H2[4] = {W_WP2, W_WP, W_WP2, H_AGV2};
int H1_NW[2] = {M_WP, CO_NW};
int C1_NW[2] = {M_WP, CO_NW};
int M_NW[2] = {M_WP2, CO_NW};
int W_SW[2] = {W_WP2, CO_SW};
// int H2_SW[4] = {W_WP0, W_WP1, W_WP2, CO_SW};
int H2_SW[2] = {W_WP2, CO_SW};
int SW_CONV[2] = {CONV_WP2, CONV};
int SW_STPI[2] = {PL_WP, STP_IN};
int STPI_PL[2] = {PL_WP2, PL};
int PL_STPO[2] = {PL_WP2, STP_OUT};
int STPE_NE[3] = {PL_WP, CO_SE, CO_NE};
int NE_M[3] = {M_WP, M_WP2, MBR};
int NE_W[3] = {CO_NW, W_WP2, WGN};
// int NE_H2[6] = {M_WP, CO_NW, W_WP2, W_WP1, W_WP0, H_AGV2};
int NE_H2[3] = {CO_NW, W_WP2, H_AGV2};
int CONV_NE[3] = {CONV_WP2, CO_SE, CO_NE};
int CONV_STPI[3] = {CONV_WP2, PL_WP, STP_IN};
int NE_H1[2] = {M_WP, H_AGV1};
int NW_STPCNV[2] = {W_WP2, STP_CONV};
int H2_STPCNV[2] = {W_WP2, STP_CONV};
int W_STPCNV[2] = {W_WP2, STP_CONV};

int init_move_;

uint8_t rackMove_ = 0;

vector<float> H1, H2, C1, C2, NE, NW, SE, SW, MWP2, MWP11, MWP12, MWP13,
    MWP11_N, MWP12_N, MWP13_N, WWP2, WWP11, WWP12, WWP13, STOPE, STOPI, STOPO,
    PLWP, PLWP11, PLWP12, PLWP2, CONVWP2, CONVWP1, STPCNV;
DIR_QR qr_move_dir;

class NavigationManagement
{
public:
  NavigationManagement();
  ~NavigationManagement();
  void init();
  void spin();
  void update();
  void param_set();

private:
  void nav_control();
  void XY_control();
  void YAW_control();
  double lrf_tracking_ctrl(double Kp, double dx, double vmax, double vmin);

  void h_agv1_move();
  void h_agv2_move();
  void init_pose();
  void chg1_move();
  void chg2_move();
  void mbr_wp2_move();
  void wgn_wp2_move();
  void wgn_wp0_move();
  void pl_wp2_move();
  void cor_nw_move();
  void cor_ne_move();
  void cor_se_move();
  void cor_sw_move();
  void pl_wp0_move();
  void stp_e_move();
  void stp_in_move();
  void stp_out_move();
  void conv_wp2_move();
  void wgn_move();
  void mbr_move();
  void wgn_wp_move();
  void mbr_wp_move();
  void conv_move();
  void pl_move();
  void stp_cnv_move();

  // QR CTRL
  void qrCtrl();

  float PIDy(float Kp, float Ki);
  float PIDth(float ref, float Kp, float Ki);
  float QRtrackingCtrl();
  float QRthCtrl(float ref);

  WP wp_;
  bool obstacle_;
  int rate = 10;
  int move_state; // move : 1, stop : 0
  float target_pos_x, target_pos_y, target_A;
  float goal_x, goal_y, goal_A;
  float current_x = 0, current_y = 0, current_A = 0;
  float cur_x = 0, cur_y = 0, cur_A = 0;
  bool manual_mode;
  int count = 0, pt_ = 0, pts_ = 0;
  uint8_t tag_, control_;
  int tag_id, line_, preLine_id = 0, line_id, control_id, fault, warning,
                     command;
  int start_id = 0, stop_id = 0;
  double line_x, line_y, line_theta;
  double line_y_i, line_th_i;
  double X_TOL = 0.0, X_STA = 350.0, X_MID = 700.0, X_MAX = 1000.0;
  double QR_VX_MAX = 0.1, QR_VY_MAX = 0.1;
  double delX_TOL = 0.03, delY_TOL = 0.03, delTH_TOL = 0.7;
  double delX_LSTP = 0.3;
  double delY_LSTP = 0.3;
  double delX_ = 0, delY_ = 0, delTH_ = 0, lenX_ = 0, lenY_ = 0, angTH_ = 0;
  double delTH_LSTP = 10, delTH_STA = 10;
  // double LVX_MAX = 0.38, LVY_MAX = 0.3, LVTH_MAX = 0.05;
  double LVX_MAX = 0.6, LVY_MAX = 0.45, LVTH_MAX = 0.05;
  double ratio_ = 0;
  int time_ = 0;
   //double Kp_ = 0.85;
  double Kp_ = 0.5;

  int8_t limUpcnt_ = 0, limDowncnt_ = 0, lim1cnt_ = 0, lim2cnt_ = 0,
         lim3cnt_ = 0, lim4cnt_ = 0;

  int step_ = 0;
  int cnt_line_ = 0;

  uint8_t laserStatus_ = 0;
  uint8_t laserCnt_ = 0;

  double QR_VEL = 0.01, QR_AVEL = 0.006;
  bool limSw1_ = false, limSw2_ = false, limSw3_ = false, limSw4_ = false;
  bool limUp_ = false, limDown_ = false;

  int mov_rack = 0, mov_door = 0;

  bool obs_detect = false, emc_push_ = false;
  int8_t cnt_charge = 0;

  float old_x = 0, old_y = 0, old_A = 0 , old_vx = 0, old_vy = 0, old_vA = 0;
  int init_cnt_ = 0;
  bool lrf_fail = false, qr_fail = false, sen_fail = false;
  int idCnt_ = 0;
  bool lineFault_ = false;
  int rec_cnt_ = 0;
  bool lrfRecover_ = false;
  uint8_t pgvStatus_ = 0, cnt_pgv = 0;

  bool _emc = false;
  int8_t main_soc = 100;


  // 0.14, -4.958, 2.28  7.026, -12.29, 3.03
  // AGV1

   double H1_X = 0.28, H1_Y = -5.12, H2_X = 7.0, H2_Y = -12.36, H1_A
    = 0.0, H2_A = -0.36;
    double C1_X = 1.25, C1_Y = -5.12, C2_X = 7.134, C2_Y =
   -12.36, C1_A = 0.84, C2_A = -0.6;

  // AGV2
  // 0.137, -4.92, 1.39 7.032, -12.32, 1.55
/*
  double H1_X = 0.19, H1_Y = -5.16, H2_X = 7.05, H2_Y = -12.416, H1_A = 4.0,
         H2_A = 0.8;
  double C1_X = 1.2, C1_Y = -5.14, C2_X = 7.185, C2_Y = -12.411, C1_A = 5.9,
         C2_A = 0.82;
*/
  std_msgs::UInt8 state_goal;
  std_msgs::UInt8 charge_sig;
  geometry_msgs::Twist vel_;
  geometry_msgs::Pose2D cur_goal_;

  ros::NodeHandle nh;
  ros::Publisher goal_pub, waypoint_pub, vel_pub, rackMove_pub, emc_pub;
  ros::Publisher pub_CMD_LIFT, pub_plc, pub_door, pub_plt, pub_Charge;
  ros::Publisher pub_initPos, pub_pgv_reset;
  ros::Subscriber reach_sub, goal_sub, obstacle_sub, joy_sub, pgv_sub, io_sub,
      sub_init, sub_pgv_status;

  ros::Subscriber laser_status_sub, bat_sub;
  ros::Publisher pubLaser;

  void goal_target_cb(const if_node_msgs::cmdMove& goal);
  void obstacle_cb(const obstacle_detector::obstacles_detect msg);
  void io_cb(const usb_dio::io_in msg);
  void wp_pub();
  void joystickControlCallback(const move_control::teleop::ConstPtr& motorCmd_);
  void pgv_cb(const pgv::vision_msg& pgv_msg);
  void pgv_status_cb(const std_msgs::UInt8& msg);
  void init_cb(const std_msgs::UInt8::ConstPtr& msg);
  void bms_cb(const tabos_bms::bat_data::ConstPtr& msg);
  void stop_agv();
  float sign(float in);

  void laserStatusCallback(const std_msgs::UInt8::ConstPtr& msg);

  // void timer_callback(const ros::TimerEvent& e);
};

} // namespace agv_wp

using namespace std;
