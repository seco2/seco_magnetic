#include "ros/time.h"
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Twist.h>
#include <if_node_msgs/cmdMove.h>
#include <if_node_msgs/waypoint.h>
#include <obstacle_detector/obs_detect.h>
#include <obstacle_detector/obstacles_detect.h>
#include <pgv/vision_msg.h>
#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Int8.h>
#include <std_msgs/UInt32.h>
#include <std_msgs/UInt8.h>
#include <tf/transform_broadcaster.h>
#include <vector>

#include <math.h>
#include <iostream>
#include <fstream>

#include "move_control/teleop.h"
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf/LinearMath/Transform.h>
#include <tf2_ros/transform_listener.h>

#define NO_WHERE 0
#define H_AGV1 1
#define H_AGV2 2
#define CHG1 3
#define CHG2 4
#define MBR1 5
#define MBR2 6
#define MBR3 7
#define WGN1 8
#define WGN2 9
#define WGN3 10
#define CONV 11
#define PL1 12
#define PL2 13
#define M_WP1 14
#define M_WP2 15
#define W_WP1 16
#define W_WP2 17
#define M_WP 18
#define W_WP 19
#define W_WP0 20
#define PL_WP1 21
#define PL_WP2 22
#define CO_NW 23
#define CO_NE 24
#define CO_SW 25
#define CO_SE 26
#define WP_E1 27
#define WP_E2 28
#define PL_WP 29
#define CONV_WP1 30
#define CONV_WP2 37
#define STP_E 31
#define STP_IN 32
#define STP_OUT 33
#define MBR 34
#define WGN 35
#define PL 36

#define L_STR_STA 1
#define L_STR_FAST 2
#define L_STR_DEC 3
#define L_STR_STP 4

#define L_SIDE_STA 5
#define L_SIDE_FAST 6
#define L_SIDE_DEC 7
#define L_SIDE_STP 8

#define L_DIAG_STA 9
#define L_DIAG_FAST 10
#define L_DIAG_DEC 11
#define L_DIAG_STP 12

#define L_TH_STA 1
#define L_TH_FAST 2
#define L_TH_DEC 3
#define L_TH_STP 4

#define QR_STR_OUT 1
#define QR_STR_LINE 2
#define QR_STR_IN 3

#define QR_SIDE_OUT 1
#define QR_SIDE_LINE 2
#define QR_SIDE_IN 3

#define H1_ID 6
#define CHG1_ID 7
#define CONVWP1_ID 2
#define CONV_ID 8
#define H2_ID 3
#define W_WP12_ID 6
#define W_WP0_ID 7
#define CHG2_ID 1
#define WGN1_ID 3
#define W_WP11_ID 4
#define W_WP13_ID 5
#define PL_WP11_ID 3
#define PL1_ID 2
#define PL_WP12_ID 3
#define PL2_ID 2
#define M_WP11_ID 6
#define MBR1_ID 5
#define M_WP12_ID 4
#define MBR2_ID 7
#define M_WP13_ID 4
#define MBR3_ID 5

using namespace std;

namespace agv_wp
{

tf2_ros::Buffer tfBuffer;

enum MODE_AGV
{
  mode_stop_ = 1,
  mode_start_ = 2,
  mode_obs_ = 3
};

enum WP
{
  NO_MOVE = 0,
  M_H1_,
  H1_M_,
  C1_M_,
  H1_NW_,
  M_NW_,
  C1_NW_,
  NW_SW_,
  W_H2_,
  H2_W_,
  H2_SW_,
  W_SW_,
  SW_CONV_,
  SW_STPI_,
  STPI_PL_,
  PL_STPO_,
  STPO_STPE_,
  STPE_NE_,
  NE_M_,
  NE_W_,
  NE_H2_,
  NE_H1_,
  H1_CHG1_,
  H2_CHG2_,
  CHG1_H1_,
  CHG2_H2_,
  CONV_STPI_,
  CONV_NE_
};

enum AGV_STATUS
{
  AGV_STP = 0,
  WP_REACH = 2,
  LRF_PREC = 4,
  GOAL_REACH = 3,
  QR_OUT_COMP = 8,
  QR_LINE_COMP = 9,
  QR_IN_COMP = 10,
  LRF_RUN = 1,
  QR_LINE_CTRL = 6,
  QR_IN_CTRL = 7,
  QR_OUT_CTRL = 5,
  OBS = 11,
  MANUAL = 12,
  STEP1 = 13, // test 210110
  STEP2 = 14
};

enum MODE_QR_LINE_STR
{
  QR_STR_STP,
  STA_STR,
  MID_STR,
  STP_STR
};

enum MODE_QR_LINE_SIDE
{
  QR_SIDE_STP,
  STA_SIDE,
  MID_SIDE,
  STP_SIDE
};

enum DIR_QR
{
  NO_DIR,
  LEFT,
  RIGHT,
  FORW,
  BACKW
};

enum LRF_MOVE
{
  L_ZERO_ = 0,
  L_TH_ADJ_ = 1,
  L_XY_CTRL_ = 2,
  L_TH_CTRL_ = 3,
  PREC_CTRL_ = 4
};

enum MODE_MOVE
{
  MOVE_ZERO_ = 0,
  MOVE_LRF_ = 1,
  MOVE_QR_ = 2
};

enum QR_MOVE
{
  QR_NONE_,
  QR_STR_,
  QR_SIDE_
};

enum QR_STEP
{
    QR_NONE,
    QR_OUT,
    QR_LINE,
    QR_IN
};

QR_STEP qrStr_, qrSide_;
uint8_t move_point_;
MODE_AGV mode_agv_;
uint8_t agvPos_;
uint8_t agvGoal_;
uint8_t agvSubGoal_;
AGV_STATUS agvStatus_;
MODE_QR_LINE_STR modeQRLineStr_;
MODE_QR_LINE_SIDE modeQRLineSide_;
QR_MOVE qrMove;
MODE_MOVE moveMode_;

int M_H1[4] = {M_WP1, M_WP2, M_WP, H_AGV1};
int H1_M[4] = {M_WP, M_WP2, M_WP1, MBR};
int C1_M[4] = {M_WP, M_WP2, M_WP1, MBR};
int H2_W[6] = {W_WP0, W_WP1, W_WP, W_WP2, W_WP1, WGN};
int W_H2[6] = {W_WP1, W_WP2, W_WP, W_WP1, W_WP0, H_AGV2};
int H1_NW[2] = {M_WP, CO_NW};
int C1_NW[2] = {M_WP, CO_NW};
int M_NW[3] = {M_WP1, M_WP2, CO_NW};
int W_SW[3] = {W_WP1, W_WP2, CO_SW};
int H2_SW[4] = {W_WP0, W_WP1, W_WP2, CO_SW};
int SW_CONV[3] = {CONV_WP2,CONV_WP1, CONV};
int SW_STPI[2] = {PL_WP, STP_IN};
int STPI_PL[3] = {PL_WP2, PL_WP1, PL};
int PL_STPO[3] = {PL_WP1, PL_WP2, STP_OUT};
int STPE_NE[5] = {PL_WP, CO_SE, WP_E2, WP_E1, CO_NE};
int NE_M[4] = {M_WP, M_WP2, M_WP1, MBR};
int NE_W[5] = {M_WP, CO_NW, W_WP2, W_WP1, WGN};
int NE_H2[6] = {M_WP, CO_NW, W_WP2, W_WP1, W_WP0, H_AGV2};
int CONV_NE[6] = {CONV_WP1, CONV_WP2, CO_SE, WP_E2, WP_E1, CO_NE};
int CONV_STPI[4] = {CONV_WP1, CONV_WP2, PL_WP, STP_IN};

//int QR_STR[3] = {QR_STR_OUT, QR_STR_LINE, QR_STR_IN};
//int QR_SIDE[3] = {QR_SIDE_OUT, QR_SIDE_LINE, QR_SIDE_IN};

vector<float> H1, H2, C1, C2, NE, NW, SE, SW, MWP2, MWP11,MWP12, MWP13, WWP2, WWP11, WWP12, WWP13, STOPE, STOPI, STOPO, PLWP,
    PLWP11, PLWP12, PLWP2, CONVWP2, CONVWP1, E1, E2;
DIR_QR qr_move_dir;
LRF_MOVE lrfMove;
class NavigationManagement
{
public:
  NavigationManagement();
  ~NavigationManagement();
  void init();
  void spin();
  void update();
  void param_set();

private:
  void nav_control();

  void h_agv1_move();
  void h_agv2_move();
  void init_pose();
  void chg1_move();
  void chg2_move();
  void mbr_wp1_move();
  void wgn_wp1_move();
  void pl_wp1_move();
  void mbr_wp2_move();
  void wgn_wp2_move();
  void wgn_wp0_move();
  void pl_wp2_move();
  void cor_nw_move();
  void cor_ne_move();
  void cor_se_move();
  void cor_sw_move();
  void e1_move();
  void e2_move();
  void pl_wp0_move();
  void stp_e_move();
  void stp_in_move();
  void stp_out_move();
  void conv_wp1_move();
  void conv_wp2_move();
  void wgn_move();
  void mbr_move();
  void wgn_wp_move();
  void mbr_wp_move();
  void conv_move();
  void pl_move();


  // LRF CTRL
  void lrf_XYctrl();
  void lrf_THctrl();
  void lrf_prec_ctrl();

  float lrf_sta(float curx, float xrun, float dx, float lenx, float vmax,float vmin, float thresh);
  float lrf_stp(float Kp,float dx);

  float lrf_th_sta();
  float lrf_th_fast();
  float lrf_th_dec();
  float lrf_th_stp();

  void lrf_th_adj();
  float lrf_tracking_ctrl(float Kd, float dx, float tol);

  // QR CTRL
  void qr_str_Ctrl();
  void qr_side_Ctrl();

  void qr_str_line();
  void qr_str_sta();
  void qr_str_mid();
  void qr_str_stp();

  void qr_side_line();
  void qr_side_sta();
  void qr_side_mid();
  void qr_side_stp();

  void qrIn_tag_ctrl();  // INSIDE
  void qrOut_tag_ctrl(); // OUTSIDE

  float PIDy(float Kp, float Ki);
  float PIDth(float ref, float Kp, float Ki);
  float QRtrackingCtrl();
  float QRthCtrl(float ref);

  WP wp_;
  bool obstacle_;
  int rate = 10;
  int move_state; // move : 1, stop : 0
  float target_pos_x, target_pos_y, target_A;
  float goal_x, goal_y, goal_A;
  float current_x = 0, current_y = 0, current_A = 0;
  float cur_x = 0, cur_y = 0, cur_A = 0;
  bool manual_mode;
  int count=0, pt_=0, pts_=0, qr_str_step_ = 0, qr_side_step_=0, qr_tag_step_=0, lrf_step_=0;
  uint8_t tag_, control_;
  int tag_id, line_, preLine_id=0, line_id, control_id, fault, warning, command;
  int start_id = 0, stop_id = 0;
  double line_x, line_y, line_theta;
  double line_y_i, line_th_i;
  double X_TOL = 0.0, X_STA = 350.0, X_MID = 700.0, X_MAX = 1000.0;
  double QR_VX_MAX = 0.1, QR_VY_MAX = 0.1;
  double delX_TOL = 0.05, delY_TOL = 0.05, delTH_TOL = 1;
  double delX_LSTP = 0.3;
  double delY_LSTP = 0.3;
  double delX_ = 0, delY_ = 0, delTH_ = 0, lenX_ = 0, lenY_ = 0, angTH_ = 0;
  double delTH_LSTP = 10, delTH_STA = 10;
  double LVX_MAX = 0.5, LVY_MAX = 0.5, LVTH_MAX = 0.2;
  double ratio_ = 0;
  int time_ = 0;

  int lrfXYCtrl = 0, lrfTHCtrl = 0;
  double cal_vx = 0, cal_vy = 0, A_adj = 0;

  double QR_VEL=0.01, QR_AVEL=0.006;

  std_msgs::UInt8 state_goal;
  geometry_msgs::Twist vel_;
  geometry_msgs::Pose2D cur_goal_;

  ros::NodeHandle nh;
  ros::Publisher goal_pub, waypoint_pub, vel_pub;
  ros::Subscriber reach_sub, goal_sub, obstacle_sub, joy_sub, pgv_sub;

  void goal_target_cb(const if_node_msgs::cmdMove& goal);
  void obstacle_cb(const obstacle_detector::obstacles_detect msg);
  void wp_pub();
  void joystickControlCallback(const move_control::teleop::ConstPtr& motorCmd_);
  void pgv_cb(const pgv::vision_msg& pgv_msg);
  void stop_agv();
  float sign(float in);

  // void timer_callback(const ros::TimerEvent& e);
};

} // namespace agv_wp

using namespace std;
