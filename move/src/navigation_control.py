#!/usr/bin/env python3.8
import rospy
import copy
import math
from enum import Enum

from geometry_msgs.msg import Pose2D, Twist
from std_msgs.msg import UInt8, Bool, Float64MultiArray
from move_control.msg import lift_man, teleop, cmdMove
from usb_dio.msg import io_in, mag_detect, mag_mark
from pgv.msg import vision_msg


# move 종류
class MoveType(Enum):
    STOP = 0            # 정지
    XY_MOVE = 1         # X, Y축 이동
    ROTATE = 2          # 회전
    LIFT_MOVE = 3       # 리프트 작동
    MAG_MOVE = 4        # 마그네틱 이동
    MAG_MOVE_FINE = 5   # 마그네틱 이동 최종
    MAG_YAW = 6         # 마그네틱 회전
    MAG_YAW_FINE = 7    # 마그네틱 회전 최종
    GOAL_REACH = 8      # 목표지점 도달


# AGV 오토 동작 모드 종류
class AgvMode(Enum):
    STOP = 0        # 정지 상태
    START = 1       # 시작 상태
    ERROR = 2       # 에러 상태


# 방향 종류
class DirectionType(Enum):
    STOP = 0        # 정지 상태
    FRONT = 1       # 전방 이동
    REAR = -1       # 후방 이동
    LEFT = -2       # 좌측 이동
    RIGHT = 2       # 우측 이동 


# 속도 설정
# speed_scale : 속도 크기
# speed : 속도
# vmax : 최대 속도
# vmin : 최저 속도
def lrf_tracking_ctrl(speed_scale, speed, vmax, vmin):
    v_cal = speed_scale * speed
    if v_cal >= 0:
        # Forward
        if v_cal > vmax:
            # 최대 속도보다 크면 최고 속도로 설정
            v_cal = vmax
        if v_cal < vmin:
            # 최저 속도보다 작으면 최저 속도 설정
            v_cal = vmin
    else:
        # Backward
        if v_cal < -vmax:
            # 최대 속도보다 크면 최고 속도로 설정
            v_cal = -vmax
        if v_cal > -vmin:
            # 최저 속도보다 작으면 최저 속도 설정
            v_cal = -vmin
    return v_cal

# 마그네틱 직선과 AGV 직선의 거리(x축 절편) 계산
# dist : 마그네틱 센서 사이 거리
# x_1 : 앞 센서 값
# x_2 : 뒤 센서 값
def calc_magnetic_distance(dist: float, x_1: float, x_2: float) -> list:
    x_1 = round(((x_1-5)*35)/1000, 4)
    x_2 = round(((x_2-5)*35)/1000, 4)

    x_intercept: float = -(x_1 + x_2) / 2                # X 절편 (AGV가 중앙으로 이동해야할 거리)
    d_theta: float = -math.atan2(-dist, x_2 - x_1)*180/math.pi     # 사이 각도
    d_theta += 90 if d_theta <= 0 else -90

    print("사이 각도: ", d_theta)
    print("사이 거리: ", x_intercept)
    return [x_intercept, d_theta]


class NavigationControl: 
    # ====================================================
    #                   상태 관련 정의
    # ====================================================
    manual_mode: bool = False   # 메뉴얼 모드 여부
    auto_move: AgvMode = AgvMode.STOP      # AGV 오토 동작 모드
    move_mode: MoveType = MoveType.STOP      # 오토모드 종류
    number_data: int = 0        # 오토모드 번호 (순서, 인덱스 용)

    precise_mode: Bool = Bool()       # 정밀모드 여부 (속도 느려짐)
    precise_mode.data = False      # 정밀모드 여부 (속도 느려짐, 근접센서 OFF)

    error_count: int = 0            # 에러 발생 카운트

    mag_flag: bool = False      # 마그네틱 판별 변수
    decel_flag: bool = False    # 감속 여부
    received_decel_flag: bool = False
    decel_count: float = 0.0
    mag_count: int = 0
    detect_count: bool = False
    yaw_restart_flag: bool = False

    # ====================================================
    #         위치, 속도, 남은 거리 관련 정의
    # ====================================================
    current_pose: Pose2D = Pose2D()       # AGV 현재 위치
    current_pose.x = 0.0           # AGV 현재 X 축
    current_pose.y = 0.0           # AGV 현재 Y 축
    current_pose.theta = 0.0       # AGV 현재 방향

    target_pose: Pose2D = Pose2D()      # 목표 위치
    target_pose.x = 0.0                 # 타겟 X 축
    target_pose.y = 0.0                 # 타겟 Y 축
    target_pose.theta = 0.0             # 타겟 방향

    velocity: Twist = Twist()     # 속도 지정
    velocity.linear.x = 0.0       # 속도 X 축
    velocity.linear.y = 0.0       # 속도 Y 축
    velocity.linear.z = 0.0       # 속도 Z 축 (사용 X)
    velocity.angular.x = 0.0      # 속도 각도 X (사용 X)
    velocity.angular.y = 0.0      # 속도 각도 X (사용 X)
    velocity.angular.z = 0.0      # 타겟 방향

    lift_cmd_data: lift_man = lift_man()    # 리프트 명령 데이터
    lift_cmd_data.mode = 0                       # 리프트 모드
    lift_cmd_data.operate = 0                    # 리프트 작동 여부
    lift_cmd_data.dir = 0                        # 리프트 방향

    lenX: float = 0.0        # X 남은 거리
    lenY: float = 0.0        # Y 남은 거리
    angTH: float = 0.0       # 남은 각도

    pgv_data: vision_msg = vision_msg()     # QR 코드 데이터
    pgv_data.Tag = 0
    pgv_data.TagID = 0
    pgv_data.Line = 0
    pgv_data.LineID = 0
    pgv_data.Control = 0
    pgv_data.ControlID = 0
    pgv_data.X = 0.0
    pgv_data.Y = 0.0
    pgv_data.theta = 0.0
    pgv_data.Fault = 0
    pgv_data.Warning = 0
    pgv_data.command = 0


    # 마그네틱 데이터
    magnetic_data: dict = {
        'front': 0.0,
        'rear': 0.0,
        'left': 0.0,
        'right': 0.0
    }

    # 마그네틱 인식 여부
    magnetic_detect: dict = {
        'front': False,
        'rear': False,
        'left': False,
        'right': False
    }

    magnetic_mark: dict = {
        'front': False,
        'decel': False,
        'left': False
    }

    # 마그네틱 인식 카운트
    mag_detect_count: dict = {
        'front': 0,
        'rear': 0,
        'left': 0,
        'right': 0
    }

    mark_detect_count: dict = {
        'front': 0,
        'decel': 0,
        'left': 0
    }

    # ====================================================
    #              속도, 거리 제한 관련 정의
    # ====================================================
    marginX: float = 0.0125                 # X축 허용 값
    marginY: float = 0.0125                 # Y축 허용 값
    marginTH: float = 0.23                 # 회전 허용 값
    
    # 정밀 모드시 허용값
    preciseMarginX: float = 0.005
    preciseMarginY: float = 0.005
    preciseMarginTH: float = 0.15

    maxX: float = 0.8                    # X축 최대 속도
    maxY: float = 0.75                    # Y축 최대 속도
    maxTH: float = 0.45                   # 회전 최대 속도

    minX: float = 0.05                   # X축 최저 속도
    minY: float = 0.04                   # Y축 최저 속도
    minTH: float = 0.01                  # 회전 최소 속도

    reverse_x_vel: bool = False         # X축 속도 반전
    reverse_y_vel: bool = False          # y축 속도 반전
    reverse_angle_vel: bool = False     # 회전 속도 반전

    x_speed_scale: float = 0.3            # X축 속도 스케일
    y_speed_scale: float = 0.3            # Y축 속도 스케일
    theta_speed_scale: float = 0.004       # 회전 속도 스케일
    mag_scale: float = 0.3          # 마그네틱 스케일

    magnetic_front: float = 0.0     # AGV 전방 마그네틱 센서 값
    magnetic_rear: float = 0.0      # AGV 후방 마그네틱 센서 값
    magnetic_left: float = 0.0      # AGV 좌측 마그네틱 센서 값
    magnetic_right: float = 0.0     # AGV 우측 마그네틱 센서 값

    magnetic_x_dist: float = 2.08    # AGV X축 마그네틱 사이 거리
    magnetic_y_dist: float = 1.36    # AGV Y축 마그네틱 사이 거리

    lift_up_lim: bool = False       # 리프트 상승 리미트
    lift_down_lim: bool = False     # 리프트 하강 리미트

    lift_plt_dectect: bool = False      # 리프트 팔레트 인식 여부

    # ====================================================
    #                  ROS Publish 정의
    # ====================================================
    wheel_velocity_pub: rospy.Publisher   # 바퀴 속도 지정 Publish
    lift_cmd_pub: rospy.Publisher         # 리프트 명령 지정 Publish
    precise_pub: rospy.Publisher          # 정밀 모드 Publish
    move_state_pub: rospy.Publisher       # 오토모드 상태 Publish

    def __init__(self):
        rospy.init_node('Navigation_Control')

        # 변수 설정
        rate = rospy.Rate(10)               # 10hz

        # Parameter 가져오기
        self.marginX = float(rospy.get_param("~marginX", self.marginX))       # X축 허용 값
        self.marginY = float(rospy.get_param("~marginY", self.marginY))       # Y축 허용 값
        self.marginTH = float(rospy.get_param("~marginTH", self.marginTH))    # 회전 허용 값
        self.maxX = float(rospy.get_param("~maxX", self.maxX))                # X축 최대 속도
        self.maxY = float(rospy.get_param("~maxY", self.maxY))                # Y축 최대 속도
        self.maxTH = float(rospy.get_param("~maxTH", self.maxTH))             # 회전 최대 속도
        self.minX = float(rospy.get_param("~minX", self.minX))                # X축 최저 속도
        self.minY = float(rospy.get_param("~minY", self.minY))                # Y축 최저 속도
        self.minTH = float(rospy.get_param("~minTH", self.minTH))             # 회전 최소 속도
        self.reverse_x_vel = bool(rospy.get_param("~reverse_x_vel", self.reverse_x_vel))                # X축 속도 반전
        self.reverse_y_vel = bool(rospy.get_param("~reverse_y_vel", self.reverse_y_vel))                # y축 속도 반전
        self.reverse_angle_vel = bool(rospy.get_param("~reverse_angle_vel", self.reverse_angle_vel))    # 회전 속도 반전
        self.x_speed_scale = float(rospy.get_param("~x_speed_scale", self.x_speed_scale))               # X축 속도 스케일
        self.y_speed_scale = float(rospy.get_param("~y_speed_scale", self.y_speed_scale))               # Y축 속도 스케일
        self.theta_speed_scale = float(rospy.get_param("~theta_speed_scale", self.theta_speed_scale))   # 회전 최소 속도
        self.magnetic_x_dist = float(rospy.get_param("~magnetic_x_dist", self.magnetic_x_dist))     # 마그네틱 x축 사이 거리
        self.magnetic_y_dist = float(rospy.get_param("~magnetic_y_dist", self.magnetic_y_dist))     # 마그네틱 y축 사이 거리

        # Publish 설정
        self.wheel_velocity_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=1)       # 바퀴 속도 지정 Publish
        self.lift_cmd_pub = rospy.Publisher('/lift/CMD', lift_man, queue_size=1)         # 리프트 명령 지정 Publish
        self.precise_pub = rospy.Publisher('/MOVE/PRECISE', Bool, queue_size=1)          # 정밀모드 Publish
        self.move_state_pub = rospy.Publisher('/MOVE/STATE', cmdMove, queue_size=100)    # 오토모드 상태 Publish

        # Subscriber 설정
        rospy.Subscriber("/MOVE/CMD", cmdMove, self.moveCmdCallback)               # 목표 설정
        rospy.Subscriber("/cmd_vel_joy", teleop, self.joystickControlCallback)   # 조이스틱
        rospy.Subscriber("/agv_pose", Pose2D, self.setCurrentPose)               # 현재 위치 설정
        rospy.Subscriber("/io_in", io_in, self.liftLimCallback)              # 리프트 리미트 센서 상태 가져오기
        rospy.Subscriber("/pgv_data", vision_msg, self.pgvCallback)         # QR 코드 데이터 가져오기
        rospy.Subscriber("/MAG", Float64MultiArray, self.magneticCallback)      # 마그네틱 데이터 가져오기
        rospy.Subscriber("/MAG/DETECT", mag_detect, self.magneticDetectCallback)    # 마그네틱 인식 여부 가져오기
        rospy.Subscriber("/MAG/MARK", mag_mark, self.magneticMarkCallback) 

        while not rospy.is_shutdown():
            self.update()
            rate.sleep()

    # 반복 작업
    def update(self):
        if self.manual_mode:
            # 메뉴얼 모드
            print("Manual Mode")
            self.auto_move = AgvMode.STOP
            self.number_data = 0

            self.autoModeReset()
            self.sendStateMove()
        else:
            # 오토모드
            print("Auto Mode")
            if self.auto_move == AgvMode.START:
                # 오토 모드 동작 중이라면
                print("Auto Move Start ...")
                self.navControl()
            else:
                # 오토 모드가 동작 중이지 않으면 초기화
                self.autoModeReset()
                self.sendStateMove()
                
        self.wheel_velocity_pub.publish(self.velocity)         # 속도 지정 (움직이기)
        self.lift_cmd_pub.publish(self.lift_cmd_data)          # 리프트 명령 지정 (움직이기)
        self.precise_pub.publish(self.precise_mode)            # 정밀 모드 보내기 (움직이기)

    # 자동 이동 컨트롤
    def navControl(self):
        self.sendStateMove()

        self.setLiftCMD()
        self.setAGVVelocity()

        if self.move_mode == MoveType.STOP and self.number_data != 0:
            self.move_mode = MoveType.GOAL_REACH

        # if self.move_mode == MoveType.XY_MOVE:
        #     # XY 이동 작업
        #     self.xyControl(self.target_pose.x, self.target_pose.y)
        
        # if self.move_mode == MoveType.ROTATE:
        #     # 회전 작업
        #     self.yawControl(self.target_pose.theta, flag=True)

        if self.move_mode == MoveType.MAG_MOVE or self.move_mode == MoveType.MAG_MOVE_FINE:
            self.magneticMoveControl()

        if self.move_mode == MoveType.MAG_YAW or self.move_mode == MoveType.MAG_YAW_FINE:
            self.magneticYAWControl()

        if self.move_mode == MoveType.LIFT_MOVE:
            # 리프트 작업
            self.liftControl(self.target_pose.x)

        if self.move_mode == MoveType.GOAL_REACH:
            # 목표 지점 도달 시
            self.auto_move = AgvMode.STOP
            self.sendStateMove()
            self.autoModeReset()  # 초기화

    # 오토 모드 초기화
    def autoModeReset(self):
        self.move_mode = MoveType.STOP
        self.target_pose.x = 0.0
        self.target_pose.y = 0.0
        self.target_pose.theta = 0.0
        self.precise_mode.data = False   # 정밀 모드
        self.setAGVVelocity()
        self.setLiftCMD()
        self.error_count = 0
        self.decel_count = 0
        self.detect_count = 0
        self.decel_flag = False
        self.received_decel_flag = False
        self.mag_flag = False
        self.yaw_restart_flag = False

    # 자동 움직임 상태 보내기
    def sendStateMove(self):
        cmd_data: cmdMove = cmdMove()
        cmd_data.move = self.auto_move.value
        cmd_data.type = self.move_mode.value
        cmd_data.number = self.number_data
        cmd_data.X = self.target_pose.x
        cmd_data.Y = self.target_pose.y
        cmd_data.A = self.target_pose.theta
        cmd_data.mag_count = self.mag_count 
        # cmd_data.mag_flag = self.mag_flag
        self.move_state_pub.publish(cmd_data)

    # ====================================================
    #            속도 지정 함수 정의 부분
    # ====================================================
    # XY 이동 작업
    def xyControl(self, goalX: float, goalY: float):
        currentX: float = self.current_pose.x
        currentY: float = self.current_pose.y
        currentA: float = self.current_pose.theta

        delX: float = goalX - currentX
        delY: float = goalY - currentY
        self.lenX = abs(delX)
        self.lenY = abs(delY)

        vel_x: float = 0.0
        vel_y: float = 0.0

        print("delX = ", delX, "(", self.lenX, ")")
        print("delY = ", delY, "(", self.lenY, ")")
        
        self.velocity.angular.z = 0

        print("set XY ... ")
        if self.lenX <= self.marginX and self.lenY <= self.marginY:
            # 목표 좌표로 도착했으면 회전 설정
            print("XY Control Completion")
            self.move_mode = MoveType.ROTATE
        else:
            # X, Y 속도 설정
            if 0 <= currentA <= 5 or 355 <= currentA <= 360:
                # 현재 각도 : 0도
                self.yawControl(0, True)

                if self.lenX <= self.marginX:
                    vel_x = 0
                else:
                    vel_x = -lrf_tracking_ctrl(self.x_speed_scale, delX, self.maxX, self.minX)

                if self.lenY <= self.marginY:
                    vel_y = 0
                else:
                    vel_y = lrf_tracking_ctrl(self.y_speed_scale, delY, self.maxY, self.minY)
            elif 85 <= currentA <= 95:
                # 현재 각도 : 90도
                self.yawControl(90, True)

                if self.lenY <= self.marginY:
                    vel_x = 0
                else:
                    vel_x = -lrf_tracking_ctrl(self.x_speed_scale, delY, self.maxX, self.minX)

                if self.lenX <= self.marginX:
                    vel_y = 0
                else:
                    vel_y = -lrf_tracking_ctrl(self.y_speed_scale, delX, self.maxY, self.minY)

            elif 175 <= currentA <= 185:
                # 현재 각도 : 180도
                self.yawControl(180, True)

                if self.lenX <= self.marginX:
                    vel_x = 0
                else:
                    vel_x = lrf_tracking_ctrl(self.x_speed_scale, delX, self.maxX, self.minX)

                if self.lenY <= self.marginY:
                    vel_y = 0
                else:
                    vel_y = -lrf_tracking_ctrl(self.y_speed_scale, delY, self.maxY, self.minY)
                    
            elif 265 <= currentA <= 275:
                # 현재 각도 : 270도
                self.yawControl(270, True)

                if self.lenY <= self.marginY:
                    vel_x = 0
                else:
                    vel_x = lrf_tracking_ctrl(self.x_speed_scale, delY, self.maxX, self.minX)

                if self.lenX <= self.marginX:
                    vel_y = 0
                else:
                    vel_y = lrf_tracking_ctrl(self.y_speed_scale, delX, self.maxY, self.minY)
            else:
                # 각도가 맞지 않을 경우 각도 맞추기
                if 315 <= currentA <= 360 or 0 <= currentA <= 45:
                    vel_a = 0
                elif 225 <= currentA < 315:
                    vel_a = 270
                elif 135 <= currentA < 225:
                    vel_a = 180
                else:
                    vel_a = 90
                self.yawControl(vel_a)
            
            print("Set Velocity X : ", vel_x)
            print("Set Velocity y : ", vel_y)
            self.velocity.linear.x = vel_x if self.reverse_x_vel is False else vel_x * -1
            self.velocity.linear.y = vel_y if self.reverse_y_vel is False else vel_y * -1
            
    # 회전 컨트롤
    # flag : True일 경우 각도만 조절
    def yawControl(self, goalA: float, xy_with_tuning=False, flag=False, precise_flag=False):
        currentA: float = self.current_pose.theta    # 현재 각도
        delTH: float = goalA - currentA    # 남은 각도
        yaw: float = 0.0

        if self.precise_mode.data is True and precise_flag is True:
            margin_TH: float = self.preciseMarginTH
            theta_scale: float = self.theta_speed_scale*0.1
            min_TH: float = self.minTH*0.1
        else:
            margin_TH: float = self.marginTH
            theta_scale: float = self.theta_speed_scale
            min_TH: float = self.minTH

        # 회전해야할 각도가 180 이상 넘어 갈 경우
        if delTH >= 180:
            delTH -= 360
        elif delTH <= -180:
            delTH += 360

        self.angTH = abs(delTH)
        print("set YAW ... ")
        if self.angTH <= margin_TH:
            # 남은 각도가 허용범위 내라면
            print("YAW Control Completion")
            if flag is True:
                # 목표 지점 도달
                if self.lenX <= self.marginX and self.lenY <= self.marginY:
                    self.move_mode = MoveType.GOAL_REACH
                else:
                    self.move_mode = MoveType.XY_MOVE
        else:
            # 그렇지 않다면 속도 설정
            if xy_with_tuning is True and (self.lenX <= 1 or self.lenY <= 1):
                theta_scale = self.theta_speed_scale * 10
            yaw = lrf_tracking_ctrl(theta_scale, delTH, self.maxTH, min_TH)

            # if angTH <= 10:
            #     yaw = lrf_tracking_ctrl(self.theta_speed_scale, delTH, self.minTH, self.minTH)
            # else:
            #     yaw = lrf_tracking_ctrl(self.theta_speed_scale, delTH, self.maxTH, self.minTH)
            # if 355 <= currentA <= 360 or 0 <= currentA <= 5:
            #     yaw = lrf_tracking_ctrl(self.theta_speed_scale, delTH, self.minTH, self.minTH)
        
        # 속도 설정
        print("Set Velocity Z : ", yaw)
        self.velocity.linear.x = 0.0
        self.velocity.linear.y = 0.0
        self.velocity.angular.z = yaw if self.reverse_angle_vel is False else yaw * -1

    # QR 코드 컨트롤
    def qrControl(self):
        print("Set QR Control ...")
        
        # 방향이 정해지지 않은 경우 0으로 설정
        try:
            direction: int = DirectionType(int(self.target_pose.y))
        except ValueError:
            direction: int = DirectionType(0)

        if (direction == DirectionType.STOP or \
            self.pgv_data.Control == 1 or \
            self.pgv_data.Fault != 0 or \
            self.pgv_data.Warning != 0 or \
            self.pgv_data.LineID >= 3000 or \
            (self.pgv_data.Tag == 0 or self.pgv_data.Line == 0)):
            # 방향이 정해지지않았거나, 컨트롤이 인식되었거나, 에러가 발생했거나, 라인ID가 3000을 넘거나, QR 인식이 안된 경우
            if self.error_count > 5:
                # 에러 카운트가 5회 이상일 경우 에러 발생 (단순 인식 에러는 무시)
                self.auto_move = AgvMode.ERROR
            else:
                self.error_count += 1
            return

        vel_x: float = self.pgv_data.X / 1000   # X 축 남은 거리
        vel_y: float = self.pgv_data.Y / 1000   # Y 축 남은 거리
        self.lenX = abs(vel_x)     # X 축 남은 거리
        self.lenY = abs(vel_y)     # Y 축 남은 거리

        target_tagID: int = int(self.target_pose.x)         # 목표 QR TagID
        current_tagID: int = self.pgv_data.TagID    # 현재 인식된 QR TagID
        goalA: float = self.target_pose.theta                           # 목표 AGV 각도
        self.current_pose.theta = self.pgv_data.theta       # 현재 각도

        # 회전 속도 설정
        self.yawControl(goalA)

        if self.pgv_data.Tag == 1 and current_tagID == target_tagID:
            # QR 태그가 목표 태그에 도달 할 경우
            if self.lenX <= self.marginX and self.lenY <= self.marginY:
                # 목표 태그에 정위치가 맞아지면 목표 도달
                print("QR Control Completion")
                self.move_mode = MoveType.GOAL_REACH
            else:
                if direction == DirectionType.FRONT:
                    # 전방 이동이라면
                    if self.lenX <= self.marginX:
                        vel_x = 0
                    else:
                        vel_x = lrf_tracking_ctrl(self.x_speed_scale, vel_x, self.minX, self.minX)

                    if self.lenY <= self.marginY:
                        vel_y = 0
                    else:
                        vel_y = lrf_tracking_ctrl(self.y_speed_scale, vel_y, self.minY, self.minY)

                elif direction == DirectionType.REAR:
                    # 후방 이동이라면
                    if self.lenX <= self.marginX:
                        vel_x = 0
                    else:
                        vel_x = -lrf_tracking_ctrl(self.x_speed_scale, vel_x, self.minX, self.minX)

                    if self.lenY <= self.marginY:
                        vel_y = 0
                    else:
                        vel_y = -lrf_tracking_ctrl(self.y_speed_scale, vel_y, self.minY, self.minY)

                elif direction == DirectionType.LEFT:
                    # 좌측 이동이라면
                    if self.lenX <= self.marginX:
                        vel_y = 0
                    else:
                        vel_y = lrf_tracking_ctrl(self.x_speed_scale, vel_x, self.minY, self.minY)

                    if self.lenY <= self.marginY:
                        vel_x = 0
                    else:
                        vel_x = lrf_tracking_ctrl(self.x_speed_scale, vel_y, self.minX, self.minX)
            
                elif direction == DirectionType.RIGHT:
                    # 우측 이동이라면
                    if self.lenX <= self.marginX:
                        vel_y = 0
                    else:
                        vel_y = -lrf_tracking_ctrl(self.x_speed_scale, vel_x, self.minY, self.minY)

                    if self.lenY <= self.marginY:
                        vel_x = 0
                    else:
                        vel_x = -lrf_tracking_ctrl(self.x_speed_scale, vel_y, self.minX, self.minX)
        else:
            # QR 태그 라인 이동 
            if direction == DirectionType.FRONT:
                # 전방 이동이라면
                vel_x = lrf_tracking_ctrl(self.x_speed_scale, 1, self.minX, self.minX)

                if self.lenY <= self.marginY:
                    vel_y = 0
                else:
                    vel_y = lrf_tracking_ctrl(self.y_speed_scale, vel_y, self.minY, self.minY)

            elif direction == DirectionType.REAR:
                # 후방 이동이라면
                vel_x = -lrf_tracking_ctrl(self.x_speed_scale, 1, self.minX, self.minX)

                if self.lenY <= self.marginY:
                    vel_y = 0
                else:
                    vel_y = -lrf_tracking_ctrl(self.y_speed_scale, vel_y, self.minY, self.minY)

            elif direction == DirectionType.LEFT:
                # 좌측 이동이라면
                vel_y = lrf_tracking_ctrl(self.y_speed_scale, 1, self.minY, self.minY)

                if self.lenY <= self.marginY:
                    vel_x = 0
                else:
                    vel_x = lrf_tracking_ctrl(self.x_speed_scale, vel_y, self.minX, self.minX)
        
            elif direction == DirectionType.RIGHT:
                # 우측 이동이라면
                vel_y = -lrf_tracking_ctrl(self.y_speed_scale, 1, self.minY, self.minY)

                if self.lenY <= self.marginY:
                    vel_x = 0
                else:
                    vel_x = -lrf_tracking_ctrl(self.x_speed_scale, vel_y, self.minX, self.minX)

        print("Set Velocity X : ", vel_x)
        print("Set Velocity y : ", vel_y)
        self.velocity.linear.x = vel_x if self.reverse_x_vel is False else vel_x * -1
        self.velocity.linear.y = vel_y if self.reverse_y_vel is False else vel_y * -1

    # 마그네틱 컨트롤
    def magneticMoveControl(self):
        # 방향이 정해지지 않은 경우 0으로 설정
        # x = 진행 방향
        # y = 마그네틱 카운트 수 (무시할 마그네틱 카운트) [0: 만나면 바로 멈춤 / 1 : 1번은 무시]
        # A = 진행 속도 및 방향 (양수: front/right | 음수: rear/left) (고속 최적 속도: 3) (저속 최적 속도: 0.3)
        try:
            direction: int = DirectionType(int(self.target_pose.x))
        except ValueError:
            direction: int = DirectionType(0)

        print("Set Magnetic Control ... ")

        # 방향에 따라 마그네틱 인식 여부 가져오기
        if direction == DirectionType.FRONT or direction == DirectionType.REAR:
            # 전/후방
            magnetic_detect: bool = True if self.magnetic_detect['front'] is True and self.magnetic_detect['rear'] is True else False
            magnetic_detect_other: bool = True if self.magnetic_detect['left'] is True and self.magnetic_detect['right'] is True else False
            mark_detect: bool = self.magnetic_mark['front']
            delX: float = self.target_pose.theta
            del_xy: float = delX
            delY, angle = calc_magnetic_distance(self.magnetic_x_dist, self.magnetic_data['front'], self.magnetic_data['rear'])
            delY *= self.mag_scale
        else:
            # 좌/우측
            magnetic_detect: bool = True if self.magnetic_detect['left'] is True and self.magnetic_detect['right'] is True else False
            magnetic_detect_other: bool = True if self.magnetic_detect['front'] is True and self.magnetic_detect['rear'] is True else False
            mark_detect: bool = self.magnetic_mark['left']
            delY: float = self.target_pose.theta
            del_xy: float = delY
            delX, angle = calc_magnetic_distance(self.magnetic_y_dist, self.magnetic_data['right'], self.magnetic_data['left'])
            delX *= self.mag_scale

        # 방향이 후진일 경우 좌측 감지 센서를 감속 센서로 사용 / 다른 방향일 경우 감속 감지 센서로 사용
        decel_detect: bool = self.magnetic_mark['left'] if direction in [DirectionType.REAR, DirectionType.LEFT] else self.magnetic_mark['decel']
        # decel_detect: bool = self.magnetic_mark['decel'] if direction != DirectionType.REAR else self.magnetic_mark['left']
        mark_detect = False
        if direction == DirectionType.STOP or magnetic_detect is False or (self.move_mode == MoveType.MAG_MOVE_FINE and magnetic_detect_other is False):
            # 방향이 정해지지않았거나, 마그네틱이 인식이 안 된 경우
            if self.error_count > 3:
                print("ERROR END")
                # 에러 카운트가 5회 이상일 경우 에러 발생 (단순 인식 에러는 무시)
                self.auto_move = AgvMode.ERROR
                self.error_count = 0
            else:
                self.error_count += 1
            return
        else:
            self.error_count = 0

        if self.move_mode == MoveType.MAG_MOVE:
            # 마그네틱 이동 이라면
            if self.mag_count >= self.target_pose.y:
                print("FINE")
                # 인식된 마그네틱 개수와 목표 마그네틱 개수가 동일 할 경우
                if self.mag_flag is False and mark_detect is True:
                    # 정지 마크가 인식되었다면 목표 도달
                    print("Magnetic Move Control Completion[MARK]")
                    self.move_mode = MoveType.GOAL_REACH
                    return
                else:
                    # 정지 마크가 인식이 안되었다면
                    if decel_detect is True:
                        # 감속 마크를 인식했다면
                        if abs(self.target_pose.theta) <= 0.25:
                            if self.detect_count >= 12:
                                # count 5       
                                # 설정 속도가 0.25 이하일 경우 목표 도달
                                print("Magnetic Move Control Completion[MARK]")
                                self.move_mode = MoveType.GOAL_REACH
                                return
                            else:
                                self.detect_count += 1
                        
                        if magnetic_detect_other is False:
                            # 감속 시작 (다른 마그네틱 센서가 인식이 되지않았다면) (add 220513)
                            self.decel_flag = True

                    if self.decel_flag is True:
                        # 감속 여부가 참일 경우 감속 시작
                        absSpeed: float = abs(self.decel_count + del_xy)
                        if absSpeed >= 0.3:
                            # 속도 비례 감속 (2.5 이하일 경우 / 2.5 이상 일 경우 2.5로 고정)
                            abs_del_xy: float = abs(del_xy)
                            if del_xy > 0:
                                self.decel_count -= (abs_del_xy if abs_del_xy <= 2.5 else 2.5) * 0.026     # 2.5 -> abs(del_xy)
                            else:
                                self.decel_count += (abs_del_xy if abs_del_xy <= 2.5 else 2.5) * 0.026     # 2.5 -> abs(del_xy)

                        del_xy += self.decel_count

                        if absSpeed >= 1:
                            # 1이상일 경우 단계별로 감속 (0.1 단위로)
                            del_xy = math.trunc(del_xy*10)/10

                        if direction == DirectionType.FRONT or direction == DirectionType.REAR:
                            delX = del_xy
                        else:
                            delY = del_xy

                    if self.mag_flag is False and magnetic_detect_other is True:
                        # 다른 방향의 마그네틱이 인식이 시작되었다면 목표 도달
                        self.move_mode = MoveType.MAG_MOVE_FINE
                    elif self.mag_flag is True and magnetic_detect_other is False and mark_detect is False:
                        # 다른 방향의 마그네틱 인식이랑 정지 마크가 풀렸다면
                        self.mag_flag = False
            else:
                # 인식된 마그네틱 개수와 목표 마그네틱 개수가 동일하지 않은 경우
                print("MOVE")
                if self.mag_flag is False and (magnetic_detect_other is True or \
                    (self.mag_count == 0 and self.target_pose.y == 1 and mark_detect is True)):
                    # 다른 방향의 마그네틱이 인식이 시작되었다면 카운트 추가 (카운트가 0이고 정지 마크가 인식되었다면 카운트 추가)
                    self.mag_count += 1
                    self.mag_flag = True
                elif self.mag_flag is True and magnetic_detect_other is False:
                    # 다른 방향의 마그네틱 인식이 풀렸다면
                    self.mag_flag = False

        self.lenX = abs(delX)     # X 축 남은 거리
        self.lenY = abs(delY)     # Y 축 남은 거리

        if self.move_mode == MoveType.MAG_MOVE_FINE:
            print("MAG_MOVE_FINE")
            # 중앙 정렬
            self.magneticCenterControl(direction)
        else:
            self.current_pose.theta = 0.0 - angle       # 현재 각도
            self.yawControl(0.0)                        # 회전 속도 설정
            self.magneticXYControl(direction, delX, delY)
    
    # 마그네틱 회전 컨트롤
    # x = 방향 (최종 방향)
    # y = 사용 X
    # A = 회전 속도 및 방향 (최적 속도: 85)
    def magneticYAWControl(self):
        print("Set Magnetic YAW Control ... ")
        try:
            direction: DirectionType = DirectionType(int(self.target_pose.x))
        except ValueError:
            direction: DirectionType = DirectionType(0)

        # 마그네틱 인식 여부 가져오기
        mag_detect_none: bool = True if self.magnetic_detect['front'] is False and self.magnetic_detect['rear'] is False and self.magnetic_detect['left'] is False and self.magnetic_detect['right'] is False else False
        if direction == DirectionType.FRONT or direction == DirectionType.REAR:
            mag_detect: bool = True if self.magnetic_detect['front'] is True and self.magnetic_detect['rear'] is True else False
        else:
            mag_detect: bool = True if self.magnetic_detect['left'] is True and self.magnetic_detect['right'] is True else False
        angle: float = self.target_pose.theta

        if self.mag_flag is False and self.mag_count == 1:
            # 마그네틱 카운트가 1일 경우 마그네틱 처음 인식이 풀리고 재 시작인 것으로 파악 
            self.mag_flag = True
            self.yaw_restart_flag = True
            
        if self.mag_flag is False and mag_detect_none is True:
            # 마그네틱 인식이 처음 전부 풀릴 경우 마그네틱 인식 될때까지 회전
            self.mag_count = 1
            self.sendStateMove()
            self.mag_flag = True
        elif self.mag_flag is True:
            if mag_detect is True:
                # 해당 방향의 마그네틱이 인식이 되면 최종 회전으로 변경
                self.move_mode = MoveType.MAG_YAW_FINE
                self.decel_count = 0
            elif self.move_mode == MoveType.MAG_YAW:
                # 해당 방향의 마그네틱이 인식이 안된 경우 속도 점점 줄이기
                if self.yaw_restart_flag is False:
                    if abs(self.decel_count + angle) != 5:
                        if angle > 0:
                            self.decel_count -= 1
                        else:
                            self.decel_count += 1
                    angle += self.decel_count
                else:
                    # 재시작인경우
                    if angle > 0:
                        angle = 5
                    else:
                        angle = -5
            elif self.move_mode == MoveType.MAG_YAW_FINE:
                if mag_detect is False:
                    # 최종 회전인데 마그네틱 인식이 안된 경우
                    if self.error_count > 3:
                        # 에러 카운트가 3회 이상일 경우 에러 발생 (단순 인식 에러는 무시)
                        self.auto_move = AgvMode.ERROR
                        self.error_count = 0
                    else:
                        self.error_count += 1 
                else:
                    self.error_count = 0

        if self.move_mode == MoveType.MAG_YAW_FINE:
            print("MAG_YAW_FINE")
            self.magneticCenterControl(direction)
        else:
            self.current_pose.theta = 0.0 - angle       # 현재 각도
            self.yawControl(0.0)

    # 마그네틱 XY 컨트롤
    def magneticXYControl(self, direction: DirectionType, delX: float, delY, precise_flag: bool = False):
        vel_x: float = 0.0
        vel_y: float = 0.0

        if self.precise_mode.data is True and precise_flag is True:
            margin_X = self.preciseMarginX
            margin_Y = self.preciseMarginY
            min_X = self.minX*self.mag_scale
            min_Y = self.minY*self.mag_scale
        else:
            margin_X = self.marginX
            margin_Y = self.marginY
            min_X = self.minX
            min_Y = self.minY
        
        if direction == DirectionType.FRONT or direction == DirectionType.REAR:
            # 전/후방 이동이라면
            if self.lenX <= margin_X:
                vel_x = 0
            else:
                vel_x = lrf_tracking_ctrl(self.x_speed_scale, delX, self.maxX, min_X)

            if self.lenY <= margin_Y:
                vel_y = 0
            else:
                vel_y = -lrf_tracking_ctrl(self.y_speed_scale, delY, self.maxY, min_Y)

        elif direction == DirectionType.LEFT or direction == DirectionType.RIGHT:
            # 좌/우측 이동이라면
            if self.lenX <= margin_X:
                vel_x = 0
            else:
                vel_x = lrf_tracking_ctrl(self.x_speed_scale, delX, self.maxX, min_X)

            if self.lenY <= margin_Y:
                vel_y = 0
            else:
                vel_y = lrf_tracking_ctrl(self.y_speed_scale, delY, self.maxY, min_Y)

        print("Set Velocity X : ", vel_x)
        print("Set Velocity y : ", vel_y)
        self.velocity.linear.x = vel_x if self.reverse_x_vel is False else vel_x * -1
        self.velocity.linear.y = vel_y if self.reverse_y_vel is False else vel_y * -1

    # 마그네틱 중앙 정렬 컨트롤
    def magneticCenterControl(self, direction):
        print("front - rear")
        print(self.magnetic_data['front'], "-", self.magnetic_data['front'])
        delY, angle_front = calc_magnetic_distance(self.magnetic_x_dist, self.magnetic_data['front'], self.magnetic_data['rear'])
        delY *= self.mag_scale
        print("right - left")
        print(self.magnetic_data['right'], "-", self.magnetic_data['left'])
        delX, angle_right = calc_magnetic_distance(self.magnetic_y_dist, self.magnetic_data['right'], self.magnetic_data['left'])
        delX *= self.mag_scale

        # 현재 방향에 맞는 방향 설정
        angle = angle_front if direction == DirectionType.FRONT or direction == DirectionType.REAR else angle_right

        self.lenX = abs(delX)     # X 축 남은 거리
        self.lenY = abs(delY)     # Y 축 남은 거리

        self.current_pose.theta = 0.0 - angle       # 현재 각도
        self.yawControl(0.0, precise_flag=True)

        margin_X, margin_Y, margin_TH = [self.preciseMarginX, self.preciseMarginY, self.preciseMarginTH] \
            if self.precise_mode.data is True else [self.marginX, self.marginY, self.marginTH]
        
        if self.lenX <= margin_X and self.lenY <= margin_Y and self.angTH <= margin_TH:
            # 회전 및 중앙 위치 맞을 경우 완료
            print("Magnetic Control Completion")
            if (self.precise_mode.data is True and direction == DirectionType.RIGHT and self.lift_up_lim is True and
                self.target_pose.y == 1 and self.received_decel_flag is False and self.detect_count >= 5) or \
                    (self.precise_mode.data is True and self.detect_count >= 15) or \
                    (self.precise_mode.data is False and self.detect_count >= 5):
                # 정밀모드 ON & 우측 이동 & 마그네틱 1칸 이동 & 리프트 상승 리미트 ON & 강제 감속 OFF 일 경우 5번
                # 정밀모드일 경우 15
                # 정밀모드가 아닌경우 5
                print(self.detect_count)
                self.move_mode = MoveType.GOAL_REACH
                self.magneticXYControl(DirectionType.FRONT, 0, 0)
                return
            else:
                self.detect_count += 1
        else:
            print("Nope")
            self.detect_count = 0
        self.magneticXYControl(DirectionType.FRONT, delX, delY, True)

    # 리프트 컨트롤
    def liftControl(self, direction: float):
        lim_flag: bool = False   # True가 될 경우 감속 후 정지
        lift_dir: int = 0   # 리프트 방향 속도

        if direction > 0:
            # 리프트 상승이라면
            lim_flag = self.lift_up_lim
        elif direction < 0:
            # 리프트 하강이라면
            lim_flag = self.lift_down_lim
        else:
            # 아무것도 아니라면
            lim_flag = True

        print("set Lift ... ")
        if lim_flag is True:
            print("Lift Control Completion")
            self.move_mode = MoveType.GOAL_REACH
            lift_dir = 0
        else:
            # 그렇지 않다면 속도 설정
            lift_dir = int(direction)
        
        # 속도 설정
        self.setLiftCMD(lift_dir)

    # AGV 속도 설정
    def setAGVVelocity(self, x: float=0.0, y: float=0.0, z: float=0.0):
        self.velocity.linear.x = x              # 속도 X 축
        self.velocity.linear.y = y              # 속도 Y 축
        self.velocity.angular.z = z             # 타겟 방향
    
    # 리프트 명령 지정 (0일 경우 정지 / 1일 경우 상승 / -1일 경우 하강)
    def setLiftCMD(self, direction: int=0):
        self.lift_cmd_data.mode = 0 if direction == 0 else 1        # 리프트 모드
        self.lift_cmd_data.operate = self.lift_cmd_data.mode        # 리프트 작동 여부
        self.lift_cmd_data.dir = direction                          # 리프트 방향

    # ====================================================
    #                  콜백 함수 정의 부분
    # ====================================================
    # 목표 설정 콜백 함수
    def moveCmdCallback(self, goalData: cmdMove):
        print("receive data: ", goalData)

        self.error_count = 0
        self.decel_count = 0
        self.detect_count = 0
        self.mag_flag = False

        self.move_mode = MoveType(goalData.type)    # 오토 모드 종류
        self.number_data = goalData.number          # 오토 모드 번호
        self.target_pose.x = goalData.X             # 타겟 X 축
        self.target_pose.y = goalData.Y             # 타겟 Y 축
        self.target_pose.theta = goalData.A         # 타겟 방향
        self.precise_mode.data = goalData.precise   # 정밀 모드
        self.mag_count = goalData.mag_count   # 마그네틱 개수 설정
        self.decel_flag = goalData.decel_flag
        self.received_decel_flag = goalData.decel_flag
        
        if goalData.move == 1:
            # 동작 실행 일 경우
            if self.auto_move == AgvMode.STOP or self.auto_move == AgvMode.ERROR:
                # AGV가 멈춰있는 상태라면 AGV 시작
                self.auto_move = AgvMode.START
        else:
            if self.auto_move == AgvMode.START:
                # AGV가 동작하고 있는 경우 정지
                self.auto_move = AgvMode.STOP
        
        # 속도 초기화
        self.setAGVVelocity()
        self.setLiftCMD()

    # 조이스틱 콜백 함수 (메뉴얼 모드인지 파악)
    def joystickControlCallback(self, joystickData: teleop):
        if joystickData.control_mode == 1:
            self.manual_mode = True
        else:
            self.manual_mode = False

    # 현재 위치 설정
    def setCurrentPose(self, poseData):
        self.current_pose = poseData

    # 리프트 리미트 센서 상태 가져오기
    def liftLimCallback(self, io_in_data: io_in):
        self.lift_up_lim = True if io_in_data.lim_up == 1 else False
        self.lift_down_lim = True if io_in_data.lim_down == 1 else False
        
        self.lift_plt_dectect = True if io_in_data.limSen1 == 1 or io_in_data.limSen2 == 1 or io_in_data.limSen3 == 1 or io_in_data.limSen4 == 1 else False

    # QR 코드 데이터 가져오기
    def pgvCallback(self, pgv_data: vision_msg):
        self.pgv_data = vision_msg

    # 마그네틱 데이터 가져오기
    def magneticCallback(self, mag_data: Float64MultiArray):
        self.magnetic_data['front'] = mag_data.data[0]
        self.magnetic_data['rear'] = mag_data.data[1]
        self.magnetic_data['left'] = mag_data.data[2]
        self.magnetic_data['right'] = mag_data.data[3]

    # 마그네틱 인식 여부 가져오기
    def magneticDetectCallback(self, mag_detect_data: mag_detect):
        mag_detect_dict: dict = {
            'front': mag_detect_data.front,
            'rear': mag_detect_data.rear,
            'left': mag_detect_data.left,
            'right': mag_detect_data.right
        }

        for i in mag_detect_dict.keys():
            mag_flag: bool = mag_detect_dict[i]
            self.magnetic_detect[i] = mag_flag

        # self.magnetic_detect['front'] = mag_detect_data.front
        # self.magnetic_detect['rear'] = mag_detect_data.rear
        # self.magnetic_detect['left'] = mag_detect_data.left
        # self.magnetic_detect['right'] = mag_detect_data.right

    # 마그네틱 마크 센서 인식 여부 가져오기
    def magneticMarkCallback(self, mag_mark_data: mag_mark):
        mark_detect_dict: dict = {
            'front': mag_mark_data.front,
            'decel': mag_mark_data.decel,
            'left': mag_mark_data.left,
        }

        for i in mark_detect_dict.keys():
            mark_data: bool = mark_detect_dict[i]
            if mark_data is True:
                self.mark_detect_count[i] = 1
            
            if self.mark_detect_count[i] >= 1:
                mark_data = True
                if self.mark_detect_count[i] >= 5:
                    self.mark_detect_count[i] = 0
                else:
                    self.mark_detect_count[i] += 1
            self.magnetic_mark[i] = mark_data

        # self.magnetic_mark['front'] = mag_mark_data.front
        # self.magnetic_mark['decel'] = mag_mark_data.decel
        # self.magnetic_mark['left'] = mag_mark_data.left


if __name__ == '__main__':
    NavigationControl()
