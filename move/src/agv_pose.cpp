#include <ros/ros.h>
#include <math.h>
#include "ros/time.h"
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/Pose2D.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf/LinearMath/Transform.h>
#include <tf2_ros/transform_listener.h>

class AGVPose
{
public:
  void spin();

private:
  int rate = 10;
  float current_x = 0, current_y = 0, current_A = 0;

  geometry_msgs::Pose2D cur_pose;
  ros::NodeHandle nh_;
  ros::Publisher pose_pub;
  tf2_ros::Buffer tfBuffer;

  void init();
  void update();
};

void AGVPose::init() {
  ros::NodeHandle nhLocal("~");
  pose_pub = nh_.advertise<geometry_msgs::Pose2D>("/agv_pose", 1, true);
}

void AGVPose::spin(){
  init();
  ros::Rate loop_rate(rate);
  tf2_ros::TransformListener tfListener(tfBuffer);

  while (ros::ok())
  {
    update();
    ros::spinOnce();
    loop_rate.sleep();
  }
}

void AGVPose::update()
{
  geometry_msgs::TransformStamped transformStamped;
  try
  {
    transformStamped = tfBuffer.lookupTransform("map", "base_link", ros::Time(0));
    current_x = transformStamped.transform.translation.x;
    current_y = transformStamped.transform.translation.y;

    tf::Quaternion q(transformStamped.transform.rotation.x,
                     transformStamped.transform.rotation.y,
                     transformStamped.transform.rotation.z,
                     transformStamped.transform.rotation.w);
    double roll, pitch, yaw;
    tf::Matrix3x3(q).getRPY(roll, pitch, yaw);
    current_A = yaw * 180 / M_PI;   // + 180

    cur_pose.x = current_x;
    cur_pose.y = current_y;
    cur_pose.theta = current_A;

    pose_pub.publish(cur_pose);
  }
  catch (tf2::TransformException& ex)
  {
    ROS_WARN("%s", ex.what());
    ros::Duration(0.01).sleep();
    //		continue;
  }
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "AGV_POSE");
  AGVPose agv_pose;
  agv_pose.spin();
  return 0;
}
