#!/usr/bin/env python3.8
import rospy
import os
import datetime
from std_msgs.msg import Float64MultiArray, Int32MultiArray
from motor_driver_msgs.msg import Channel_value
from dbClass import DBClass


class RecordNode:
    moter_pub: rospy.Publisher                   # 모터 정보 Pulisher
    power_pub: rospy.Publisher                    # 전원 정보 Pulisher

    db_class: DBClass       # DB 정보

    start_time: datetime.datetime = datetime.datetime.now()     # 시작 시간
    now_time: datetime.datetime = datetime.datetime.now()       # 현재 시간
    pre_second: int = 0     # 이전 초
    wheel_radius: float = 0.2   # 바퀴 반지름

    left_front_rpm_data: int = 0    # 이전 앞 왼쪽 바퀴 RPM 데이터
    right_front_rpm_data: int = 0   # 이전 앞 오른쪽 바퀴 RPM 데이터
    left_rear_rpm_data: int = 0     # 이전 뒤 왼쪽 바퀴 RPM 데이터
    right_rear_rpm_data: int = 0    # 이전 뒤 오른쪽 바퀴 RPM 데이터

    left_front_rpm_count: int = 0    # 이전 앞 왼쪽 바퀴 RPM 데이터 카운트
    right_front_rpm_count: int = 0   # 이전 앞 오른쪽 바퀴 RPM 데이터 카운트
    left_rear_rpm_count: int = 0     # 이전 뒤 왼쪽 바퀴 RPM 데이터 카운트
    right_rear_rpm_count: int = 0    # 이전 뒤 오른쪽 바퀴 RPM 데이터 카운트

    left_front_data: float = 0.0    # 앞 왼쪽 바퀴 데이터
    right_front_data: float = 0.0   # 앞 오른쪽 바퀴 데이터
    left_rear_data: float = 0.0     # 뒤 왼쪽 바퀴 데이터
    right_rear_data: float = 0.0    # 뒤 오른쪽 바퀴 데이터

    db_power_time: datetime.timedelta = datetime.timedelta()   # DB 시간

    power_day: int = 0              # 전원 일
    power_hour: int = 0             # 전원 시
    power_minute: int = 0           # 전원 분
    power_second: int = 0           # 전원 초

    def __init__(self):
        rospy.init_node('emergency_node')

        # Set Variable
        rate = rospy.Rate(10) #10hz
        self.wheel_radius = float(rospy.get_param("~wheel_radius", self.wheel_radius))

        # Set Publish
        self.moter_pub = rospy.Publisher("/RECORD/MOTER", Float64MultiArray, queue_size=10)
        self.power_pub = rospy.Publisher("/RECORD/POWER", Int32MultiArray, queue_size=10)

        # Set Subscriber
        rospy.Subscriber("/motor_driver_front/speed_data", Channel_value, self.wheelFRPMCallback)           # 앞 바퀴 엔코더 값 가져오기
        rospy.Subscriber("/motor_driver_rear/speed_data", Channel_value, self.wheelRRPMCallback)            # 뒤 바퀴 엔코더 값 가져오기

        # Set dir
        data_path: str = "/home/sis/catkin_ws/data/"
        os.makedirs(data_path, exist_ok=True)
        os.chdir(data_path)
        
        self.__initDatabase()

        # Loop
        while not rospy.is_shutdown():
            self.now_time = datetime.datetime.now() 
            self.setPowerTime()     # 전원 시간 설정
            self.publish()          # publish
            rate.sleep()

    # 데이터베이스 정의
    def __initDatabase(self):
        self.db_class = DBClass('record.db')

        # 모터 드라이브 테이블 확인
        self.db_class.checkTable('moter_drive', {
            "id": 'INTEGER PRIMARY KEY NOT NULL',
            "left_front": 'REAL NOT NULL',
            "right_front": 'REAL NOT NULL',
            "left_rear": 'REAL NOT NULL',
            "right_rear": 'REAL NOT NULL',
        })
        
        # 전원 테이블 확인
        self.db_class.checkTable('power', {
            "id": 'INTEGER PRIMARY KEY NOT NULL',
            "day": 'INTEGER NOT NULL',
            "hour": 'INTEGER NOT NULL',
            "minute": 'INTEGER NOT NULL',
            "second": 'INTEGER NOT NULL',
        })

        # 모터 드라이브 데이터 확인
        if not self.db_class.selectData('moter_drive'):
            # 데이터가 없을 경우 생성
            self.db_class.insertData('moter_drive', {'id': 1, 'left_front': 0.0, 'right_front': 0.0, 'left_rear': 0.0, 'right_rear': 0.0})
        self.getMoterDriveData()

        # 전원 데이터 확인
        if not self.db_class.selectData('power'):
            # 데이터가 없을 경우 생성
            self.db_class.insertData('power', {'id': 1, 'day': 0, 'hour': 0, 'minute': 0, 'second': 0})
        self.getPowerData()
    
    # 모터 드라이브 데이터 가져오기
    def getMoterDriveData(self):
        moter_drive_data: list = self.db_class.selectData('moter_drive')
        
        self.left_front_data = moter_drive_data[0][1]
        self.right_front_data = moter_drive_data[0][2]
        self.left_rear_data = moter_drive_data[0][3]
        self.right_rear_data = moter_drive_data[0][4]

    # 전원 데이터 가져오기
    def getPowerData(self):
        power_data: list = self.db_class.selectData('power')
        self.db_power_time = datetime.timedelta(days=power_data[0][1], hours=power_data[0][2], minutes=power_data[0][3], seconds=power_data[0][4])

    # 전원 시간 설정
    def setPowerTime(self):
        power_time: datetime.timedelta = self.db_power_time + (self.now_time - self.start_time)
        power_seconds: int = power_time.seconds         # 전체 초 가져오기

        self.power_day = power_time.days                # 일 가져오기
        self.power_hour = power_seconds // (60 * 60)    # 시 가져오기
        power_seconds %= (60 * 60)
        self.power_minute = power_seconds // 60         # 분 가져오기
        self.power_second = power_seconds % 60          # 초 가져오기

        # DB 업데이트
        self.db_class.updateData('power', ('id', 1), {'id': 1, 'day': self.power_day, 'hour': self.power_hour, 'minute': self.power_minute, 'second': self.power_second})

    # 모터 이동 거리 설정
    def getMoterDist(self) -> list:
        left_front_dist: float = 0.0
        right_front_dist: float = 0.0
        left_rear_dist: float = 0.0
        right_rear_dist: float = 0.0

        # 앞 왼쪽 바퀴 이동 거리
        if self.left_front_rpm_count != 0:
            left_front_dist = self.calcMoterDist(self.left_front_rpm_data / self.left_front_rpm_count)
            self.left_front_rpm_data = 0
            self.left_front_rpm_count = 0

        # 앞 오른쪽 바퀴 이동 거리
        if self.right_front_rpm_count != 0:
            right_front_dist = self.calcMoterDist(self.right_front_rpm_data / self.right_front_rpm_count)
            self.right_front_rpm_data = 0
            self.right_front_rpm_count = 0
        
        # 뒤 왼쪽 바퀴 이동 거리
        if self.left_rear_rpm_count != 0:
            left_rear_dist = self.calcMoterDist(self.left_rear_rpm_data / self.left_rear_rpm_count)
            self.left_rear_rpm_data = 0
            self.left_rear_rpm_count = 0

        # 뒤 오른쪽 바퀴 이동 거리
        if self.right_rear_rpm_count != 0:
            right_rear_dist = self.calcMoterDist(self.right_rear_rpm_data / self.right_rear_rpm_count)
            self.right_rear_rpm_data = 0
            self.right_rear_rpm_count = 0


        self.left_front_data += abs(left_front_dist)
        self.right_front_data += abs(right_front_dist)
        self.left_rear_data += abs(left_rear_dist)
        self.right_rear_data += abs(right_rear_dist)

        # DB 업데이트
        self.db_class.updateData('moter_drive', ('id', 1), {'id': 1, 'left_front': self.left_front_data, 'right_front': self.right_front_data, 'left_rear': self.left_rear_data, 'right_rear': self.right_rear_data})

        return [self.left_front_data, self.right_front_data, self.left_rear_data, self.right_rear_data] 

    # 모터 이동 거리 계산
    def calcMoterDist(self, rpm_data: float):
        return round(((rpm_data / 60) * 2 * self.wheel_radius * 3.141592) / 1000, 4)    # 1km 단위

    # publish
    def publish(self):
        if self.pre_second != self.now_time.second:
            # 이전 초와 현재 시간의 초와 동일하지 않을 경우 publish 하기
            # 모터 값 publish
            moter_data: Float64MultiArray = Float64MultiArray() 
            moter_data.data = self.getMoterDist()
            self.moter_pub.publish(moter_data)

            # 전원 값 publish
            power_data: Int32MultiArray = Int32MultiArray() 
            power_data.data = [self.power_day, self.power_hour, self.power_minute, self.power_second]
            self.power_pub.publish(power_data)

            self.pre_second = self.now_time.second

    # 앞 바퀴 속도 가져오기
    def wheelFRPMCallback(self, wheel_f_rpm_data):
        self.left_front_rpm_data += wheel_f_rpm_data.left_wheel
        self.right_front_rpm_data += wheel_f_rpm_data.right_wheel

        self.left_front_rpm_count += 1
        self.right_front_rpm_count += 1

    # 뒤 바퀴 속도 가져오기
    def wheelRRPMCallback(self, wheel_r_rpm_data):
        self.left_rear_rpm_data += wheel_r_rpm_data.left_wheel
        self.right_rear_rpm_data += wheel_r_rpm_data.right_wheel

        self.left_rear_rpm_count += 1
        self.right_rear_rpm_count += 1


if __name__=='__main__':
    RecordNode()