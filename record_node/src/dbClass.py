import sqlite3 as sq

from typing import Dict, Any, Tuple


class DBClass:
    db_name: str
    conn: sq.connect  # DB 연결
    cur: sq.Cursor

    # 생성자
    def __init__(self, db_name: str):
        self.db_name = db_name

    # 테이블 확인
    def checkTable(self, table_name: str, column_data: Dict[str, str]):
        self.open()  # DB 실행

        # 테이블 정보 가져오기
        sql_query: str = f"SELECT name FROM sqlite_master WHERE type='table' AND name ='{table_name}'"
        self.cur.execute(sql_query)

        # 테이블 정보가 없으면 테이블 만들기
        if not self.cur.fetchall():
            sql_query: str = f"CREATE TABLE {table_name} ("
            column_list: list = list(column_data.keys())
            for i in range(len(column_list)):
                sql_query += f"{column_list[i]} {column_data[column_list[i]]}"
                if i < len(column_list) - 1:
                    # 마지막 요소가 아닐 경우
                    sql_query += ", "
                else:
                    # 마지막 요소 일 경우
                    sql_query += ")"
            self.cur.execute(sql_query)

        self.close()  # DB 닫기

    # DB 실행 / 커서 획득
    def open(self):
        self.conn = sq.connect(self.db_name)  # sqlite3 db 파일 접속, 없으면 생성
        self.cur = self.conn.cursor()  # 커서 획득

    # DB 명령 수행 확정/ DB 닫기 함수
    def close(self):
        self.conn.commit()
        self.conn.close()

    # 데이터 베이스 삽입 함수
    def insertData(self, table_name: str, column_data: Dict[str, Any]):
        self.open()

        # 쿼리문 작성
        sql_first: str = f"INSERT INTO {table_name} ("
        sql_second: str = "("
        column_list: list = list(column_data.keys())
        for i in range(len(column_list)):
            sql_first += column_list[i]
            sql_second += f":{column_list[i]}"
            if i < len(column_list) - 1:
                # 마지막 요소가 아닐 경우
                sql_first += ", "
                sql_second += ", "
            else:
                # 마지막 요소 일 경우
                sql_first += ")"
                sql_second += ")"
        sql_query: str = f"{sql_first} VALUES {sql_second}"
        self.cur.execute(sql_query, column_data)

        self.close()

    # 데이터 베이스 수정 함수
    def updateData(self, table_name: str, search_data: Tuple[str, Any], column_data: Dict[str, Any]):
        # 데이터 있는지 확인
        return_data: list = self.selectData(table_name, search_data)
        if not return_data:
            # 데이터가 없을 경우 생성
            self.insertData(table_name, column_data)

        self.open()

        # 쿼리문 작성
        sql_query: str = f"UPDATE {table_name} SET "
        column_list: list = list(column_data.keys())
        for i in range(len(column_list)):
            sql_query += f"{column_list[i]}=:{column_list[i]}"
            if i < len(column_list) - 1:
                # 마지막 요소가 아닐 경우
                sql_query += ", "
        sql_query += f" WHERE {search_data[0]}=:{search_data[0]}"
        self.cur.execute(sql_query, column_data)

        self.close()

    # 데이터 베이스 테이블 출력 함수
    def selectData(self, table_name: str, search_data: Tuple[str, Any] = None) -> list:
        self.open()

        # 쿼리문 작성
        sql_query: str = f"SELECT * FROM {table_name}"
        if search_data is not None:
            # 찾을 데이터가 None 이 아닐 경우
            sql_query += f" WHERE {search_data[0]}=?"
            self.cur.execute(sql_query, (search_data[1], ))
        else:
            self.cur.execute(sql_query)

        rows = self.cur.fetchall()

        self.close()
        return rows